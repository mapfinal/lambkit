# Lambkit Document


# json简洁架构
Consul+Motan/HttpRpc+Redis+Api

Consul的主要功能 ：
（1） 通过DNS或HTTP，服务发现功能简单
（2）多种健康的检查方式：http返回码200，内存是否超限，tcp连接是否成功
（3） KV存储
（4）多数据中心（这个zookeeper没有）

支持 Service Mesh服务网格


# 仿SpringCloud
Nacos+HttpRpc+Redis+Api


# RPC服务治理架构
Zookeeper+Dubbo+Redis



## 高并发

在开发高并发系统时有三把利器用来保护系统：缓存、降级和限流。

缓存的目的是提升系统访问速度和增大系统能处理的容量，可谓是抗高并发流量的银弹；
而降级是当服务出问题或者影响到核心流程的性能则需要暂时屏蔽掉，待高峰或者问题解决后再打开；
而有些场景并不能用缓存和降级来解决，比如稀缺资源（秒杀、抢购）、写服务（如评论、下单）、频繁的复杂查询（评论的最后几页），因此需有一种手段来限制这些场景的并发/请求量，即限流。


### 限流

常见的限流有：
限制总并发数（比如数据库连接池、线程池）、
限制瞬时并发数（如nginx的limit_conn模块，用来限制瞬时并发连接数）、
限制时间窗口内的平均速率（如Guava的RateLimiter、nginx的limit_req模块，限制每秒的平均速率）；
其他还有如限制远程接口调用速率、限制MQ的消费速率。
另外还可以根据网络连接数、网络流量、CPU或内存负载等来限流。

常用的算法：
令牌桶算法Guava
令牌桶算法同样是实现限流是一种常见的思路，最为常用的 Google 的 Java 开发工具包 Guava 中的限流工具类 RateLimiter 就是令牌桶的一个实现。令牌桶的实现思路类似于生产者和消费之间的关系。
系统服务作为生产者，按照指定频率向桶（容器）中添加令牌，如 QPS 为 2，每 500ms 向桶中添加一个令牌，如果桶中令牌数量达到阈值，则不再添加。
请求执行作为消费者，每个请求都需要去桶中拿取一个令牌，取到令牌则继续执行；如果桶中无令牌可取，就触发拒绝策略，可以是超时等待，也可以是直接拒绝本次请求，由此达到限流目的。

固定窗口限流redis
Redis 中的固定窗口限流是使用 incr 命令实现的，incr 命令通常用来自增计数；如果我们使用时间戳信息作为 key，自然就可以统计每秒的请求量了，以此达到限流目的。
滑动窗口限流redis


单机限流，可以用到 AtomicInteger、RateLimiter、Semaphore 这些。但是在分布式中，就不能使用了。
常用分布式限流用 Nginx 限流，但是它属于网关层面，不能解决所有问题，例如内部服务，短信接口，你无法保证消费方是否会做好限流控制，所以自己在应用层实现限流还是很有必要的。


| 算法名称 | 需要确定参数 | 实现简介 | 空间复杂度 | 说明  |
|:----------|:----------|:----------|:----------|:----------|
| 固定窗口计数 | 计数周期T，周期内最大访问数N | 使用计数器在周期内累加访问次数，达到最大次数后出发限流策略 | O(1)，仅需要记录周期内访问次数及周期开始时间 | 周期切换时可能出现访问次数超过限定值 |
| 滑动窗口计数 | 计数周期T，周期内最大访问数N，滑动窗口数M | 将时间周期分为M个小周期，分别记录每个小周期内访问次数，并且根据时间滑动删除过期的小周期        | O(M)，需要记录每个小周期中的访问数量 | 解决固定窗口算法周期切换时的访问突发问题 |
| 漏桶算法 | 漏桶流出速度r，漏桶容量N | 服务到达时直接放入漏桶，如当前容量达到N，则触发限流侧率，程序以r的速度在漏桶中获取访问请求，知道漏桶为空 | O(1)，仅需要记录当前漏桶中容量 | 平滑流量，保证服务请求到达服务方的速度恒定 |
| 令牌桶算法 | 令牌产生速度r，令牌桶容量N | 程序以r的速度向令牌桶中增加令牌，直到令牌桶满，请求到达时向令牌桶请求令牌，如有满足需求的令牌则通过请求，否则触发限流策略 | O(1)，仅需要记录当前令牌桶中令牌数 | 能够在限流的基础上，处理一定量的突发请求 |



### consul

``` json
{
	"id": "192.168.8.136:9090-/sdss/gis/wms",
	"interval": 10,
	"name": "lambkit-service",
	"port": 9090,
	"tags": [
		"TYPE_action",
		"SERVER_SDSS-Server",
		"URL_http://192.168.8.136:9090/sdss/gis/wms"
	],
	"ip": "192.168.8.136",
	"url": "http://192.168.8.136:9090/sdss/gis/wms",
	"checks": [{
		"checkId": "serfHealth",
		"name": "Serf Health Status",
		"node": "yangyongdeMBP",
		"notes": "",
		"output": "Agent alive and reachable",
		"serviceId": "",
		"serviceName": "",
		"serviceTags": [],
		"status": "passing"
	}, {
		"checkId": "service:192.168.8.136:9090-/sdss/gis/wms",
		"name": "Service 'lambkit-service' check",
		"node": "yangyongdeMBP",
		"notes": "",
		"output": "HTTP GET http://192.168.8.136:9090/sdss/gis/wms: 200 OK Output: layer or server is blank",
		"serviceId": "192.168.8.136:9090-/sdss/gis/wms",
		"serviceName": "lambkit-service",
		"serviceTags": ["TYPE_action", "SERVER_SDSS-Server", "URL_http://192.168.8.136:9090/sdss/gis/wms"],
		"status": "passing"
	}],
	"node": {
		"address": "127.0.0.1",
		"datacenter": "dc1",
		"node": "yangyongdeMBP",
		"nodeMeta": {
			"consul-network-segment": ""
		},
		"taggedAddresses": {
			"lan": "127.0.0.1",
			"wan": "127.0.0.1"
		}
	}
}
```

### nacos

``` json
{
	"clusterName": "DEFAULT",
	"enabled": true,
	"ephemeral": true,
	"healthy": true,
	"instanceHeartBeatInterval": 5000,
	"instanceHeartBeatTimeOut": 15000,
	"instanceId": "192.168.10.2#9090#DEFAULT#lambkit@@http-apiroute-sdss.version",
	"instanceIdGenerator": "simple",
	"ip": "192.168.10.2",
	"ipDeleteTimeout": 30000,
	"metadata": {},
	"port": 9090,
	"serviceName": "lambkit@@http-apiroute-sdss.version",
	"weight": 1.0
}
```
