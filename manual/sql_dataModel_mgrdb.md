
{
	
	$alias: {
		//DataModel（mgrdb）的参数
		type: "DataModel",
		name: "dataModel_name_alias",
		config: {
		},
		table: "table_name",
		columns: "*",//select
		alias: "alias",
		filter: {
		},
		join: [
			{
				type: "left",
				table: "table_name1",
				alias: "alias1",
				on: [
					{
						mainField: "",
						joinField: ""
					}
				],
				filter: {
					column: ""
				}
			}
		]
	},
	$data0: {
		//SqlTemplate的参数
		type: "SqlTemplate",
		name: "template_name",
		param: {
		}
	},
	$data1: {
		//Sql拼接的参数
		type: "Sql",
		name: "name",
		database: "main",
		table: "table_name",
		columns: "*",//select
		page: 1,
		count: 15,
		order: "order_field",
		filter: {//where
			field:"condition",
			field:"@$data0/pac"
			$and: {//嵌套
			},
			$or: {//嵌套
			}
			// field = condition and (...) or (...)
		},
		group: {
			by: "field",
			having: {
				//filter
			}
		},
		join: [
			{
				type: "left",
				table: "table2_name",
				column: "",
				on: [
					{
						maincol:"", 
						joincol:""
					}
				]
			}
		}
	}
}