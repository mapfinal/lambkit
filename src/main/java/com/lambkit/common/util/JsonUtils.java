/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.common.util;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.JsonKit;

/**
 * 
 * 1:将JavaBean转换成Map、JSONObject 2:将Map转换成Javabean 3:将JSONObject转换成Map、Javabean
 */
public class JsonUtils {

	public static String writeValueAsString(Object value) {
		return JsonKit.toJson(value);
	}
	
	public static Map<String, Object> toMapValue(Object value) throws IllegalArgumentException {
		return convertValue(value, Map.class);
	}

	public static <T> T convertValue(Object value, Class<T> clazz) throws IllegalArgumentException {
		if (StringUtils.isNullOrEmpty(value))
			return null;
		String jsonString = JsonKit.toJson(value);
		return JsonKit.parse(jsonString, clazz);
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> toMapValue(String jsonString) throws IllegalArgumentException {
		return convertValue(jsonString, Map.class);
	}

	public static <T> T convertValue(String jsonString, Class<T> clazz) throws IllegalArgumentException {
		if (StringUtils.isNullOrEmpty(jsonString))
			return null;
		return JsonKit.parse(jsonString, clazz);
	}
	
	/**
	 * 获取节点值
	 * @author mengfeiyang
	 * @param jsonContent
	 * @param jsonPath
	 * @return
	 * @throws Exception
	 */
	public static synchronized Object getNodeValue(String jsonContent, String jsonPath) throws Exception {
		String[] nodes = jsonPath.split("\\.");
		JSONObject obj = JSONObject.parseObject(jsonContent);

		for (int i = 0; i < nodes.length; i++) {
			if (obj != null) {
				obj = getObj(obj, nodes[i]);
			}

			if ((i + 1) == nodes.length) {
				try{
					return obj.get(nodes[i]);
				}catch(Exception e){
					return "JSONException:"+e.getMessage()+",NodeString:"+obj.toString();
				}
			}
		}
		return null;
	}
	
	/**
	 * 对节点进行解析
	 * 
	 * @author mengfeiyang
	 * @param obj
	 * @param node
	 * @return
	 */
	private static JSONObject getObj(JSONObject obj, String node) {
		try {
			if (node.contains("[")) {
				JSONArray arr = obj.getJSONArray(node.substring(0,node.indexOf("[")));
				for (int i = 0; i < arr.size(); i++) {
					if ((i + "").equals(node.substring(node.indexOf("["),node.indexOf("]")).replace("[", ""))) {
						return arr.getJSONObject(i);
					}
				}
			} else {
				return obj.getJSONObject(node);
			}
		} catch (Exception e) {
			return obj;
		}
		return null;
	}

	/**
	 * 将Javabean转换为Map
	 * 
	 * @param javaBean javaBean
	 * @return Map对象
	 */
	public static Map toMap(Object javaBean) {
		Map result = new HashMap();
		Method[] methods = javaBean.getClass().getDeclaredMethods();
		for (Method method : methods) {
			try {
				if (method.getName().startsWith("get")) {
					String field = method.getName();
					field = field.substring(field.indexOf("get") + 3);
					field = field.toLowerCase().charAt(0) + field.substring(1);
					Object value = method.invoke(javaBean, (Object[]) null);
					result.put(field, null == value ? "" : value.toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 将Json对象转换成Map
	 * 
	 * @param jsonObject json对象
	 * @return Map对象
	 */
	public static Map toMap(String jsonString) {
		return JSON.parseObject(jsonString, Map.class);
	}

//	/**
//	 * 将JavaBean转换成JSONObject（通过Map中转）
//	 * 
//	 * @param bean javaBean
//	 * @return json对象
//	 */
//	public static JSONObject toJSON(Object bean) {
//		return JSON.parseObject(bean);
//	}
//
//	public static String toJSON(Map map) {
//		return JSON.parseObject(map).toJSONString();
//	}

	/**
	 * 将Map转换成Javabean
	 * 
	 * @param javabean javaBean
	 * @param data     Map数据
	 */
	public static Object toJavaBean(Object javabean, Map data) {
		Method[] methods = javabean.getClass().getDeclaredMethods();
		for (Method method : methods) {
			try {
				if (method.getName().startsWith("set")) {

					String field = method.getName();
					field = field.substring(field.indexOf("set") + 3);
					field = field.toLowerCase().charAt(0) + field.substring(1);
					method.invoke(javabean, new Object[] {
							data.get(field)
					});
				}
			} catch (Exception e) {
			}
		}
		return javabean;
	}

	/**
	 * JSONObject到JavaBean
	 * 
	 * @param bean javaBean
	 * @return json对象
	 * @throws ParseException json解析异常
	 * @throws JSONException
	 */
	public static Object toJavaBean(Object javabean, String jsonString) {
		JSONObject jsonObject = JSONObject.parseObject(jsonString);
		Map map = toMap(jsonObject.toString());
		return toJavaBean(javabean, map);
	}
}
