package com.lambkit.common.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.lambkit.common.model.LambkitPojo;
import com.lambkit.common.util.ModelUtils;
import com.lambkit.db.sql.IQuery;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

public abstract class LambkitApiServiceImpl<M extends LambkitPojo<M>> implements LambkitApiService<M> {
	
	public abstract LambkitModelServiceImpl dao();
	
	public M findById(Object idValue) {
		return (M) dao().findById(idValue).toPojo();
	}
	
	public M findById(Object idValue, String columns) {
		return (M) dao().findById(idValue, columns).toPojo();
	}
	
	public M findById(Object[] idValues, String columns) {
		return (M) dao().findById(idValues, columns).toPojo();
	}
	
	@Override
	public M findByPrimaryKey(Object id) {
		return (M) dao().findById(id).toPojo();
	}
	
	@Override
	public M findFirst(Example example) {
		return (M) dao().findFirst(example).toPojo();
	}
	
	@Override
	public M findFirst(Columns columns) {
		// TODO Auto-generated method stub
		return (M) dao().findFirstByColumns(columns).toPojo();
	}
	
	@Override
	public M findFirst(Columns columns, String orderby) {
		// TODO Auto-generated method stub
		return (M) dao().findFirstByColumns(columns, orderby).toPojo();
	}
	
	@Override
	public List<M> find(Example example, Integer count) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().find(example, count));
	}
	
	@Override
	public List<M> find(Columns columns, Integer count) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().findListByColumns(columns, count));
	}
	
	@Override
	public List<M> find(Columns columns, String orderby, Integer count) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().findListByColumns(columns, orderby, count));
	}
	
	@Override
	public List<M> find(Example example) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().find(example));
	}
	
	@Override
	public List<M> find(Columns columns) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().findListByColumns(columns));
	}
	
	@Override
	public List<M> find(Columns columns, String orderby) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().findListByColumns(columns, orderby));
	}
	
	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Example example) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().paginate(pageNumber, pageSize, example));
	}
	
	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().paginateByColumns(pageNumber, pageSize, columns));
	}
	

    @Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns, String orderby) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().paginateByColumns(pageNumber, pageSize, columns, orderby));
	}
    
	@Override
	public Page<M> paginate(Example example, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		int pageSize = limit;
		int pageNumber = offset / pageSize + 1;
		return paginate(pageNumber, pageSize, example);
	}
	
	@Override
	public Page<M> paginate(Columns columns, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		int pageSize = limit;
		int pageNumber = offset / pageSize + 1;
		return paginate(pageNumber, pageSize, columns);
	}
	

    @Override
	public Page<M> paginate(Columns columns, String orderby, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		int pageSize = limit;
		int pageNumber = offset / pageSize + 1;
		return paginate(pageNumber, pageSize, columns, orderby);
	}
    
	public Long count(Example example) {
		return dao().count(example);
	}
	
	public Long count(Columns columns) {
		return dao().count(columns);
	}

	public boolean insert(Record record) { 
		return dao().insert(record);
	}
	
	public boolean insert(String primaryKey, Record record) { 
		return dao().insert(primaryKey, record);
	}
	
	public boolean deleteById(Object idValue) {
		return dao().deleteById(idValue);
	}
	
	public int deleteByPrimaryKey(Object id) {
		return dao().deleteById(id) ? 1 : -1;
	}
	
	public int deleteByPrimaryKeys(String ids) {
		if (StringUtils.isBlank(ids)) {
			return 0;
		}
		String[] idArray = ids.split(",");
		int count = 0;
		for (String idStr : idArray) {
			if (StringUtils.isBlank(idStr)) {
				continue;
			}
			Long id = Long.parseLong(idStr);
			int result = deleteByPrimaryKey(id);
			count += result;
		}
		return count;
	}
	
	@Override
	public int delete(Example example) {
		return dao().delete(example);
	}
	
	@Override
	public int delete(Columns columns) {
		return dao().delete(columns);
	}

	@Override
	public int update(Record record, Example example) {
		return dao().update(record, example);
	}
	
	@Override
	public int update(Record record, Columns columns) {
		return dao().update(record, columns);
	}

	@Override
	public boolean updateByPrimaryKey(String primaryKey, Record record) { 
		return dao().updateByPrimaryKey(primaryKey, record);
	}
	
	@Override
	public boolean update(Record record) {
		return dao().update(record);
	}
	
	@Override
	public M findFirst(IQuery queryParas) {
		// TODO Auto-generated method stub
		return (M) dao().findFirst(queryParas).toPojo();
	}

	@Override
	public List<M> find(IQuery queryParas, Integer count) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().find(queryParas, count));
	}

	@Override
	public List<M> find(IQuery queryParas) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().find(queryParas));
	}

	@Override
	public Page<M> paginate(IQuery queryParas) {
		// TODO Auto-generated method stub
		return ModelUtils.modelToPojo(dao().paginate(queryParas));
	}

	@Override
	public Page<M> paginate(IQuery queryParas, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		int pageSize = limit;
		int pageNumber = offset / pageSize + 1;
		queryParas.setPage(pageSize);
		queryParas.setCount(pageNumber);
		return paginate(queryParas);
	}

	@Override
	public Object query(IQuery queryParas) {
		// TODO Auto-generated method stub
		return dao().query(queryParas);
	}

	@Override
	public Long count(IQuery queryParas) {
		// TODO Auto-generated method stub
		return dao().count(queryParas);
	}

	@Override
	public int delete(IQuery queryParas) {
		// TODO Auto-generated method stub
		return dao().delete(queryParas);
	}

	@Override
	public int update(Record record, IQuery queryParas) {
		// TODO Auto-generated method stub
		return dao().update(record, queryParas);
	}
}
