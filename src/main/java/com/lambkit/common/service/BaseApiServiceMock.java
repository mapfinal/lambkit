package com.lambkit.common.service;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.lambkit.common.model.LambkitPojo;
import com.lambkit.db.sql.IQuery;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

/**
 *  降级实现LambkitApiService抽象类 
 * @author yangyong
 *
 * @param <M>
 */
public abstract class BaseApiServiceMock<M extends LambkitPojo<M>> implements LambkitApiService<M> {

	@Override
	public M findById(Object idValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public M findById(Object idValue, String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public M findById(Object[] idValues, String columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public M findByPrimaryKey(Object id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public M findFirst(IQuery queryParas) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public M findFirst(Example example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public M findFirst(Columns columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public M findFirst(Columns columns, String orderby) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<M> find(IQuery queryParas, Integer count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<M> find(Example example, Integer count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<M> find(Columns columns, Integer count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<M> find(Columns columns, String orderby, Integer count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<M> find(IQuery queryParas) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<M> find(Example example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<M> find(Columns columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<M> find(Columns columns, String orderby) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<M> paginate(IQuery queryParas) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Example example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns, String orderby) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<M> paginate(IQuery queryParas, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<M> paginate(Example example, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<M> paginate(Columns columns, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<M> paginate(Columns columns, String orderby, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object query(IQuery queryParas) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long count(IQuery queryParas) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long count(Example example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long count(Columns columns) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean insert(M record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean insert(Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean insert(String primaryKey, Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteById(Object idValue) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int deleteByPrimaryKey(Object id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteByPrimaryKeys(String ids) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(IQuery queryParas) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(Example example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(Columns columns) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean update(M record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateByPrimaryKey(String primaryKey, Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int update(Record record, IQuery queryParas) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Record record, Example example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Record record, Columns columns) {
		// TODO Auto-generated method stub
		return 0;
	}
}
