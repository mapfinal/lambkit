/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.common.service;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.lambkit.common.model.LambkitPojo;
import com.lambkit.db.sql.IQuery;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

/**
 * LambkitApiService接口
 */
public interface LambkitApiService<M extends LambkitPojo<M>> {

	M findById(Object idValue); 
	M findById(Object idValue, String columns); 
	M findById(Object[] idValues, String columns); 
	M findByPrimaryKey(Object id); 
	
	M findFirst(IQuery queryParas);
	M findFirst(Example example);
	M findFirst(Columns columns);
	M findFirst(Columns columns, String orderby);

	List<M> find(IQuery queryParas, Integer count);
	List<M> find(Example example, Integer count);
	List<M> find(Columns columns, Integer count);
	List<M> find(Columns columns, String orderby, Integer count);
	
	List<M> find(IQuery queryParas);
	List<M> find(Example example);
	List<M> find(Columns columns);
	List<M> find(Columns columns, String orderby);

	 /**
     * 分页查询数据
     * @return
     */
    Page<M> paginate(IQuery queryParas);
    Page<M> paginate(Integer pageNumber, Integer pageSize, Example example);
    Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns);
    Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns, String orderby);
    /**
     * offet page
     * @param query
     * @param offset
     * @param limit
     * @return
     */
    Page<M> paginate(IQuery queryParas, Integer offset, Integer limit);
    Page<M> paginate(Example example, Integer offset, Integer limit);
    Page<M> paginate(Columns columns, Integer offset, Integer limit);
    Page<M> paginate(Columns columns, String orderby, Integer offset, Integer limit);
	
    /**
     * findFirst or Find or paginate
     * @param query
     * @return
     */
    Object query(IQuery queryParas);
    
	/**
	 * 获取数量	
	 * @param query
	 * @return
	 */
    Long count(IQuery queryParas);
    Long count(Example example);
    Long count(Columns columns);

    boolean insert(M record);
	boolean insert(Record record);
	boolean insert(String primaryKey, Record record);
	
	boolean deleteById(Object idValue);
	int deleteByPrimaryKey(Object id);
	/**
	 * ids逗号分隔
	 * @param ids
	 * @return
	 */
	int deleteByPrimaryKeys(String ids);
	int delete(IQuery queryParas);
	int delete(Example example);
	int delete(Columns columns);

	boolean update(M record);
	boolean update(Record record);
	boolean updateByPrimaryKey(String primaryKey, Record record);
	int update(Record record, IQuery queryParas);
	int update(Record record, Example example);
	int update(Record record, Columns columns);

}