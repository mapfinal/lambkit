/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.common;

import java.lang.management.ManagementFactory;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.Query;

import com.jfinal.kit.StrKit;
import com.lambkit.common.bean.ActiveRecordBean;
import com.lambkit.common.bean.RpcBean;
import com.lambkit.common.bean.TableMappingBean;
import com.lambkit.common.bean.TagBean;

public class LambkitManager {

	private LambkitBean info = new LambkitBean();
	private String serverType;
	private int port = -1;

	private static final LambkitManager me = new LambkitManager();

	private LambkitManager() {
	}

	public static LambkitManager me() {
		return me;
	}

	public void init() {
		info.afterJFinalStart();
	}

	public LambkitBean getInfo() {
		return info;
	}

	public void addTag(String name, TagBean info) {
		getInfo().addTag(name, info);
	}

	public void addRpc(RpcBean rpc) {
		getInfo().addRpc(rpc);
	}

	public void addActiveRecord(ActiveRecordBean ari) {
		getInfo().addActiveRecord(ari);
	}

	public void addMapping(TableMappingBean mapping) {
		getInfo().addMapping(mapping);
	}

	public String getServerType() {
		return serverType;
	}

	public void setServerType(String serverType) {
		this.serverType = serverType;
		if("undertow".equals(serverType)) {
			port = getUndertowPort();
		} else {//if("tomcat".equals(serverType)) 
			port = getLocalPort();
		} 
	}

	public int getPort() {
		if(port==-1) {
			if("undertow".equals(serverType)) {
				port = getUndertowPort();
			} else {//if("tomcat".equals(serverType)) 
				port = getLocalPort();
			} 
		}
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * 获取当前机器端口号
	 * 
	 * @throws MalformedObjectNameException
	 * @throws MBeanException
	 * @throws ReflectionException
	 * @throws AttributeNotFoundException
	 * @throws InstanceNotFoundException
	 */
	public int getLocalPort() {
		try {
			MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
			Set<ObjectName> objectNames = mBeanServer.queryNames(new ObjectName("*:type=Connector,*"), null);
			if (objectNames == null || objectNames.size() <= 0) {
				//throw new IllegalStateException("Cannot get the names of MBeans controlled by the MBean server.");
			}
			for (ObjectName objectName : objectNames) {
				String protocol = String.valueOf(mBeanServer.getAttribute(objectName, "protocol"));
				String port = String.valueOf(mBeanServer.getAttribute(objectName, "port"));
				// windows下属性名称为HTTP/1.1, linux下为org.apache.coyote.http11.Http11NioProtocol
				if (protocol.equals("HTTP/1.1") || protocol.equals("org.apache.coyote.http11.Http11NioProtocol")) {
					return Integer.valueOf(port);
				}
			}
			//throw new IllegalStateException("Failed to get the HTTP port of the current server");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getUndertowPort() {
		try {
			// 通过Query匹配查询，找到含有端口信息的节点
			Set<ObjectName> objectNames = ManagementFactory.getPlatformMBeanServer()
					.queryNames(new ObjectName("*:type=Xnio,*"),
							Query.match(Query.attr("BindAddress"), Query.value("*:*")));
			if (objectNames == null || objectNames.size() <= 0) {
				return 0;
			}
			String keyProp = 
					objectNames.iterator().next().getKeyProperty("address");
			if(StrKit.isBlank(keyProp)) return 0;
			// 从节点中取出端口信息
			keyProp = keyProp.replace("\"", "");
			String portStr = keyProp.substring(keyProp.lastIndexOf(":") + 1);
			return Integer.valueOf(portStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
