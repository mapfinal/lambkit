package com.lambkit.core.mq;

/**
 * 消息
 * @author yangyong
 *
 */
public interface Mq {

	public void addSender(Sender<?> sender);
	
	public void addReceiver(Receiver<?> receiver);
	
	public Sender<?> getSender(String name);
	
	public Receiver<?> getReceiver(String name);
	
	public void removeSender(String name);

	public void removeAllSender();
	
	public void removeReceiver(String name);
	
	public void removeAllReceiver();
}
