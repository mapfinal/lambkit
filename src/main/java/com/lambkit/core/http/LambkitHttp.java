package com.lambkit.core.http;

/**
 * http客户端
 * @author yangyong
 *
 */
public interface LambkitHttp {

	LambkitHttpResponse execute(LambkitHttpRequest request);
}
