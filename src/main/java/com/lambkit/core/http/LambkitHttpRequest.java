package com.lambkit.core.http;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.kit.Kv;

public class LambkitHttpRequest {

	// 访问地址
	private String url;
	// 请求方法
	private String method = RequestMethod.GET.name();
	// 编码
	private String charset = "UTF-8";
	// 请求头
	private Map<String, String> headers;
	// 请求参数
    private Map<String, Object> params; 
    // 用户
    private String username = null;
    // 密码
    private String password = null;
    // 表单
    private boolean multipartFormData = false;
    // 证书
    private String certPath;
    private String certPass;
    // 读取超时时间
    private int readTimeOut = 10000;//10秒
    // 连接超时时间
    private int connectTimeOut = 5000;//5秒
    // 类型
    private String contentType;
    // 文件
    private File downloadFile;
    // 其他
	private Kv attrs;
	
	public static final String CONTENT_TYPE_TEXT = "text/plain; charset=utf-8";
    public static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";
    public static final String CONTENT_TYPE_URL_ENCODED = "application/x-www-form-urlencoded; charset=utf-8";
	
	private HttpServletRequest httpServletRequest = null;
	
	public LambkitHttpRequest() {
	}
	
	public LambkitHttpRequest(HttpServletRequest request) {
		httpServletRequest = request;
		if(httpServletRequest!=null) {
			method = httpServletRequest.getMethod();
		}
	}
	
	public void setHeader(String key, String value) {
		if (headers == null) {
            headers = new LinkedHashMap<>();
        }
        headers.put(key, value);
	}
	
	public String getHeader(String key) {
		return headers==null ? null : headers.get(key);
	}
	
	public void addParam(String key, Object value) {
        if (params == null) {
            params = new LinkedHashMap<>();
        }
        if (value instanceof File) {
            setMultipartFormData(true);
        }
        params.put(key, value);
    }

    public void addParams(Map<String, Object> map) {
        if (params == null) {
            params = new LinkedHashMap<>();
        }
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (entry.getValue() == null) {
                continue;
            }
            if (entry.getValue() instanceof File) {
                setMultipartFormData(true);
            }

            params.put(entry.getKey(), entry.getValue());
        }
    }

	
	/**
	 * 设置属性
	 * @param key
	 * @param value
	 */
	public void setAttr(String key, Object value) {
		if(attrs==null) {
			attrs = Kv.by(key, value);
		} else {
			attrs.set(key, value);
		}
	}
	
	public <T>T getAttr(String key) {
		return attrs==null ? null : attrs.getAs(key);
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public boolean isMultipartFormData() {
		return multipartFormData;
	}

	public void setMultipartFormData(boolean multipartFormData) {
		this.multipartFormData = multipartFormData;
	}

	public String getCertPath() {
		return certPath;
	}

	public void setCertPath(String certPath) {
		this.certPath = certPath;
	}

	public String getCertPass() {
		return certPass;
	}

	public void setCertPass(String certPass) {
		this.certPass = certPass;
	}

	public int getReadTimeOut() {
		return readTimeOut;
	}

	public void setReadTimeOut(int readTimeOut) {
		this.readTimeOut = readTimeOut;
	}

	public int getConnectTimeOut() {
		return connectTimeOut;
	}

	public void setConnectTimeOut(int connectTimeOut) {
		this.connectTimeOut = connectTimeOut;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public File getDownloadFile() {
		return downloadFile;
	}

	public void setDownloadFile(File downloadFile) {
		this.downloadFile = downloadFile;
	}

	public Kv getAttrs() {
		return attrs;
	}

	public void setAttrs(Kv attrs) {
		this.attrs = attrs;
	}

	public HttpServletRequest getHttpServletRequest() {
		return httpServletRequest;
	}

	public void setHttpServletRequest(HttpServletRequest httpServletRequest) {
		this.httpServletRequest = httpServletRequest;
		if(httpServletRequest!=null) {
			method = httpServletRequest.getMethod();
		}
	}
}
