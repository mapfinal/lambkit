package com.lambkit.core.http.result;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.lambkit.common.ResultKit;
import com.lambkit.common.util.AppUtils;

public class HttpResultInterceptor implements Interceptor {

	private String publicKey;
	private int signSeconds = 300000;//5 * 60 * 1000;5分钟
	
	public HttpResultInterceptor(String publicKey) {
		// TODO Auto-generated constructor stub
		this.publicKey = publicKey;
	}
	
	public HttpResultInterceptor(String publicKey, int signSeconds) {
		// TODO Auto-generated constructor stub
		this.publicKey = publicKey;
		this.signSeconds = signSeconds;
	}
	
	@Override
	public void intercept(Invocation inv) {
		// TODO Auto-generated method stub
		if(StrKit.notBlank(publicKey)) {
			Controller ctl = inv.getController();
			String accessToken = ctl.getHeader("token");
			accessToken = StrKit.notBlank(accessToken) ? accessToken : ctl.getPara("tk", ctl.getPara("token", ctl.getPara("accessToken")));
			String sign = accessToken;
			
        	String timestamp = ctl.getHeader("requestTime");
        	timestamp = StrKit.notBlank(timestamp) ? timestamp : ctl.getPara("timestamp", ctl.getPara("requestTime"));
        	String appid = ctl.getHeader("appid");
        	appid = StrKit.notBlank(appid) ? appid : ctl.getPara("appid");
        	if(StrKit.isBlank(timestamp) || StrKit.isBlank(sign) || StrKit.isBlank(appid)) {
        		// 返回错误消息
        		ctl.renderJson(ResultKit.json(0, "invalid param", null));
                return;
        	}
        	
        	long reqeustInterval = System.currentTimeMillis() - Long.valueOf(timestamp);
        	if(reqeustInterval > signSeconds) {
        		// 请求过期，请重新请求
        		ctl.renderJson(ResultKit.json(0, "timeout", null));
                return;
        	}
        	
        	if(validationSign(appid, publicKey, timestamp, sign)) {
    			inv.invoke();
    		} else {
    			// accessToken 已经失效!
    			ctl.renderJson(ResultKit.json(0, "invalid token", null));
    		}
		} else {
			inv.invoke();
		}
	}
	
	private boolean validationSign(String appId, String appSecret, String timestamp, String sign) {
		String signature = AppUtils.genSign(appId, appSecret, timestamp);
		return signature.equals(sign);
	}

}
