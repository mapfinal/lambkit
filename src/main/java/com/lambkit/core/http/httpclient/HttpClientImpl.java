package com.lambkit.core.http.httpclient;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.HttpClientBuilder;

import com.lambkit.core.http.LambkitHttp;
import com.lambkit.core.http.LambkitHttpRequest;
import com.lambkit.core.http.LambkitHttpResponse;

public class HttpClientImpl implements LambkitHttp {

	// true, shiro of upms success
	protected boolean doPreserveCookies = true;
	// 重定向
	protected boolean doHandleRedirects = false;
	//保持长连接
	protected boolean doKeepAlive = false;

	private HttpClient httpClient = null;
	
	@Override
	public LambkitHttpResponse execute(LambkitHttpRequest request) {
		// TODO Auto-generated method stub
		if(httpClient==null) httpClient = createHttpClient(request);
		
		return null;
	}

	protected HttpClient createHttpClient(LambkitHttpRequest request) {
		RequestConfig.Builder builder = RequestConfig.custom().setRedirectsEnabled(doHandleRedirects)
				.setCookieSpec(CookieSpecs.IGNORE_COOKIES) // we handle them in the servlet instead
				.setConnectTimeout(request.getConnectTimeOut()).setSocketTimeout(request.getReadTimeOut());
		RequestConfig requestConfig = builder.build();
		HttpClientBuilder httpBuilder = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig);
		if (doKeepAlive) {
			httpBuilder.setKeepAliveStrategy(DefaultConnectionKeepAliveStrategy.INSTANCE);
		}
		return httpBuilder.build();
	}

}
