package com.lambkit.core.http;

public enum RequestMethod {
	GET,
	POST,
	PUT,
	DELETE
}
