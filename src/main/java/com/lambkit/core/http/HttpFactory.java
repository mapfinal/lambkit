package com.lambkit.core.http;

import java.util.Map;

public interface HttpFactory {

	String get(String url, Map<String, String> headers, Map<String, Object> params);
	
	String post(String url, Map<String, String> headers, Map<String, Object> params, Map<String, Object> formData);
	
	String postBody(String url, Map<String, String> headers, Map<String, Object> params, String rawData);
	
	String postBody(String url, Map<String, String> headers, Map<String, Object> params, String rawData, String type);
	
	String upload();
	
	String download();
	
}
