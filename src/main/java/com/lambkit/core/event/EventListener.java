package com.lambkit.core.event;

/**
 * 时间监听
 * @author yangyong
 *
 */
public interface EventListener {
	public void onEvent(Event event);
}
