package com.lambkit.core.api;

/**
 * 用户验证服务
 * key+secret+time=sign（secret不参与传输）
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.core.api
 */
public interface TokenService {

	public Token createToken();

	public Token getToken(String token);
	
	public boolean check(String token, String assist);
}
