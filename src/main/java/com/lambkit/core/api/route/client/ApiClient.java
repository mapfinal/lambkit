package com.lambkit.core.api.route.client;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiClient {
	String value() default "";
	int auth() default 0;//0-unlogin,1-remember,2-login
	String role() default "";//role=admin,guest or role=!admin,
	String rule() default "";
}
