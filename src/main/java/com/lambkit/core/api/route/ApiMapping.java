package com.lambkit.core.api.route;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.core.api.route
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiMapping {
	String value();
	int auth() default 0;//0-unlogin,1-remember,2-login
	String role() default "";//role=admin,guest or role=!admin,
	String rule() default "";
}
