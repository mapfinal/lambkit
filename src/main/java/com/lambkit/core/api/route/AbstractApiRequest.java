package com.lambkit.core.api.route;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.lambkit.Lambkit;
import com.lambkit.common.util.RequestUtils;
import com.lambkit.plugin.jwt.JwtConfig;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.core.api.route
 */
public abstract class AbstractApiRequest {

	/**
	 * 登录的token
	 */
	private String accessToken;
	/**
	 * 用户ip
	 */
	private String clientIp;
	/**
	 * 用户主键
	 */
	private String userId;
	/**
	 * 是否登录
	 * 0-未登录
	 * 1-记忆用户
	 * 2-已登录
	 */
	private int loginStatus = 0;
	/**
	 * 方法对应参数
	 */
	private String params;
	/**
	 * 方法名称
	 */
	private String methodName;
	/**
	 * 其他属性
	 */
	private Kv attr;
	
	public AbstractApiRequest() {
		attr = new Kv();
	}
	
	/**
     * Get cookie value by cookie name.
     */
    public String getCookie(HttpServletRequest request, String name) {
        Cookie cookie = getCookieObject(request, name);
        return cookie != null ? cookie.getValue() : null;
    }

    /**
     * Get cookie object by cookie name.
     */
    public Cookie getCookieObject(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null)
            for (Cookie cookie : cookies)
                if (cookie.getName().equals(name))
                    return cookie;
        return null;
    }
	
	public AbstractApiRequest build(HttpServletRequest request) {
		String accessToken = request.getParameter("tk");
		accessToken = StrKit.isBlank(accessToken) ? request.getParameter("token") : accessToken;
		accessToken = StrKit.isBlank(accessToken) ? request.getParameter("accessToken") : accessToken;
		accessToken = StrKit.isBlank(accessToken) ? request.getParameter("sessionId") : accessToken;
		accessToken = StrKit.isBlank(accessToken) ? request.getParameter("sessionid") : accessToken;
		if(StrKit.isBlank(accessToken)) {
			JwtConfig config = Lambkit.config(JwtConfig.class);
			accessToken = request.getHeader(config.getHeader());
		}
		accessToken = StrKit.isBlank(accessToken) ? getCookie(request, "JSESSIONID") : accessToken;
		accessToken = StrKit.isBlank(accessToken) ? (String) request.getAttribute("JSESSIONID") : accessToken;
		if(StrKit.isBlank(accessToken)) {
			Subject subject = SecurityUtils.getSubject();
	        Session session = subject.getSession();
	        accessToken = session.getId().toString();
		}
		this.setAccessToken(accessToken);
		String ip = RequestUtils.getIpAddress(request);
		this.setClientIp(ip);
		
		String apiName = request.getParameter(ApiRoute.API_METHOD);
		String apiParam = request.getParameter(ApiRoute.API_PARAMS);
		if(StrKit.isBlank(apiParam)) {
			//在body中读取
			apiParam = com.jfinal.kit.HttpKit.readData(request);
		}
		setMethodName(apiName);
		setParams(apiParam);
		buildExt(request);
		return this;
	}
	
	public abstract void buildExt(HttpServletRequest request);

	public void put(String key, Object value) {
		attr.set(key, value);
	}
	
	public <M>M get(String key) {
		return (M) attr.get(key);
	}
	
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public int getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Kv getAttr() {
		return attr;
	}

	public void setAttr(Kv attr) {
		this.attr = attr;
	}
}
