package com.lambkit.core.api.route.client;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.util.EntityUtils;

import com.jfinal.log.Log;
import com.lambkit.component.httpclient.HttpClientFactory;
import com.lambkit.component.httpclient.LambkitResponseHandler;

public class ApiRouteHttpService {
	static Log log = Log.getLog(ApiRouteHttpService.class);

	private String targetUrl;
	
	public ApiRouteHttpService(String targetUrl) {
		this.targetUrl = targetUrl;
	}
	
	/**
	 * HttpClient Get请求
	 * @param clazz
	 * @param uri 请求相对路径
	 * @param keyValues 例如： name=张三
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T>T get(Class<?> clazz, String uri, String...keyValues) {
		
		CloseableHttpResponse response = null;

		uri = urlAddPara(uri, keyValues);
    	HttpGet httpGet = new HttpGet(targetUrl+uri);
    	try {
			response = HttpClientFactory.getHttpClient().execute(httpGet, HttpClientContext.create());
			return new LambkitResponseHandler<T>((Class<T>) clazz).handleResponse(response);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return null;
	}
	
	/**
	 * HttpClient Post 请求
	 * @param clazz
	 * @param uri
	 * @param nvps
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T>T post(Class<?> clazz, String uri, List<NameValuePair> nvps) {
		
		 CloseableHttpResponse response = null;
		try {

	    	HttpPost httpPost = new HttpPost(targetUrl+uri);
	    	httpPost.setEntity(new UrlEncodedFormEntity(nvps));
	    	return new LambkitResponseHandler<T>((Class<T>) clazz).handleResponse(response);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return null;
	}
	
	/**
	 * HttpClient Get请求
	 * @param clazz
	 * @param uri 请求相对路径
	 * @param keyValues 例如： name=张三
	 * @return json
	 */
	public String get(String uri, String...keyValues) {
		
		 CloseableHttpResponse response = null;
		 String json = null;

		uri = urlAddPara(uri, keyValues);
    	HttpGet httpGet = new HttpGet(targetUrl+uri);
		
    	try {
    		
			response = HttpClientFactory.getHttpClient().execute(httpGet, HttpClientContext.create());
			HttpEntity entity = response.getEntity();
			json =  entity != null ? EntityUtils.toString(entity) : null;
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	return json;
    	
	}
	
	/**
	 * HttpClient Post 请求
	 * @param clazz
	 * @param uri
	 * @param nvps
	 * @return
	 */
	public String post(String uri, List<NameValuePair> nvps) {
		
		 CloseableHttpResponse response = null;
		 String json = null;
		try {

	    	HttpPost httpPost = new HttpPost(targetUrl+uri);
	    	httpPost.setEntity(new UrlEncodedFormEntity(nvps));
	    	
			response = HttpClientFactory.getHttpClient().execute(httpPost);
			
			HttpEntity entity = response.getEntity();
			json =  entity != null ? EntityUtils.toString(entity) : null;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return json;
	}
	
	/**
	 * 拼接get请求参数
	 * @param uri
	 * @param keyValues
	 * @return
	 */
	private String urlAddPara(String uri, String...keyValues) {
		
		if (keyValues.length == 0) return uri;
		
		if (uri.contains("?")) {
			for (int i=0; i < keyValues.length; i++) {
				uri += "&" + keyValues[i];
			}	
		} else {
			uri += "?" + keyValues[0];
			for (int i=1; i < keyValues.length; i++) {
				uri += "&" + keyValues[i];
			}
		}
		
		return uri;
	}
	
}
