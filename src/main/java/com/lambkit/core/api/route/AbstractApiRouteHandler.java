package com.lambkit.core.api.route;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
import com.jfinal.log.Log;
import com.lambkit.Lambkit;
import com.lambkit.core.aop.AopKit;
import com.lambkit.core.api.route.render.ApiRenderJson;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.core.api.route
 */
public abstract class AbstractApiRouteHandler extends Handler {
	static Log log = Log.getLog(AbstractApiRouteHandler.class);

	private String targetName = "/api";
	private String supportRequestMethod = "POST";//POST/GET/ALL

	public AbstractApiRouteHandler(String targetName) {
		this.targetName = targetName;
		ApiRoute.me().setTargetName(targetName);
	}
	
	/**
	 * 签名验证
	 * @param apiRequest
	 * @param apiAction
	 * @param request
	 * @param response
	 * @return
	 * @throws ApiException
	 */
	protected abstract AbstractApiRequest AccessSignCheck(AbstractApiRequest apiRequest, ApiAction apiAction, HttpServletRequest request, HttpServletResponse response) throws ApiException;

	/**
	 * 构建apiRequest对象
	 * 
	 * @param request
	 * @return
	 */
	protected abstract AbstractApiRequest buildApiRequest(HttpServletRequest request);
	
	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		// TODO Auto-generated method stub
		if (targetName.equals(target)) {
			//System.out.println("Api handle: " + target);
			isHandled[0] = true;
			if(supportRequestMethod.equals("ALL") || request.getMethod().equals(supportRequestMethod)) {
				handleApiRequest(target, request, response);
			} else {
				ApiResult result = new ApiResult(0, "请求方法不支持", "request method unsupport");
				returnResult(result, null, request, response);
			}
		} else {
			next.handle(target, request, response, isHandled);
		}
	}

	private void handleApiRequest(String target, HttpServletRequest request, HttpServletResponse response) {
		// 系统参数验证
		String params = null;
		String method = null;
		ApiResult result;

		ApiAction apiAction = null;
		AbstractApiRequest apiRequest = null;
		try {
			// 构建apiRequest
			apiRequest = buildApiRequest(request);
			if(apiRequest!=null) {
				params = apiRequest.getParams();
				method = apiRequest.getMethodName();
			}
			// 校验请求参数
			apiAction = requestParamsValidate(apiRequest);
			// 签名验证
			apiRequest = AccessSignCheck(apiRequest, apiAction, request, response);
			log.info("请求接口={" + method + "} 参数=" + params + "");
			Object[] args = ApiRoute.me().getParamsBuilder().buildParams(apiAction, params, request, apiRequest);
			ApiInvocation inv = new ApiInvocation(apiAction, request, args);
			if (Lambkit.isDevMode()) {
				if (ApiActionReporter.isReportAfterInvocation(request)) {
					inv.invoke();
					ApiActionReporter.report(target, request, apiAction);
				} else {
					ApiActionReporter.report(target, request, apiAction);
					inv.invoke();
				}
			}
			else {
				inv.invoke();
			}
			Object error = inv.getErrorValue();
			Object data = inv.getReturnValue();
			if(error!=null) {
				result = ApiResult.fail("校验失败", data).setError(error);
			} else {
				result = ApiResult.ok(data);
			}
		} catch (ApiException e) {
			response.setStatus(500);// 封装异常并返回
			log.error("调用接口={" + method + "}异常  参数=" + params + "", e);
			result = handleError(e);
		} catch (Exception e) {
			response.setStatus(500);// 封装业务异常并返回
			log.error("其他异常", e);
			result = handleError(e);
		}

		// 统一返回结果
		returnResult(result, apiAction, request, response);
	}
	
	/**
	 * 校验请求参数
	 * @param request
	 * @return
	 * @throws ApiException
	 */
	protected ApiAction requestParamsValidate(AbstractApiRequest apiRequest) throws ApiException {
		String apiName = apiRequest.getMethodName();
		String apiParam = apiRequest.getParams();
		ApiStore apiStore = ApiRoute.me().getApiStore();
		ApiAction apiRunnable;
		if (apiName == null || apiName.trim().equals("")) {
			throw new ApiException("调用失败：参数'method'为空");
		} else if (apiParam == null) {
			throw new ApiException("调用失败：参数'params'为空");
		} else if ((apiRunnable = apiStore.findApiRunnable(apiName)) == null) {
			throw new ApiException("调用失败：指定API不存在，API:" + apiName);
		}
		return apiRunnable;
	}

	/**
	 * 处理异常
	 *
	 * @param throwable
	 * @return
	 */
	protected ApiResult handleError(Throwable throwable) {
		ApiResult result;
		if (throwable instanceof ApiException) {
			result = ApiResult.by((ApiException) throwable);
		} else {
			result = ApiResult.fail(throwable.getMessage(), null);
		}
		//ByteArrayOutputStream out = new ByteArrayOutputStream();
		//PrintStream stream = new PrintStream(out);
		//throwable.printStackTrace(stream);
		return result;
	}


	/**
	 * 构建响应结果
	 *
	 * @param result
	 * @param response
	 */
	protected void returnResult(ApiResult result, ApiAction action, HttpServletRequest request, HttpServletResponse response) {
		if(action==null || action.getBody()==null) {
			AopKit.singleton(ApiRenderJson.class).Render(result, request, response);
		} else {
			ApiBody body = action.getBody();
			ApiRender render = AopKit.singleton(body.value());
			render.setView(body.view());
			render.Render(result, request, response);
		}
	}

	public String getSupportRequestMethod() {
		return supportRequestMethod;
	}

	public void setSupportRequestMethod(String supportRequestMethod) {
		this.supportRequestMethod = supportRequestMethod;
	}

}
