
##启动流程
ApiRoute.me().onStart();

- ApiStore搜索ServiceManager中的service加载@ApiMapping的方法
- 

##执行流程

- ApiRouteHandler进入
- 校验请求参数，从ApiStore中获取ApiAction
- 构建ApiRequest
- 签名验证，用户登录验证
- ApiRoute中进行参数构建buildParams
- 生成ApiInvocation，并调用方法。
- 获取返回值ApiResult
- 统一返回结果ApiRender


##启动配置

```java
public class ApiRouteApplication extends LambkitApplicationContext {

	@Override
	public void configModule(LambkitModule module) {
		// TODO Auto-generated method stub
		ServiceManager.me().mapping(GoodsService.class, GoodsServiceImpl.class, null);
	}
	
	@Override
	public void configRoute(Routes routes) {
		// TODO Auto-generated method stub
		super.configRoute(routes);
		routes.add("/test/api", ApiRouteController.class, "/lambkit/test/api");
	}

	@Override
	public void configHandler(Handlers handlers) {
		// TODO Auto-generated method stub
		super.configHandler(handlers);
		handlers.add(ApiRoute.me().getHandler("/api"));
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		//ApiInterceptorManager.me().addGlobalServiceInterceptor(inters);
		ApiRoute.me().onStart();
	}
	
	public static void main(String[] args) {
		LambkitApplication.run(ApiRouteApplication.class, args);
	}
}
```