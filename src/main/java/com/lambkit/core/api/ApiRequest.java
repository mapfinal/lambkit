package com.lambkit.core.api;

import javax.servlet.http.HttpServletRequest;

import com.lambkit.core.api.route.AbstractApiRequest;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.core.api.route
 */
public class ApiRequest extends AbstractApiRequest {

	private String sign;
	private String uCode;
	private String eCode;
	private String timestamp;
	
	@Override
	public void buildExt(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getuCode() {
		return uCode;
	}

	public void setuCode(String uCode) {
		this.uCode = uCode;
	}

	public String geteCode() {
		return eCode;
	}

	public void seteCode(String eCode) {
		this.eCode = eCode;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}
