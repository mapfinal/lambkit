package com.lambkit.core.config;

import java.util.List;

import com.lambkit.component.consul.Consuls;

/**
 * 未完成，ConsulClient未完成，kv存储方式还没有了解清楚
 * @author yangyong
 *
 */
public class ConsulConfigCenter implements ConfigCenter {

	
	@Override
	public String getValue(String key) {
		// TODO Auto-generated method stub
		return Consuls.use().getKV(key);
	}

	@Override
	public boolean containsKey(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<String> getKeys() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean refresh() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setValue(String key, String value) {
		// TODO Auto-generated method stub
		
	}

}
