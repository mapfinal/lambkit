package com.lambkit.core.config;

import java.util.List;

public interface ConfigCenter {

	public String getValue(String key);
    
    public boolean containsKey(String key);
    
    public List<String> getKeys();
    /**
     * 更新、刷新、重启配置
     * @return
     */
    public boolean refresh();
    
    /**
     * 写入配置
     * @param key
     * @param value
     */
    public void setValue(String key, String value);
}
