package com.lambkit.core.gateway;

import java.util.List;

import com.google.common.collect.Lists;
import com.jfinal.kit.StrKit;

public class RouteServiceInstance {
	// 服务实例类型-aciton、apiroute
	private String type;
	// 服务实例名称
	private String name;
	// 服务实例地址
	private String ip;
	// 实例计算机名称
	private String node;
	// 服务实例端口
	private int port;
	// ip + ":" + port + "-" + name
	private String id;
	// 标签
	private List<String> tags;
	// 服务实例地址-http://host:port/address
	private String url;
	
	public RouteServiceInstance(String name, String ip, int port) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.ip = ip;
		this.port = port;
	}

	public void addTag(String tag) {
		if (StrKit.isBlank(tag))
			return;
		if (tags == null) {
			tags = Lists.newArrayList();
		}
		tags.add(tag);
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
