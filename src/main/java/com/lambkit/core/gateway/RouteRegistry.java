package com.lambkit.core.gateway;

/**
 * 路由注册
 * @author yangyong
 *
 */
public interface RouteRegistry {
	void handle(RouteServiceInstance route);
}
