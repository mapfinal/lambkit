package com.lambkit.core.gateway;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lambkit.component.consul.ConsulService;
import com.lambkit.component.consul.Consuls;

public class RouteDiscoverConsulImpl implements RouteDiscover {

	@Override
	public RouteServiceInstance handle(Route route, String target, HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		String instanceName = Consuls.getInstanceName();
		String serviceId = Consuls.getServiceName("http", "action", target);
		ConsulService service = Consuls.use().discover(instanceName, serviceId);
		RouteServiceInstance rsi = new RouteServiceInstance(service.getName(), service.getIp(), service.getPort());
		rsi.setId(service.getId());
		rsi.setType("action");
		rsi.setNode(service.getNode());
		rsi.setUrl(service.getUrl());
		rsi.setTags(service.getTags());
		return rsi;
	}

}
