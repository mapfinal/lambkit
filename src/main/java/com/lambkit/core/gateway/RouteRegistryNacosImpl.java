package com.lambkit.core.gateway;

import com.alibaba.nacos.api.exception.NacosException;
import com.lambkit.component.nacos.Nacos;

public class RouteRegistryNacosImpl implements RouteRegistry {

	@Override
	public void handle(RouteServiceInstance route) {
		// TODO Auto-generated method stub
		if(route==null) {
			return;
		}
		String serviceName = Nacos.getServiceName(route);
		try {
			Nacos.use().registerInstance(serviceName);
		} catch (NacosException e) {
			e.printStackTrace();
		}
	}

}
