package com.lambkit.core.gateway;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.kit.Kv;

public class RouteInvocation {

	private Route route;
	private RouteServiceInstance routeService;
	private RouteInterceptor[] interceptors;
	private String targetUrl;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private int index = 0;

	private boolean result = false;
	private Kv resultData;

	public RouteInvocation(String targetUrl, RouteInterceptor[] interceptors, HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated constructor stub
		this.targetUrl = targetUrl;
		this.interceptors = interceptors;
		this.request = request;
		this.response = response;
	}

	public void routing(Route route, RouteServiceInstance routeService) {
		this.route = route;
		this.routeService = routeService;
	}

	public void invoke() {
		if (index < interceptors.length) {
			interceptors[index++].intercept(this);
		} else if (index++ == interceptors.length) {
			// 暂时不处理
		}
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public RouteServiceInstance getRouteService() {
		return routeService;
	}

	public void setRouteService(RouteServiceInstance routeService) {
		this.routeService = routeService;
	}

	public RouteInterceptor[] getInterceptors() {
		return interceptors;
	}

	public void setInterceptors(RouteInterceptor[] interceptors) {
		this.interceptors = interceptors;
	}

	public String getTargetUrl() {
		return targetUrl;
	}

	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public boolean isResult() {
		return this.result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public Kv getResultData() {
		if (resultData == null) {
			resultData = new Kv();
		}
		return resultData;
	}

	public void setResultData(Kv resultData) {
		this.resultData = resultData;
	}

}
