package com.lambkit.core.gateway;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.lambkit.component.consul.Consuls;
import com.lambkit.component.nacos.Nacos;

public class RouteDiscoverNacosImpl implements RouteDiscover {

	@Override
	public RouteServiceInstance handle(Route route, String target, HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		String serviceName = Consuls.getServiceName("http", "action", target);
		RouteServiceInstance serviceInstance = null;
		try {
			//根据负载均衡算法随机获取一个健康实例
			Instance instance = Nacos.use().selectOneHealthyInstance(serviceName);
			serviceInstance = new RouteServiceInstance(serviceName, instance.getIp(), instance.getPort());
		} catch (NacosException e) {
			e.printStackTrace();
		}
		return serviceInstance;
	}

}
