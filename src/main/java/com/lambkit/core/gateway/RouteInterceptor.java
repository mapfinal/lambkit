package com.lambkit.core.gateway;

/**
 * 路由拦截器
 * @author yangyong
 *
 */
public interface RouteInterceptor {
	void intercept(RouteInvocation route);
}
