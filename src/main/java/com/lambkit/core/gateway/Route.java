package com.lambkit.core.gateway;

import com.lambkit.core.limiter.LimitFallbackStrategy;
import com.lambkit.core.limiter.LimiterType;

public class Route {
	/**
	 * id
	 */
	private String id;//id=serviceId

	/**
	 * 路由类型，本地-local, 网关路由-cluster
	 */
	private String type;
	
	/**
	 * 识别地址
	 */
	private String urlpattern;

	/**
	 * 目标地址
	 */
	private String uri;

	
	/**
	 * loadbalance weight 负载均衡权重
	 */
	private int weight;

	/**
	 * loadbalance 类型
	 */
	private String lbtype;
	/**
	 * 一定时间内最多访问次数，默认50,每秒创建令牌个数，默认:10
	 */
	private int rate = -1;

	/**
	 * 给定的时间范围 单位(秒) 默认1,获取令牌等待超时时间 默认:500
	 */
	private int period = -1;

	/**
	 * 限流的类型
	 */
	private LimiterType limitType = LimiterType.REDIS_TOKEN_BUCKET;

	/**
	 * 降级策略:默认快速失败
	 */
	private LimitFallbackStrategy fallbackStrategy = LimitFallbackStrategy.FAIL_FAST;

	/**
	 * 当降级策略为:回退 时回退处理接口实现名称
	 */
	private String fallbackImpl;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrlpattern() {
		return urlpattern;
	}

	public void setUrlpattern(String urlpattern) {
		this.urlpattern = urlpattern;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public LimiterType getLimitType() {
		return limitType;
	}

	public void setLimitType(LimiterType limitType) {
		this.limitType = limitType;
	}

	public LimitFallbackStrategy getFallbackStrategy() {
		return fallbackStrategy;
	}

	public void setFallbackStrategy(LimitFallbackStrategy fallbackStrategy) {
		this.fallbackStrategy = fallbackStrategy;
	}

	public String getFallbackImpl() {
		return fallbackImpl;
	}

	public void setFallbackImpl(String fallbackImpl) {
		this.fallbackImpl = fallbackImpl;
	}

	public String getLbtype() {
		return lbtype;
	}

	public void setLbtype(String lbtype) {
		this.lbtype = lbtype;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
