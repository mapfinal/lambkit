package com.lambkit.core.gateway;

import com.lambkit.component.consul.ConsulConstants;
import com.lambkit.component.consul.ConsulService;
import com.lambkit.component.consul.Consuls;
import com.lambkit.core.api.route.ApiRoute;
import com.lambkit.distributed.node.NodeManager;
import com.lambkit.distributed.node.info.Node;

public class RouteRegistryConsulImpl implements RouteRegistry {

	@Override
	public void handle(RouteServiceInstance route) {
		// TODO Auto-generated method stub
		String instanceName = Consuls.getInstanceName();
		String serviceId = Consuls.getServiceName(route);
		
		String ip = route.getIp();
		int port = route.getPort();
		StringBuffer strb = new StringBuffer("http://");
		strb.append(ip).append(":").append(port);
		String address = strb.toString();
		String key = route.getName();
		
		ConsulService service = new ConsulService(instanceName, ip, port);
		service.setId(serviceId);
		Node node = NodeManager.me().getNode();
		service.setNode(node.getHost());
		service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_PROTOCOL + "http");
		if("apiroute".equals(route.getType())) {
			service.setUrl(address + ApiRoute.me().getTargetName() + "?method=" + key);
			service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_TYPE + "apiroute");
		} else {
			service.setUrl(address + key);
			service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_TYPE + "action");
		}
		service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_URL + address + key);
		Consuls.use().registry(service);
	}

}
