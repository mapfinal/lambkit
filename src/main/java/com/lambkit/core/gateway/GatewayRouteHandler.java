/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.core.gateway;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.jfinal.handler.Handler;
import com.jfinal.kit.StrKit;
import com.lambkit.Lambkit;
import com.lambkit.common.exception.LambkitException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 单个接口的代理，无法整站代理 
 * An HTTP reverse proxy/gateway JFinal Handler. 
 * It is designed to be extended for customization
 */
public class GatewayRouteHandler extends Handler {
	//private static final Log log = Log.getLog(GatewayRouteHandler.class);
	private static final ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>() {
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
	};
	// 反向代理实现
	protected RouteProxy proxy;
	// 拦截器
	private RouteInterceptor[] interceptors;
	// 路由发现
	private RouteDiscover discover;
	// 网关路由
	private GatewayRoute gatewayRoute;

	public GatewayRouteHandler(RouteInterceptor[] interceptors) {
		this.interceptors = interceptors;
		try {
			YamlReader reader = new YamlReader(new FileReader("gateway.yml"));
			gatewayRoute = reader.read(GatewayRoute.class);
		} catch (YamlException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String proxyType = gatewayRoute!=null ? gatewayRoute.getProxy() : "default";
		if("default".equals(proxyType)) {
			proxy = new RouteProxyHttpClientImpl();
		}
		String discoverType = gatewayRoute!=null ? gatewayRoute.getDiscover() : "nacos";
		if("consul".equals(discoverType)) {
			discover = new RouteDiscoverConsulImpl();
		}else if("nacos".equals(discoverType)) {
			discover = new RouteDiscoverNacosImpl();
		}
	}

	public GatewayRouteHandler(RouteProxy proxy, RouteInterceptor[] interceptors, RouteDiscover discover) {
		this.proxy = proxy;
		this.interceptors = interceptors;
		this.discover = discover;
		try {
			YamlReader reader = new YamlReader(new FileReader("gateway.yml"));
			gatewayRoute = reader.read(GatewayRoute.class);
		} catch (YamlException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		// TODO Auto-generated method stub
		String targetUri = null;
		RouteInvocation routeAction = new RouteInvocation(target, interceptors, request, response);
		if (gatewayRoute != null) {
			Route route = gatewayRoute.getRoute(target);
			if (route != null) {
				if ("local".equals(route.getType())) {
					targetUri = route.getUri();
					if (targetUri == null) {
						throw new LambkitException("targetUri is required.");
					}
					// String targetName = config.getName();
					String sourceUrlpattern = route.getUrlpattern();
					String turl = targetUri.endsWith("/") ? "" : "/";
					turl = targetUri + turl;
					String surl = target.replace(sourceUrlpattern.replace("*", ""), "");
					turl += surl;
					// String originTargetUri = targetUri;
					if (!targetUri.equals(turl)) {
						targetUri = turl;
					}
					RouteServiceInstance rsi = new RouteServiceInstance(target, "localhost", 0);
					rsi.setUrl(targetUri);
					routeAction.routing(route, rsi);
				} else if (discover != null) {
					RouteServiceInstance routeSI = discover.handle(route, target, request, response);
					if (routeSI == null) {
						// Support four types of url
						// 1: http://abc.com/controllerKey ---> 00
						// 2: http://abc.com/controllerKey/para ---> 01
						// 3: http://abc.com/controllerKey/method ---> 10
						// 4: http://abc.com/controllerKey/method/para ---> 11
						// The controllerKey can also contains "/"
						// Example: http://abc.com/uvw/xyz/method/para
						int i = target.lastIndexOf('/');
						if (i != -1) {
							routeSI = discover.handle(route, target.substring(0, i), request, response);
						}
					}
					if (routeSI != null) {
						// 访问注册中心获取的接口
						targetUri = routeSI.getUrl();
					}
					routeAction.routing(route, routeSI);
				} // else
			} // if
		}

		if (StrKit.notBlank(targetUri)) {
			if (Lambkit.isDevMode()) {
				System.out.println();
				System.out.println("Lambkit http proxy report -------- " + sdf.get().format(new Date())
						+ " -------------------------");
				// System.out.println("http-proxy: " + targetName + " from " + sourceUrlpattern
				// + " to " + targetUri);
				// System.out.println("name : " + targetName);
				System.out.println("from    : " + target);
				// System.out.println("pattern : " + sourceUrlpattern);
				System.out.println("to      : " + targetUri);
				System.out.println("--------------------------------------------------------------------------------");
				/*
				 * StringBuilder sb = new
				 * StringBuilder("\nLambkit http proxy report -------- ").append(sdf.get().
				 * format(new Date())).append(" ------------------------------\n");
				 * sb.append("name : ").append(targetName).append("\n");
				 * sb.append("from : ").append(sourceUrlpattern).append("\n");
				 * sb.append("to   : ").append(targetUri).append("\n"); sb.append(
				 * "--------------------------------------------------------------------------------\n"
				 * );
				 */
			}
			if(routeAction!=null) {
				routeAction.invoke();
			}
			if(!routeAction.isResult()) {
				proxy.service(targetUri, request, response);
			}
			isHandled[0] = true;
			// targetUri = originTargetUri;
		} else {
			next.handle(target, request, response, isHandled);
		}
	}

}
