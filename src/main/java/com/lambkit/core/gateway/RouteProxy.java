package com.lambkit.core.gateway;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 反向代理
 * @author yangyong
 *
 */
public interface RouteProxy {

	void service(String targetUri, HttpServletRequest request, HttpServletResponse response);

}
