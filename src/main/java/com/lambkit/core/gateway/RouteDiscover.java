package com.lambkit.core.gateway;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface RouteDiscover {

	RouteServiceInstance handle(Route route, String target, HttpServletRequest request, HttpServletResponse response);
}
