package com.lambkit.core.gateway;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.lambkit.common.util.StringUtils;

public class GatewayRoute {
	//反向代理default,okhttp
	private String proxy = "default";
	//注册中心consul,nacos
	private String discover="nacos";
	// 路由
	private List<Route> routes = null;
	
	public Route getRoute(String target) {
		if(StrKit.isBlank(target) || routes==null) return null;
		for (Route route : routes) {
			if(route!=null && StringUtils.isMatch(target, route.getUrlpattern())) {
				return route;
			}
		}
    	return null;
    }

	public List<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}

	public String getProxy() {
		return proxy;
	}

	public void setProxy(String proxy) {
		this.proxy = proxy;
	}

	public String getDiscover() {
		return discover;
	}

	public void setDiscover(String discover) {
		this.discover = discover;
	}

}
