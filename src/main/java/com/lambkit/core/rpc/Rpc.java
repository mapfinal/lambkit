package com.lambkit.core.rpc;

/**
 * RPC
 * @author yangyong
 *
 */
public interface Rpc {
	
	public <T> T serviceObtain(Class<T> serviceClass, RpcServiceConfig serviceConfig, String url);

    public <T> T serviceObtain(Class<T> serviceClass, RpcServiceConfig serviceConfig);

    public <T> boolean serviceExport(Class<T> interfaceClass, Object object, RpcServiceConfig serviceConfig);
}
