package com.lambkit.core.rpc.http;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.jfinal.handler.Handler;
import com.jfinal.kit.JsonKit;
import com.lambkit.common.service.ServiceKit;
import com.lambkit.core.aop.AopKit;

public class HttpRpcHandler extends Handler {

	private String targetPrefix = "/lambkit/rpc";

	public HttpRpcHandler() {
		// TODO Auto-generated constructor stub
	}

	public HttpRpcHandler(String targetPrefix) {
		// TODO Auto-generated constructor stub
		this.setTargetPrefix(targetPrefix);
	}

	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		// TODO Auto-generated method stub
		if (target.startsWith(targetPrefix)) {
			isHandled[0] = true;
			/**
			 * 获取 http 请求 body 中的原始数据，通常用于接收 json String 这类数据<br>
			 * 可多次调用此方法，避免掉了 HttpKit.readData(...) 方式获取该数据时多次调用引发的异常
			 */
			String requestRawData = com.jfinal.kit.HttpKit.readData(request);
			HttpRpcRequest rpcReqest = JSON.parseObject(requestRawData, HttpRpcRequest.class);
			HttpRpcResponse rpcResponse = handlerRequest(rpcReqest);
			new HttpResultRender(rpcResponse).render(request, response, "json");
		} else {
			next.handle(target, request, response, isHandled);
		}
	}

	private HttpRpcResponse handlerRequest(HttpRpcRequest rpcReqest) {
		Class<?> targetObject = rpcReqest.getTargetObject();
		Object bean = ServiceKit.inject(targetObject);
		if (bean == null) {
			bean = AopKit.get(targetObject);
		}
		if (bean == null) {
			return HttpRpcResponse.fail(0, "can not find class:" + targetObject.getName());
		}
		Method method;
		try {
			method = targetObject.getDeclaredMethod(rpcReqest.getMethodName(),
					rpcReqest.getParamTypes().toArray(new Class<?>[0]));
			//Object[] args = buildParams(method, rpcReqest);
			List<Object> paramValues = rpcReqest.getParamValues();
			Object[] args =  paramValues.toArray();
			Object data = method.invoke(bean, args);
			return HttpRpcResponse.success(JSON.toJSONString(data));//JsonKit.toJson(data)
		} catch (Exception e) {
			e.printStackTrace();
			return HttpRpcResponse.error(100, "SYSTEM_ERR", e.getMessage());
		}
	}

	/*
	private Object[] buildParams(Method method, HttpRpcRequest rpcReqest) {
		// javassist
		List<String> paramNames = rpcReqest.getParamNames();
		// 反射获取api方法中参数类型
		List<Class<?>> paramTypes = rpcReqest.getParamTypes(); // 反射
		Map<String, Object> paramValues = rpcReqest.getParamValues();

		Object[] args = new Object[paramTypes.size()];
		if (paramTypes.size() < 1) {
			return args;
		}

		for (int i = 0; i < paramTypes.size(); i++) {
			if (paramValues.containsKey(paramNames.get(i))) {
				args[i] = paramValues.get(paramNames.get(i));
//				try {
//					args[i] = json.getObject(paramNames.get(i), paramTypes.get(i));
//				} catch (Exception e) {
//					throw new ApiException("调用失败：指定参数格式错误或值错误‘" + paramNames.get(i) + "’" + e.getMessage());
//				}
			} else {
				args[i] = null;
			}
		}
		return args;
	}*/

	public String getTargetPrefix() {
		return targetPrefix;
	}

	public void setTargetPrefix(String targetPrefix) {
		this.targetPrefix = targetPrefix;
	}

}
