package com.lambkit.core.rpc.http;

import java.io.Serializable;
import java.util.List;

public class HttpRpcRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5063572996177814470L;

	private String traceId;
	private Class<?> targetObject;
	private String methodName;
	private List<String> paramNames;
	private List<Class<?>> paramTypes;
	private List<Object> paramValues;

	public HttpRpcRequest() {
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public Class<?> getTargetObject() {
		return targetObject;
	}

	public void setTargetObject(Class<?> targetObject) {
		this.targetObject = targetObject;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public List<String> getParamNames() {
		return paramNames;
	}

	public void setParamNames(List<String> paramNames) {
		this.paramNames = paramNames;
	}

	public List<Class<?>> getParamTypes() {
		return paramTypes;
	}

	public void setParamTypes(List<Class<?>> paramTypes) {
		this.paramTypes = paramTypes;
	}

	public List<Object> getParamValues() {
		return paramValues;
	}

	public void setParamValues(List<Object> paramValues) {
		this.paramValues = paramValues;
	}

}
