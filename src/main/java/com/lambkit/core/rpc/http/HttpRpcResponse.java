package com.lambkit.core.rpc.http;

import java.io.Serializable;

public class HttpRpcResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8957490581890804102L;

	// 是否成功
	private boolean success = true;

	// 状态码：1成功，其他为失败
	private int code;

	// 成功为success，其他为失败原因
	private String message;

	// 数据的json
	private String data;

	public HttpRpcResponse(boolean success, int code, String message, String data) {
		this.code = code;
		this.message = message;
		this.data = data;
		this.success = success;
	}
	
	public static HttpRpcResponse success(String data) {
		return new HttpRpcResponse(true, 1, "succes", data);
	}
	
	public static HttpRpcResponse fail(int code, String message) {
		return new HttpRpcResponse(false, code, message, null);
	}
	
	public static HttpRpcResponse error(int code, String message, String data) {
		return new HttpRpcResponse(false, code, message, data);
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
