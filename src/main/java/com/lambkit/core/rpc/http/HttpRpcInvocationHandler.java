package com.lambkit.core.rpc.http;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.lambkit.common.util.DateTimeUtils;
import com.lambkit.net.node.LambkitNode;
import com.lambkit.net.node.LambkitNodeManager;

public class HttpRpcInvocationHandler<T> implements InvocationHandler {

	private String url;
	private Class<T> targetObject;

	public HttpRpcInvocationHandler(String url, Class<T> targetObject) {
		this.url = url;
		this.targetObject = targetObject;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// TODO Auto-generated method stub
		//header
		LambkitNode node = LambkitNodeManager.me().getNode();
		String id = node.getId();
		String appKey = node.getKey();
		String secret = LambkitNodeManager.me().getParentNode() !=null ? LambkitNodeManager.me().getParentNode().getId() : null;
		if(StrKit.isBlank(secret)) {
			return null;
		}
		String requestTime = String.valueOf(System.currentTimeMillis());
		Map<String, String> headers = Maps.newHashMap();
		headers.put("nodeId", id);
		headers.put("appKey", appKey);
		headers.put("requestTime", requestTime);
		String sign = DigestUtils.md5Hex(id + appKey + secret + requestTime);
		headers.put("sign", sign);
		
		//data
		HttpRpcRequest rpcRequest = new HttpRpcRequest();
		rpcRequest.setTargetObject(targetObject);
		rpcRequest.setTraceId(getTraceId());
		rpcRequest.setMethodName(method.getName());
		List<String> paramNames = getNames(method);//方法参数名称
		rpcRequest.setParamNames(paramNames);
		Class<?>[] paramTypes = method.getParameterTypes(); // 反射获取api方法中参数类型
		List<Class<?>> paramTypeList = new ArrayList<>(Arrays.asList(paramTypes));
		rpcRequest.setParamTypes(paramTypeList);
		List<Object> paramValues = new ArrayList<>(Arrays.asList(args));
		rpcRequest.setParamValues(paramValues);
		
		//post
		String result = HttpRpcApiKit.postJson(url, headers, JsonKit.toJson(rpcRequest));
		if(StrKit.notBlank(result)) {
			HttpRpcResponse rpcResponse = JsonKit.parse(result, HttpRpcResponse.class);
			if(rpcResponse!=null && rpcResponse.isSuccess()) {
				String data = rpcResponse.getData();
				if (method.getReturnType().equals(void.class)) {
                    return null;
                } else {
                    return JSON.parseObject(data, method.getGenericReturnType());
                }
			}
		}
		return null;
	}

	private String getTraceId() {
		String dateStr = DateTimeUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmssSSS");
		long randomInt = HttpRpcApiKit.getRandom();//ThreadLocalRandom.current().nextInt(10000000);
		String traceId = String.format("lambkit-%s-%s-%07d", "nodeid", dateStr, randomInt);
		return traceId;
	}

	private List<String> getNames(Method method) {
		final int paraCount = method.getParameterCount();
		List<String> resultList = Lists.newArrayList();

		// 无参 action 共享同一个对象，该分支以外的所有 ParaProcessor 都是有参 action，不必进行 null 值判断
		if (paraCount == 0) {
			return resultList;
		}

		Parameter[] paras = method.getParameters();
		for (int i = 0; i < paraCount; i++) {
			Parameter p = paras[i];
			String parameterName = p.getName();
			// System.out.println("method param: " + parameterName);
			resultList.add(parameterName);
		}

		return resultList;
	}
}
