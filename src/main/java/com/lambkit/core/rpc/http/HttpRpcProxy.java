package com.lambkit.core.rpc.http;

import java.lang.reflect.Proxy;

public class HttpRpcProxy {
	@SuppressWarnings("unchecked")
	public static <T>T getObject(String url, Class<T> targetObject) {
		HttpRpcInvocationHandler<T> handler = new HttpRpcInvocationHandler<T>(url, targetObject);
		return (T) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[] { targetObject },
				handler);
	}
}
