/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.core.cache;

import java.util.Map;

import com.google.common.collect.Maps;
import com.lambkit.Lambkit;
import com.lambkit.core.cache.impl.EhcacheCacheImpl;
import com.lambkit.core.cache.impl.RedisCacheImpl;
import com.lambkit.core.cache.impl.RedisHashCacheImpl;

public class CacheManager {

	private static CacheManager me = new CacheManager();

	private CacheManager() {
	}

	public static final String TYPE_EHCACHE = "ehcache";
	public static final String TYPE_REDIS = "redis";
	public static final String TYPE_EHREDIS = "ehredis";
	public static final String TYPE_NONE_CACHE = "nonecache";

	private String defaultCacheType = null;
	private Map<String, BaseCache> caches = Maps.newHashMap();

	public static CacheManager me() {
		return me;
	}

	public BaseCache getCache() {
		if (defaultCacheType == null) {
			CacheConfig config = Lambkit.config(CacheConfig.class);
			defaultCacheType = config.getType();
			BaseCache cache = buildCache(config.getType());
			caches.put(defaultCacheType, cache);
		}
		return caches.get(defaultCacheType);
	}

	public EhcacheCacheImpl getEhCache() {
		BaseCache cache = caches.get(TYPE_EHCACHE);
		if (cache == null) {
			cache = new EhcacheCacheImpl();
			caches.put(TYPE_EHCACHE, cache);
		}
		return (EhcacheCacheImpl) cache;
	}

	public RedisHashCacheImpl getRedis() {
		BaseCache cache = caches.get(TYPE_REDIS);
		if (cache == null) {
			cache = new RedisHashCacheImpl();
			caches.put(TYPE_REDIS, cache);
		}
		return (RedisHashCacheImpl) cache;
	}

	public BaseCache buildCache(String type) {
		switch (type) {
		case TYPE_EHCACHE:
			return new EhcacheCacheImpl();
		case TYPE_REDIS:
			return new RedisCacheImpl();
		case TYPE_NONE_CACHE:
			return new NoneCacheImpl();
		default:
			return new NoneCacheImpl();
		}
	}
}
