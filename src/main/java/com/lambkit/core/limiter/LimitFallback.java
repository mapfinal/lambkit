package com.lambkit.core.limiter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.aop.Invocation;

public interface LimitFallback {

	void handle(String target, HttpServletRequest request, HttpServletResponse response, LimitFallbackStrategy fallbackStrategy);
	
	void handle(Invocation inv, LimitFallbackStrategy fallbackStrategy);
}
