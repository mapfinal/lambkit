/**
 * 限流方法<br/>
 * 
 * <br/>限流器Limiter
 * <br/>限流注解Limit
 * <br/>限流配置LimitConfig
 * <br/>限流管理器LimitManager
 * <br/>限流拦截器LimiterInterceptor，白名单未实现，限流fallback未实现
 * 
 */
package com.lambkit.core.limiter;