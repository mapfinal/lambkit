package com.lambkit.core.limiter;

public class LimitConfig {
    /**
     * 是否开启限流配置，这个的开启或关闭对注解的限流配置不影响
     */
    private boolean enable = false;
    /**
     * Limiter类型
     */
    private LimiterType limiterType = LimiterType.TOKEN_BUCKET;
    /**
     * IP 白名单，不受限流的配置
     */
    private String ipWhitelist;
    /**
     * 默认的降级处理策略
     */
    private LimitFallbackStrategy fallbackStrategy = LimitFallbackStrategy.FAIL_FAST;
    
    /**
     * 默认的降级处理（被限流后的处理器）
     */
    private String fallbackImpl;

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getIpWhitelist() {
		return ipWhitelist;
	}

	public void setIpWhitelist(String ipWhitelist) {
		this.ipWhitelist = ipWhitelist;
	}

	public String getFallbackImpl() {
		return fallbackImpl;
	}

	public void setFallbackImpl(String fallbackImpl) {
		this.fallbackImpl = fallbackImpl;
	}

	public LimiterType getLimiterType() {
		return limiterType;
	}

	public void setLimiterType(LimiterType limiterType) {
		this.limiterType = limiterType;
	}

	public LimitFallbackStrategy getFallbackStrategy() {
		return fallbackStrategy;
	}

	public void setFallbackStrategy(LimitFallbackStrategy fallbackStrategy) {
		this.fallbackStrategy = fallbackStrategy;
	}
}
