package com.lambkit.core.limiter;

/**
 * 限流异常
 * <li></li>
 * @author yangyong
 *
 */
public class LimitException extends RuntimeException {
	/**错误码*/
    protected String code;
    public LimitException() {
    }

    public LimitException(Throwable ex) {
        super(ex);
    }
    public LimitException(String message) {
        super(message);
    }
    public LimitException(String code, String message) {
        super(message);
        this.code = code;
    }
    public LimitException(String message, Throwable ex) {
        super(message, ex);
    }
}
