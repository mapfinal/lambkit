package com.lambkit.core.limiter.redis;

import java.util.concurrent.TimeUnit;

import com.jfinal.plugin.redis.Redis;

/**
 * 分布式限流 令牌桶算法实现类
 * <p>说明:</p>
 * <li>令牌桶。当你需要尽可能的压榨程序的性能（此时桶的最大容量必然会大于等于程序的最大并发能力），并且所处的场景流量进入波动不是很大（不至于一瞬间取完令牌，压垮后端系统）</li>
 * 
 * @author yangyong
 *
 */
public class TokenBucketRedisLimiter extends RedisLimiter {

	protected String luaScript = "--利用redis的hash结构，存储key所对应令牌桶的上次获取时间和上次获取后桶中令牌数量\n" + 
			"local ratelimit_info = redis.pcall('HMGET',KEYS[1],'last_time','current_token_num')\n" + 
			"local last_time = ratelimit_info[1]\n" + 
			"local current_token_num = tonumber(ratelimit_info[2])\n" + 
			"\n" + 
			"redis.replicate_commands()\n" + 
			"local now = redis.call('time')[1]\n" + 
			"redis.call('SET','now',now);\n" + 
			"\n" + 
			"\n" + 
			"\n" + 
			"--tonumber是将value转换为数字，此步是取出桶中最大令牌数、生成令牌的速率(每秒生成多少个)、当前时间\n" + 
			"local max_token_num = tonumber(ARGV[1])\n" + 
			"local token_rate = tonumber(ARGV[2])\n" + 
			"--local current_time = tonumber(ARGV[3])\n" + 
			"local current_time = now\n" + 
			"--reverse_time 即多少毫秒生成一个令牌\n" + 
			"local reverse_time = 1000/token_rate\n" + 
			"\n" + 
			"--如果current_token_num不存在则说明令牌桶首次获取或已过期，即说明它是满的\n" + 
			"if current_token_num == nil then\n" + 
			"  current_token_num = max_token_num\n" + 
			"  last_time = current_time\n" + 
			"else\n" + 
			"  --计算出距上次获取已过去多长时间\n" + 
			"  local past_time = current_time-last_time\n" + 
			"  --在这一段时间内可产生多少令牌\n" + 
			"  local reverse_token_num = math.floor(past_time/reverse_time)\n" + 
			"  current_token_num = current_token_num +reverse_token_num\n" + 
			"  last_time = reverse_time * reverse_token_num + last_time\n" + 
			"  if current_token_num > max_token_num then\n" + 
			"    current_token_num = max_token_num\n" + 
			"  end\n" + 
			"end\n" + 
			"\n" + 
			"local result = 0\n" + 
			"if(current_token_num > 0) then\n" + 
			"  result = 1\n" + 
			"  current_token_num = current_token_num - 1\n" + 
			"end\n" + 
			"\n" + 
			"--将最新得出的令牌获取时间和当前令牌数量进行存储,并设置过期时间\n" + 
			"redis.call('HMSET',KEYS[1],'last_time',last_time,'current_token_num',current_token_num)\n" + 
			"redis.call('pexpire',KEYS[1],math.ceil(reverse_time*(max_token_num - current_token_num)+(current_time-last_time)))\n" + 
			"\n" + 
			"return result\n";
    
    /**
     * 尝试是否能正常执行
     *
     * @param resource      资源名
     * @param rate          限制次数
     * @param periodSeconds 限制时长，单位为秒
     * @return true 可以执行
     * false 限次，禁止
     */
    public boolean tryAcquire(String resource, int rate, int periodSeconds, TimeUnit unit) {
    	Long count = (Long) Redis.use().getJedis().eval(luaScript, 1, resource, String.valueOf(rate), String.valueOf(periodSeconds));
    	if (count != null && count.intValue() == 1) {
            return true;
        } else {
            return false;
        }
    }
}
