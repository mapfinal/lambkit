package com.lambkit.core.limiter.redis;

import java.util.concurrent.TimeUnit;

import com.jfinal.plugin.redis.Redis;

/**
 * 分布式限流 滑动窗口算法实现类
 * <p>说明:</p>
 * <li>滑动窗口。这个方案适用于对异常结果「高容忍」的场景，毕竟相比“两窗”少了一个缓冲区。但是，胜在实现简单</li>
 * @author yangyong
 */
public class SlidingWindowRedisLimiter extends RedisLimiter {
	protected String luaScript = "local function addToQueue(x,time)\n" + 
			"    local count=0\n" + 
			"    for i=1,x,1 do\n" + 
			"        redis.call('lpush',KEYS[1],time)\n" + 
			"        count=count+1\n" + 
			"    end\n" + 
			"    return count\n" + 
			"end\n" + 
			"--返回\n" + 
			"local result=0\n" + 
			"--限流KEY\n" + 
			"local key = KEYS[1]\n" + 
			"--申请数\n" + 
			"local applyCount = tonumber(ARGV[1])\n" + 
			"--阀值数量\n" + 
			"local limit = tonumber(ARGV[2])\n" + 
			"--阀值时间\n" + 
			"local period = tonumber(ARGV[3])\n" + 
			"redis.replicate_commands()\n" + 
			"local now = redis.call('time')[1]\n" + 
			"redis.call('SET','now',now);\n" + 
			"--当前时间\n" + 
			"local current_time = now\n" + 
			"\n" + 
			"local timeBase = redis.call('lindex',key, limit - applyCount)\n" + 
			"if (timeBase == false) or (tonumber(current_time) - tonumber(timeBase) > period) then\n" + 
			"    result = result + addToQueue(applyCount,tonumber(current_time))\n" + 
			"end\n" + 
			"if (timeBase ~= false) then\n" + 
			"    redis.call('ltrim',key,0,limit)\n" + 
			"end\n" + 
			"return result\n";
	
    /**
     * 尝试是否能正常执行
     *
     * @param resource      资源名
     * @param rate          限制次数
     * @param periodSeconds 限制时长，单位为秒
     * @return true 可以执行
     * false 限次，禁止
     */
    public boolean tryAcquire(String resource, int rate, int periodSeconds, TimeUnit unit) {
    	Long count = (Long) Redis.use().getJedis().eval(luaScript, 1, resource, "1", String.valueOf(rate), String.valueOf(periodSeconds));
    	if (count != null && count.intValue() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
