package com.lambkit.core.limiter.redis;

import java.util.concurrent.TimeUnit;

import com.jfinal.plugin.redis.Redis;

/**
 * 分布式限流 固定窗口算法实现类
 * <p>说明:</p>
 * <li>固定窗口，一般来说，如非时间紧迫，不建议选择这个方案，太过生硬。但是，为了能快速止损眼前的问题可以作为临时应急的方案</li>
 * @author yangyong
 */
public class FixedWindowRedisLimiter extends RedisLimiter {
	protected String luaScript = "--限流KEY\n" + 
			"local key = KEYS[1]\n" + 
			"--限流大小\n" + 
			"local limit = tonumber(ARGV[1])\n" + 
			"--时间周期\n" + 
			"local period = tonumber(ARGV[2])\n" + 
			"\n" + 
			"local current = tonumber(redis.call('get', key) or \"0\")\n" + 
			"--如果超出限流大小\n" + 
			"if current + 1 > limit then\n" + 
			"    return 0\n" + 
			"else\n" + 
			"    redis.call(\"incr\", key)\n" + 
			"    if current == 1 then\n" + 
			"        redis.call(\"expire\", key,period)\n" + 
			"    end\n" + 
			"    return 1\n" + 
			"end\n";
	
    /**
     * 尝试是否能正常执行
     *
     * @param resource      资源名
     * @param rate          限制次数
     * @param periodSeconds 限制时长，单位为秒
     * @return true 可以执行
     * false 限次，禁止
     */
    public boolean tryAcquire(String resource, int rate, int periodSeconds, TimeUnit unit) {
    	Long count = (Long) Redis.use().getJedis().eval(luaScript, 1, resource, String.valueOf(rate), String.valueOf(periodSeconds));
    	if (count != null && count.intValue() == 1) {
            return true;
        } else {
            return false;
        }
    }
	
}
