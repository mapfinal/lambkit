package com.lambkit.core.limiter.redis;

import java.util.concurrent.TimeUnit;

import com.lambkit.core.limiter.Limiter;

/**
 * 分布式限流
 * @author yangyong
 *
 */
public abstract class RedisLimiter implements Limiter {
	/**
     * 限制时长默认为1秒
     */
    public boolean tryAcquire(String resource, int rate) {
        return tryAcquire(resource, rate, DEFAULT_PERIOD);
    }
    
    /**
     * 尝试是否能正常执行
     *
     * @param resource      资源名
     * @param rate          限制次数
     * @param periodSeconds 限制时长，单位为秒
     * @return true 可以执行
     * false 限次，禁止
     */
    public boolean tryAcquire(String resource, int rate, int periodSeconds) {
    	return tryAcquire(resource, rate, periodSeconds, DEFAULT_TIME_UNIT);
    }

    @Override
	public void release(String resource) {
		// TODO Auto-generated method stub
		
	}
}
