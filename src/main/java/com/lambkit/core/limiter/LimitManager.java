package com.lambkit.core.limiter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.jfinal.aop.Invocation;
import com.lambkit.Lambkit;
import com.lambkit.core.limiter.impl.GuavaLimiter;
import com.lambkit.core.limiter.impl.SemaphoreLimiter;
import com.lambkit.core.limiter.impl.SlidingWindowLimiter;
import com.lambkit.core.limiter.redis.FixedWindowRedisLimiter;
import com.lambkit.core.limiter.redis.SlidingWindowRedisLimiter;
import com.lambkit.core.limiter.redis.TokenBucketRedisLimiter;
import com.lambkit.distributed.node.NodeManager;

/**
 * Limit管理
 * 
 * @author yangyong
 *
 */
public class LimitManager {
	
	private final Map<LimiterType, Limiter> limiterMap = new ConcurrentHashMap<>();
	
	private static LimitManager me = new LimitManager();

    private LimitManager() {
    }

    public static LimitManager me() {
        return me;
    }
    
    public boolean enable() {
    	LimitConfig config = Lambkit.config(LimitConfig.class);
    	return config.isEnable();
    }
    
    public Limiter getDefaultLimiter() {
    	LimitConfig config = Lambkit.config(LimitConfig.class);
    	return getLimiter(config.getLimiterType());
    }

    public Limiter getLimiter(LimiterType type) {
    	Limiter limiter = limiterMap.get(type);
    	if(limiter==null) {
    		switch (type) {
			case TOKEN_BUCKET:
				limiter = new GuavaLimiter();
				break;
			case SEMAPHORE:
				limiter = new SemaphoreLimiter();
				break;
			case SLIDING:
				limiter = new SlidingWindowLimiter();
				break;
			case REDIS_FIXED:
				limiter = new FixedWindowRedisLimiter();
				break;
			case REDIS_SLIDING:
				limiter = new SlidingWindowRedisLimiter();
				break;
			case REDIS_TOKEN_BUCKET:
				limiter = new TokenBucketRedisLimiter();
				break;
			}
    	}
    	return limiter;
    }
    
    /**
     * 获取限流的对象的名称
     * @return
     */
    public String getResourceName(Invocation inv) {
    	//节点名称
    	String name = NodeManager.me().getNode().getName();
    	String resource = name + "/" + inv.getViewPath() + "/" + inv.getActionKey();
    	return resource;
    }
    
}
