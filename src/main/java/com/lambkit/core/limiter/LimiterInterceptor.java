package com.lambkit.core.limiter;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.log.Log;
import com.lambkit.Lambkit;

public class LimiterInterceptor implements Interceptor {
	private static final Log log = Log.getLog(LimiterInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
		// TODO Auto-generated method stub
		// 白名单

		// 限流
		Limit requestLimiter = inv.getMethod().getAnnotation(Limit.class);
		if (requestLimiter != null) {
			String url = inv.getActionKey();
			Limiter limiter = LimitManager.me().getLimiter(requestLimiter.limitType());
			if (limiter != null && !limiter.tryAcquire(url, requestLimiter.rate(), requestLimiter.period())) {
				log.warn("请求被限流,url:" + inv.getViewPath() + "/" + inv.getActionKey());
				makeFallbackResult(inv, requestLimiter.fallbackStrategy(), requestLimiter.fallbackImpl());
			} else {
				inv.invoke();
			}
		} else {
			// limit enable for all action
			if(LimitManager.me().enable()) {
				String url = inv.getActionKey();
				Limiter limiter = LimitManager.me().getDefaultLimiter();
				if (limiter != null && !limiter.tryAcquire(url)) {
					log.warn("请求被限流,url:" + inv.getViewPath() + "/" + inv.getActionKey());
					LimitConfig config = Lambkit.config(LimitConfig.class);
					makeFallbackResult(inv, config.getFallbackStrategy(), config.getFallbackImpl());
				} else {
					inv.invoke();
				}
			} else {
				inv.invoke();
			}
		}
	}

	private void makeFallbackResult(Invocation inv, LimitFallbackStrategy fallbackStrategy, String fallbackImpl) {
		// TODO Auto-generated method stub
		// render()
	}

}
