package com.lambkit.core.limiter.impl;

import java.util.concurrent.TimeUnit;

import com.jfinal.plugin.redis.Redis;
import com.lambkit.core.limiter.Limiter;

/**
 * 限流方法（滑动时间算法）
 * @author yangyong
 *
 */
public class SlidingWindowLimiter implements Limiter {
	/**
     * 限制时长默认为1秒
     */
    public boolean tryAcquire(String resource, int rate) {
        return tryAcquire(resource, rate, DEFAULT_PERIOD);
    }
    
    /**
     * 尝试是否能正常执行
     *
     * @param resource      资源名
     * @param rate          限制次数
     * @param periodSeconds 限制时长，单位为秒
     * @return true 可以执行
     * false 限次，禁止
     */
    public boolean tryAcquire(String resource, int rate, int periodSeconds) {
    	return tryAcquire(resource, rate, periodSeconds, DEFAULT_TIME_UNIT);
    }
    
	@Override
	public boolean tryAcquire(String resource, int rate, int period, TimeUnit timeUnit) {
		// TODO Auto-generated method stub
		long nowTs = System.currentTimeMillis(); // 当前时间戳
        // 删除非时间段内的请求数据（清除老访问数据，比如 period=60 时，标识清除 60s 以前的请求记录）
        Redis.use().getJedis().zremrangeByScore(resource, 0, nowTs - period * 1000);
        long currCount = Redis.use().getJedis().zcard(resource); // 当前请求次数
        if (currCount >= rate) {
            // 超过最大请求次数，执行限流
            return false;
        }
        // 未达到最大请求数，正常执行业务
        Redis.use().getJedis().zadd(resource, nowTs, "" + nowTs); // 请求记录 +1
        return true;
	}

	@Override
	public void release(String resource) {
		// TODO Auto-generated method stub

	}

}
