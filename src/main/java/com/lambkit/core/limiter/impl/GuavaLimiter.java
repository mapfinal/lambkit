package com.lambkit.core.limiter.impl;

import java.util.concurrent.TimeUnit;

import com.google.common.util.concurrent.RateLimiter;
import com.lambkit.Lambkit;
import com.lambkit.core.limiter.Limiter;

/**
 * 令牌桶算法
 * @author yangyong
 *
 */
public class GuavaLimiter implements Limiter {

	private static final String GUAVA_LIMITER_CACHE_NAME = "lambkit_limiter_guava";
	/**
	 * google guava 限流工具 RateLimiter
	private final Map<String, RateLimiter> rateLimiterMap = new ConcurrentHashMap<>();

	public RateLimiter getRateLimiter(String target, double rate) {
		RateLimiter rateLimiter = null;
        // 判断map集合中是否有创建好的令牌桶
        if (!rateLimiterMap.containsKey(target)) {
            // 创建令牌桶,以n r/s往桶中放入令牌
            rateLimiter = RateLimiter.create(rate);
            rateLimiterMap.put(target, rateLimiter);
        }
        rateLimiter = rateLimiterMap.get(target);
        return rateLimiter;
	}*/
	
	public RateLimiter getRateLimiter(String target, int rate) {
		RateLimiter rateLimiter = Lambkit.getCache().get(GUAVA_LIMITER_CACHE_NAME, target);
        // 判断map集合中是否有创建好的令牌桶
        if (rateLimiter==null) {
            // 创建令牌桶,以n r/s往桶中放入令牌
            rateLimiter = RateLimiter.create(rate);
            Lambkit.getCache().put(GUAVA_LIMITER_CACHE_NAME, target, rateLimiter);
        }
        return rateLimiter;
	}
	
	/**
     * 限制时长默认为1秒
     */
    public boolean tryAcquire(String resource, int rate) {
        return tryAcquire(resource, rate, DEFAULT_PERIOD);
    }
    
    /**
     * 尝试是否能正常执行
     *
     * @param resource      资源名
     * @param rate          限制次数
     * @param periodSeconds 限制时长，单位为秒
     * @return true 可以执行
     * false 限次，禁止
     */
    public boolean tryAcquire(String resource, int rate, int periodSeconds) {
    	return tryAcquire(resource, rate, periodSeconds, DEFAULT_TIME_UNIT);
    }
    
    @Override
    public boolean tryAcquire(String resource ,int rate, int period, TimeUnit timeUnit) {
    	RateLimiter rateLimiter = getRateLimiter(resource, rate);
		return rateLimiter.tryAcquire(period, timeUnit);
    }

	@Override
	public void release(String resource) {
		// TODO Auto-generated method stub

	}

}
