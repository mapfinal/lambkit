package com.lambkit.net.node;

import java.util.Map;

import com.google.common.collect.Maps;
import com.jfinal.kit.StrKit;
import com.lambkit.Lambkit;
import com.lambkit.common.util.StringUtils;
import com.lambkit.core.aop.AopKit;

public class LambkitNodeManager {

	private static LambkitNodeManager manager;

	public static LambkitNodeManager me() {
		if(manager==null) {
			manager = AopKit.singleton(LambkitNodeManager.class);
		}
		return manager;
	}
	
	private LambkitNode node = null;
	
	/**
	 * 父节点
	 */
	private LambkitNode parentNode;
	/**
	 * 兄弟节点
	 */
	private LambkitNode siblingNode;
	/**
	 * 叔叔节点，父节点的备份
	 */
	private Map<String, LambkitNode> uncleNodes = Maps.newConcurrentMap();
	
	/**
	 * 子节点
	 */
	private Map<String, LambkitNode> childNodes = Maps.newConcurrentMap();
	
	private LambkitNodeService service;
	
	public static final String route = "/lambkit/net/node";
	public static final String cacheName = "lambkit_node";
	private static final String nodekey = "our_node_info";
	
	public LambkitNode getNode() {
		if(node==null) {
			node = Lambkit.getCache().get(cacheName, nodekey);
		}
		if(node==null) {
			node = new LambkitNode();
			LambkitNodeConfig config = Lambkit.config(LambkitNodeConfig.class);
			String nodeId = Lambkit.getEhCache().get("LAMBKIT_NODE", "lambkit_node_id");
			if(StrKit.isBlank(nodeId)) {
				nodeId = StringUtils.getUuid32();
				Lambkit.getEhCache().put("LAMBKIT_NODE", "lambkit_node_id", nodeId);
			}
			node.setId(nodeId);
			node.setLevel(config.getLevel());
			node.setLinkParent(config.getLinkParent());
			node.setNetGroup(config.getNetGroup());
			node.setIp(config.getIp());
			node.addIp(config.getLocalNetGroup(), config.getLocalIp());
			node.setParentNodeUrl(config.getParentNodeUrl());
			node.setSiblingNodeUrl(config.getSiblingNodeUrl());
			node.setNodeType(config.getNodeType());
			Lambkit.getCache().put(cacheName, nodekey, node);
		}
		return node;
	}
	
	public LambkitNode addNetNode(LambkitNode node) {
		if(node!=null) {
			childNodes.put(node.getId(), node);
			Lambkit.getCache().put(LambkitNodeManager.cacheName, "other_node_" + node.getId(), node, 120);
			Lambkit.getCache().put(LambkitNodeManager.cacheName, "other_node_kes", childNodes.keySet());
		}
		return node;
	}
	
//	public LambkitNode getChildSibling(LambkitNode node) {
//		if(childNodes.size() < 1) return null;
//		if(node==null) {
//			return childNodes.
//		}
//	}

	public void setNode(LambkitNode node) {
		this.node = node;
	}

	public LambkitNode getParentNode() {
		return parentNode;
	}

	public void setParentNode(LambkitNode parentNode) {
		this.parentNode = parentNode;
	}

	public LambkitNode getSiblingNode() {
		return siblingNode;
	}

	public void setSiblingNode(LambkitNode siblingNode) {
		this.siblingNode = siblingNode;
	}

	public Map<String, LambkitNode> getChildNodes() {
		return childNodes;
	}
	
	public void setChildNodes(Map<String, LambkitNode> childNodes) {
		this.childNodes = childNodes;
	}

	public Map<String, LambkitNode> getUncleNodes() {
		return uncleNodes;
	}

	public void setUncleNodes(Map<String, LambkitNode> uncleNodes) {
		this.uncleNodes = uncleNodes;
	}

	public LambkitNodeService getService() {
		return service;
	}

	public void setService(LambkitNodeService service) {
		this.service = service;
	}
}
