package com.lambkit.net.node;

import com.lambkit.Lambkit;
import com.lambkit.common.LambkitResult;
import com.lambkit.core.aop.AopKit;
import com.lambkit.core.rpc.http.HttpRpcProxy;

public class NodePro {

	private NodeTarget target;
	
	public NodePro() {
	}

	public NodePro(String ip_port) {
		this.target = new NodeTarget(ip_port);
	}

	public NodePro(String ip, int port) {
		this.target = new NodeTarget(ip, port);
	}
	
	public NodePro(NodeTarget target) {
		this.target = target;
	}
	
	public NodePro(LambkitNode node) {
		
	}

	public LambkitNode detail() {
		return null;
	}
	
	//rpc, rmi
	//lambkit.node(ip).bean(service.class).method();
//	public <T> T bean(Class<T> clazz) {
//		return null;
//	}
	
	//rpc
	//lambkit.node(ip).service(service.class);
	public <T> T service(Class<T> serviceClass) {
		if(target==null || target.isLocal()) {
			return Lambkit.service(serviceClass);
		}
		return HttpRpcProxy.getObject(target.getHttp() + "/lambkit/rpc", serviceClass);
	}
	
	//rpc
	//lambkit.node(ip).model(model.class);
	public <T> T model(Class<T> modelClass) {
		if(target==null || target.isLocal()) {
			return AopKit.get(modelClass);
		}
		return HttpRpcProxy.getObject(target.getHttp() + "/lambkit/rpc/model", modelClass);
	}
	
	//
	//lambkit.node(ip).db(configName).find();
	public NodeDb db(String configName) {
		return new NodeDb(target, configName);
	}
	
	//lambkit.node(ip).api("route");
	public LambkitResult api(String route, Object... paras) {
		return null;
	}
	
	public <T> T api(String route, Class<T> modelClass, Object... paras) {
		return null;
	}

	public NodeTarget getTarget() {
		return target;
	}

	public void setTarget(NodeTarget target) {
		this.target = target;
	}

}
