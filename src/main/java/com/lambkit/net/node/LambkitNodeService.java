package com.lambkit.net.node;

public interface LambkitNodeService {

	/**
	 * 子节点发送心跳连接前的处理
	 */
	void preChildPostAlive(String url);
	/**
	 * 子节点发送心跳连接后的处理
	 */
	void endChildPostAlive(String result);
	
	/**
	 * 收到子节点的心跳
	 */
	void childPostAction(LambkitNode childNode);
	
	/**
	 * 心跳监测的校验
	 * @param node
	 * @param time
	 * @param sign
	 * @return
	 */
	boolean validationNodeAlive(LambkitNode node, String time, String sign);
	
	/**
	 * 节点API接口的检验
	 * @param nodeId
	 * @param appKey
	 * @param time
	 * @param sign
	 * @return
	 */
	boolean validationNodeApi(String nodeId, String appKey, String time, String sign);
}
