package com.lambkit.net.node;

import com.lambkit.common.LambkitConsts;
import com.lambkit.core.config.annotation.PropertieConfig;

@PropertieConfig(prefix = "lambkit.node")
public class LambkitNodeConfig {
	/**
	 * 用户的key
	 */
	private String key;
	/**
	 * 用户的密钥
	 */
	private String secret;
	/**
	 * 树结构的层级
	 */
	private int level;
	/**
	 * 节点分组名称
	 */
	private String netGroup = "internet";
	
	/**
	 * 父节点对接的地址
	 */
	private String ip;
	/**
	 * 子节点对接的地址
	 */
	private String localIp;
	private String localNetGroup;
	
	private int port = LambkitConsts.DEFAULT_PORT;
	/**
	 * 父节点
	 */
	private String parentNodeUrl;
	/**
	 * 兄弟节点
	 */
	private String siblingNodeUrl;
	/**
	 * 父子连通情况
	 */
	private int linkParent = 0;// 0单向，1双向
	
	private String nodeType = "normal";

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getParentNodeUrl() {
		return parentNodeUrl;
	}

	public void setParentNodeUrl(String parentNodeUrl) {
		this.parentNodeUrl = parentNodeUrl;
	}

	public String getSiblingNodeUrl() {
		return siblingNodeUrl;
	}

	public void setSiblingNodeUrl(String siblingNodeUrl) {
		this.siblingNodeUrl = siblingNodeUrl;
	}

	public int getLinkParent() {
		return linkParent;
	}

	public void setLinkParent(int linkParent) {
		this.linkParent = linkParent;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getLocalIp() {
		return localIp;
	}

	public void setLocalIp(String localIp) {
		this.localIp = localIp;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getNetGroup() {
		return netGroup;
	}

	public void setNetGroup(String netGroup) {
		this.netGroup = netGroup;
	}

	public String getLocalNetGroup() {
		return localNetGroup;
	}

	public void setLocalNetGroup(String localNetGroup) {
		this.localNetGroup = localNetGroup;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
}
