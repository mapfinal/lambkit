package com.lambkit.net.node;

import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.plugin.cron4j.Cron4jPlugin;
import com.lambkit.module.LambkitModule;

public class LambkitNodeModule extends LambkitModule {

	@Override
	public void configRoute(Routes me) {
		// TODO Auto-generated method stub
		super.configRoute(me);
		me.add(LambkitNodeManager.route, LambkitNodeController.class);
	}
	
	@Override
	public void configPlugin(Plugins me) {
		// TODO Auto-generated method stub
		super.configPlugin(me);
		Cron4jPlugin cron4j = new Cron4jPlugin();
		cron4j.addTask("* * * * *", new LambkitNodeTask());
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		//初始化节点
		LambkitNodeManager.me().getNode();
	}
}
