package com.lambkit.net.node;

import com.alibaba.fastjson.JSON;
import com.jfinal.aop.Clear;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.lambkit.common.ResultKit;
import com.lambkit.common.service.ServiceKit;
import com.lambkit.web.controller.LambkitController;

@Clear
public class LambkitNodeController extends LambkitController {

	/**
	 * 子服务节点进行心跳检测
	 */
	public void alive() {
		if(isPOST()) {
			// 子服务访问父节点
			// 接收节点发送过来的数据
			String data = getRawData();
			if (StrKit.isBlank(data)) {
				renderJson(ResultKit.json(0, "参数不正确", null));
				return;
			}
			LambkitNode otherNode = JSON.parseObject(data, LambkitNode.class);
			if (otherNode == null) {
				renderJson(ResultKit.json(0, "参数不正确", null));
				return;
			}
			LambkitNodeService service = LambkitNodeManager.me().getService();
			if(service!=null) {
				String time = getHeader("lkntime");
				String sign = getHeader("lknsign");
				boolean flag = service.validationNodeAlive(otherNode, time, sign);
				if(!flag) {
					renderJson(ResultKit.json(0, "校验失败", null));
					return;
				}
			}
			LambkitNodeManager.me().addNetNode(otherNode);
			if(service!=null) {
				service.childPostAction(otherNode);
			}
			// 返回自己节点的信息
			LambkitNode node = LambkitNodeManager.me().getNode();
			if (node != null) {
				Kv result = Kv.by("parent", node);
				result.set("uncle", LambkitNodeManager.me().getSiblingNode());
				renderJson(ResultKit.json(1, "成功", result));
			} else {
				renderJson(ResultKit.json(0, "服务器不可用", null));
			}
		} else {
			//父节点来访问子节点
			LambkitNode node = LambkitNodeManager.me().getNode();
			if (node != null) {
				renderJson(ResultKit.json(1, "成功", node));
			} else {
				renderJson(ResultKit.json(0, "服务器不可用", null));
			}
		}
	}

	/**
	 * 兄弟服务节点进行握手监测
	 */
	public void shake() {
		// 接收节点发送过来的数据
		String data = getRawData();
		if (StrKit.isBlank(data)) {
			renderJson(ResultKit.json(0, "fail", null));
			return;
		}
		LambkitNode otherNode = JSON.parseObject(data, LambkitNode.class);
		if (otherNode == null) {
			renderJson(ResultKit.json(0, "fail", null));
			return;
		}
		LambkitNodeService service = LambkitNodeManager.me().getService();
		if(service!=null) {
			String time = getHeader("lkntime");
			String sign = getHeader("lknsign");
			boolean flag = service.validationNodeAlive(otherNode, time, sign);
			if(!flag) {
				renderJson(ResultKit.json(0, "校验失败", null));
				return;
			}
		}
		LambkitNodeManager.me().setSiblingNode(otherNode);
		// 返回自己节点的信息
		LambkitNode node = LambkitNodeManager.me().getNode();
		if (node != null) {
			renderJson(ResultKit.json(1, "success", node));
		} else {
			renderJson(ResultKit.json(0, "fail", null));
		}
	}
}
