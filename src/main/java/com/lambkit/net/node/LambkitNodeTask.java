package com.lambkit.net.node;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;

public class LambkitNodeTask implements Runnable {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		LambkitNode node = LambkitNodeManager.me().getNode();
		String url = null;
		LambkitNode parent = LambkitNodeManager.me().getParentNode();
		if(parent==null || StrKit.isBlank(parent.getUrl())) {
			if(node!=null && StrKit.notBlank(node.getParentNodeUrl())) {
				url = node.getParentNodeUrl() + "/alive";
			}
		} else {
			url = parent.getUrl() + "/alive";
		}
		
		LambkitNodeService service = LambkitNodeManager.me().getService();
		if(service!=null) {
			service.preChildPostAlive(url);
		}
		
		String result = null;
		if(node!=null && StrKit.notBlank(url)) {
			result = HttpKit.post(url, JSON.toJSONString(node));
			if(StrKit.notBlank(result)) {
				JSONObject jsonObj = JSON.parseObject(result);
				if(jsonObj!=null && jsonObj.getInteger("code")==1 && jsonObj.containsKey("data")) {
					JSONObject dataObj = jsonObj.getJSONObject("data");
					parent = dataObj.getObject("parent", LambkitNode.class);
					if(parent!=null) {
						LambkitNodeManager.me().setParentNode(parent);
					}
					LambkitNode uncle = dataObj.getObject("uncle", LambkitNode.class);
					if(uncle!=null) {
						LambkitNodeManager.me().getUncleNodes().put(uncle.getId(), uncle);
					}
				}
			}
		}
		
		if(service!=null) {
			service.endChildPostAlive(result);
		}
		
	}

}
