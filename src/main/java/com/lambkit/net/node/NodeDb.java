package com.lambkit.net.node;

import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordException;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.ICallback;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.PageSqlKit;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.Lambkit;
import com.lambkit.db.service.DbService;

public class NodeDb {

	private NodeTarget target;
	
	private String configName;
	
	static final Object[] NULL_PARA_ARRAY = new Object[0];
	
	public NodeDb(NodeTarget target, String configName) {
		this.target = target;
		this.configName = configName;
	}
	
	private DbService dbService() {
		DbService service = Lambkit.node(target).service(DbService.class);
		service.setConfigName(configName);
		return service;
//		if(StrKit.notBlank(configName)) {
//			return Db.use(configName);
//		}
//		return Db.use();
	}
	
	private DbPro db() {
		if(StrKit.notBlank(configName)) {
			return Db.use(configName);
		}
		return Db.use();
	}

	/**
	 * @see #query(String, String, Object...)
	 */
	public <T> List<T> query(String sql, Object... paras) {
		if(target.isLocal()) {
			return db().query(sql, paras);
		} else {
			return dbService().query(sql, paras);
		}
	}
	
	/**
	 * @see #query(String, Object...)
	 * @param sql an SQL statement
	 */
	public <T> List<T> query(String sql) {		// return  List<object[]> or List<object>
		return query(sql, NULL_PARA_ARRAY);
	}
	
	/**
	 * Execute sql query and return the first result. I recommend add "limit 1" in your sql.
	 * @param sql an SQL statement that may contain one or more '?' IN parameter placeholders
	 * @param paras the parameters of sql
	 * @return Object[] if your sql has select more than one column,
	 * 			and it return Object if your sql has select only one column.
	 */
	public <T> T queryFirst(String sql, Object... paras) {
		List<T> result = query(sql, paras);
		return (result.size() > 0 ? result.get(0) : null);
	}
	
	/**
	 * @see #queryFirst(String, Object...)
	 * @param sql an SQL statement
	 */
	public <T> T queryFirst(String sql) {
		// return queryFirst(sql, NULL_PARA_ARRAY);
		List<T> result = query(sql, NULL_PARA_ARRAY);
		return (result.size() > 0 ? result.get(0) : null);
	}
	
	// 26 queryXxx method below -----------------------------------------------
	/**
	 * Execute sql query just return one column.
	 * @param <T> the type of the column that in your sql's select statement
	 * @param sql an SQL statement that may contain one or more '?' IN parameter placeholders
	 * @param paras the parameters of sql
	 * @return <T> T
	 */
	public <T> T queryColumn(String sql, Object... paras) {
		List<T> result = query(sql, paras);
		if (result.size() > 0) {
			T temp = result.get(0);
			if (temp instanceof Object[])
				throw new ActiveRecordException("Only ONE COLUMN can be queried.");
			return temp;
		}
		return null;
	}
	
	public <T> T queryColumn(String sql) {
		return (T)queryColumn(sql, NULL_PARA_ARRAY);
	}
	
	public String queryStr(String sql, Object... paras) {
		Object s = queryColumn(sql, paras);
		return s != null ? s.toString() : null;
	}
	
	public String queryStr(String sql) {
		return queryStr(sql, NULL_PARA_ARRAY);
	}
	
	public Integer queryInt(String sql, Object... paras) {
		Number n = queryNumber(sql, paras);
		return n != null ? n.intValue() : null;
	}
	
	public Integer queryInt(String sql) {
		return queryInt(sql, NULL_PARA_ARRAY);
	}
	
	public Long queryLong(String sql, Object... paras) {
		Number n = queryNumber(sql, paras);
		return n != null ? n.longValue() : null;
	}
	
	public Long queryLong(String sql) {
		return queryLong(sql, NULL_PARA_ARRAY);
	}
	
	public Double queryDouble(String sql, Object... paras) {
		Number n = queryNumber(sql, paras);
		return n != null ? n.doubleValue() : null;
	}
	
	public Double queryDouble(String sql) {
		return queryDouble(sql, NULL_PARA_ARRAY);
	}
	
	public Float queryFloat(String sql, Object... paras) {
		Number n = queryNumber(sql, paras);
		return n != null ? n.floatValue() : null;
	}
	
	public Float queryFloat(String sql) {
		return queryFloat(sql, NULL_PARA_ARRAY);
	}
	
	public java.math.BigDecimal queryBigDecimal(String sql, Object... paras) {
		return (java.math.BigDecimal)queryColumn(sql, paras);
	}
	
	public java.math.BigDecimal queryBigDecimal(String sql) {
		return (java.math.BigDecimal)queryColumn(sql, NULL_PARA_ARRAY);
	}
	
	public byte[] queryBytes(String sql, Object... paras) {
		return (byte[])queryColumn(sql, paras);
	}
	
	public byte[] queryBytes(String sql) {
		return (byte[])queryColumn(sql, NULL_PARA_ARRAY);
	}
	
	public java.util.Date queryDate(String sql, Object... paras) {
		return (java.util.Date)queryColumn(sql, paras);
	}
	
	public java.util.Date queryDate(String sql) {
		return (java.util.Date)queryColumn(sql, NULL_PARA_ARRAY);
	}
	
	public java.sql.Time queryTime(String sql, Object... paras) {
		return (java.sql.Time)queryColumn(sql, paras);
	}
	
	public java.sql.Time queryTime(String sql) {
		return (java.sql.Time)queryColumn(sql, NULL_PARA_ARRAY);
	}
	
	public java.sql.Timestamp queryTimestamp(String sql, Object... paras) {
		return (java.sql.Timestamp)queryColumn(sql, paras);
	}
	
	public java.sql.Timestamp queryTimestamp(String sql) {
		return (java.sql.Timestamp)queryColumn(sql, NULL_PARA_ARRAY);
	}
	
	public Boolean queryBoolean(String sql, Object... paras) {
		return (Boolean)queryColumn(sql, paras);
	}
	
	public Boolean queryBoolean(String sql) {
		return (Boolean)queryColumn(sql, NULL_PARA_ARRAY);
	}
	
	public Short queryShort(String sql, Object... paras) {
		Number n = queryNumber(sql, paras);
		return n != null ? n.shortValue() : null;
	}
	
	public Short queryShort(String sql) {
		return queryShort(sql, NULL_PARA_ARRAY);
	}
	
	public Byte queryByte(String sql, Object... paras) {
		Number n = queryNumber(sql, paras);
		return n != null ? n.byteValue() : null;
	}
	
	public Byte queryByte(String sql) {
		return queryByte(sql, NULL_PARA_ARRAY);
	}
	
	public Number queryNumber(String sql, Object... paras) {
		return (Number)queryColumn(sql, paras);
	}
	
	public Number queryNumber(String sql) {
		return (Number)queryColumn(sql, NULL_PARA_ARRAY);
	}
	// 26 queryXxx method under -----------------------------------------------
	
	/**
	 * Execute update, insert or delete sql statement.
	 * @param sql an SQL statement that may contain one or more '?' IN parameter placeholders
	 * @param paras the parameters of sql
	 * @return either the row count for <code>INSERT</code>, <code>UPDATE</code>,
     *         or <code>DELETE</code> statements, or 0 for SQL statements 
     *         that return nothing
	 */
	public int update(String sql, Object... paras) {
		if(target.isLocal()) {
			return db().update(sql, paras);
		} else {
			return dbService().update(sql, paras);
		}
	}
	
	/**
	 * @see #update(String, Object...)
	 * @param sql an SQL statement
	 */
	public int update(String sql) {
		return update(sql, NULL_PARA_ARRAY);
	}
	
	/**
	 * @see #find(String, String, Object...)
	 */
	public List<Record> find(String sql, Object... paras) {
		if(target.isLocal()) {
			return db().find(sql, paras);
		} else {
			return dbService().find(sql, paras);
		}
	}
	
	/**
	 * @see #find(String, String, Object...)
	 * @param sql the sql statement
	 */
	public List<Record> find(String sql) {
		return find(sql, NULL_PARA_ARRAY);
	}
	
	public List<Record> findAll(String tableName) {
		if(target.isLocal()) {
			return db().findAll(tableName);
		} else {
			return dbService().findAll(tableName);
		}
	}
	
	/**
	 * Find first record. I recommend add "limit 1" in your sql.
	 * @param sql an SQL statement that may contain one or more '?' IN parameter placeholders
	 * @param paras the parameters of sql
	 * @return the Record object
	 */
	public Record findFirst(String sql, Object... paras) {
		List<Record> result = find(sql, paras);
		return result.size() > 0 ? result.get(0) : null;
	}
	
	/**
	 * @see #findFirst(String, Object...)
	 * @param sql an SQL statement
	 */
	public Record findFirst(String sql) {
		return findFirst(sql, NULL_PARA_ARRAY);
	}
	
	/**
	 * Find record by id with default primary key.
	 * <pre>
	 * Example:
	 * Record user = Db.use().findById("user", 15);
	 * </pre>
	 * @param tableName the table name of the table
	 * @param idValue the id value of the record
	 */
	public Record findById(String tableName, Object idValue) {
		if(target.isLocal()) {
			return db().findById(tableName, idValue);
		} else {
			return dbService().findById(tableName, idValue);
		}
	}
	
	public Record findById(String tableName, String primaryKey, Object idValue) {
		return findByIds(tableName, primaryKey, idValue);
	}
	
	/**
	 * Find record by ids.
	 * <pre>
	 * Example:
	 * Record user = Db.use().findByIds("user", "user_id", 123);
	 * Record userRole = Db.use().findByIds("user_role", "user_id, role_id", 123, 456);
	 * </pre>
	 * @param tableName the table name of the table
	 * @param primaryKey the primary key of the table, composite primary key is separated by comma character: ","
	 * @param idValues the id value of the record, it can be composite id values
	 */
	public Record findByIds(String tableName, String primaryKey, Object... idValues) {
		if(target.isLocal()) {
			return db().findById(tableName, primaryKey, idValues);
		} else {
			return dbService().findById(tableName, primaryKey, idValues);
		}
	}
	
	/**
	 * Delete record by id with default primary key.
	 * <pre>
	 * Example:
	 * Db.use().deleteById("user", 15);
	 * </pre>
	 * @param tableName the table name of the table
	 * @param idValue the id value of the record
	 * @return true if delete succeed otherwise false
	 */
	public boolean deleteById(String tableName, Object idValue) {
		if(target.isLocal()) {
			return db().deleteById(tableName, idValue);
		} else {
			return dbService().deleteById(tableName, idValue);
		}
	}
	
	public boolean deleteById(String tableName, String primaryKey, Object idValue) {
		return deleteByIds(tableName, primaryKey, idValue);
	}
	
	/**
	 * Delete record by ids.
	 * <pre>
	 * Example:
	 * Db.use().deleteByIds("user", "user_id", 15);
	 * Db.use().deleteByIds("user_role", "user_id, role_id", 123, 456);
	 * </pre>
	 * @param tableName the table name of the table
	 * @param primaryKey the primary key of the table, composite primary key is separated by comma character: ","
	 * @param idValues the id value of the record, it can be composite id values
	 * @return true if delete succeed otherwise false
	 */
	public boolean deleteByIds(String tableName, String primaryKey, Object... idValues) {
		if(target.isLocal()) {
			return db().deleteByIds(tableName, primaryKey, idValues);
		} else {
			return dbService().deleteByIds(tableName, primaryKey, idValues);
		}
	}
	
	/**
	 * Delete record.
	 * <pre>
	 * Example:
	 * boolean succeed = Db.use().delete("user", "id", user);
	 * </pre>
	 * @param tableName the table name of the table
	 * @param primaryKey the primary key of the table, composite primary key is separated by comma character: ","
	 * @param record the record
	 * @return true if delete succeed otherwise false
	 */
	public boolean delete(String tableName, String primaryKey, Record record) {
		if(target.isLocal()) {
			return db().delete(tableName, primaryKey, record);
		} else {
			return dbService().delete(tableName, primaryKey, record);
		}
	}
	
	/**
	 * <pre>
	 * Example:
	 * boolean succeed = Db.use().delete("user", user);
	 * </pre>
	 * @see #delete(String, String, Record)
	 */
	public boolean delete(String tableName, Record record) {
		if(target.isLocal()) {
			return db().delete(tableName, record);
		} else {
			return dbService().delete(tableName, record);
		}
	}
	
	/**
	 * Execute delete sql statement.
	 * @param sql an SQL statement that may contain one or more '?' IN parameter placeholders
	 * @param paras the parameters of sql
	 * @return the row count for <code>DELETE</code> statements, or 0 for SQL statements 
     *         that return nothing
	 */
	public int delete(String sql, Object... paras) {
		return update(sql, paras);
	}
	
	/**
	 * @see #delete(String, Object...)
	 * @param sql an SQL statement
	 */
	public int delete(String sql) {
		return update(sql);
	}
	
	/**
	 * Paginate.
	 * @param pageNumber the page number
	 * @param pageSize the page size
	 * @param select the select part of the sql statement
	 * @param sqlExceptSelect the sql statement excluded select part
	 * @param paras the parameters of sql
	 * @return the Page object
	 */
	public Page<Record> paginate(int pageNumber, int pageSize, String select, String sqlExceptSelect, Object... paras) {
		return paginate(pageNumber, pageSize, null, select, sqlExceptSelect, paras);
	}
	
	/**
	 * @see #paginate(String, int, int, String, String, Object...)
	 */
	public Page<Record> paginate(int pageNumber, int pageSize, String select, String sqlExceptSelect) {
		return paginate(pageNumber, pageSize, null, select, sqlExceptSelect, NULL_PARA_ARRAY);
	}
	
	public Page<Record> paginate(int pageNumber, int pageSize, boolean isGroupBySql, String select, String sqlExceptSelect, Object... paras) {
		if(target.isLocal()) {
			return db().paginate(pageNumber, pageSize, isGroupBySql, select, sqlExceptSelect, paras);
		} else {
			return dbService().paginate(pageNumber, pageSize, isGroupBySql, select, sqlExceptSelect, paras);
		}
	}
	
	public Page<Record> paginateByFullSql(int pageNumber, int pageSize, String totalRowSql, String findSql, Object... paras) {
		return paginateByFullSql(pageNumber, pageSize, null, totalRowSql, findSql, paras);
	}
	
	public Page<Record> paginateByFullSql(int pageNumber, int pageSize, boolean isGroupBySql, String totalRowSql, String findSql, Object... paras) {
		if(target.isLocal()) {
			return db().paginateByFullSql(pageNumber, pageSize, isGroupBySql, totalRowSql, findSql, paras);
		} else {
			return dbService().paginateByFullSql(pageNumber, pageSize, isGroupBySql, totalRowSql, findSql, paras);
		}
	}
	
	/**
	 * Save record.
	 * <pre>
	 * Example:
	 * Record userRole = new Record().set("user_id", 123).set("role_id", 456);
	 * Db.use().save("user_role", "user_id, role_id", userRole);
	 * </pre>
	 * @param tableName the table name of the table
	 * @param primaryKey the primary key of the table, composite primary key is separated by comma character: ","
	 * @param record the record will be saved
	 * @param true if save succeed otherwise false
	 */
	public boolean save(String tableName, String primaryKey, Record record) {
		if(target.isLocal()) {
			return db().save(tableName, primaryKey, record);
		} else {
			return dbService().save(tableName, primaryKey, record);
		}
	}
	
	/**
	 * @see #save(String, String, Record)
	 */
	public boolean save(String tableName, Record record) {
		if(target.isLocal()) {
			return db().save(tableName, record);
		} else {
			return dbService().save(tableName, record);
		}
	}
	
	/**
	 * Update Record.
	 * <pre>
	 * Example:
	 * Db.use().update("user_role", "user_id, role_id", record);
	 * </pre>
	 * @param tableName the table name of the Record save to
	 * @param primaryKey the primary key of the table, composite primary key is separated by comma character: ","
	 * @param record the Record object
	 * @param true if update succeed otherwise false
	 */
	public boolean update(String tableName, String primaryKey, Record record) {
		if(target.isLocal()) {
			return db().update(tableName, primaryKey, record);
		} else {
			return dbService().update(tableName, primaryKey, record);
		}
	}
	
	/**
	 * Update record with default primary key.
	 * <pre>
	 * Example:
	 * Db.use().update("user", record);
	 * </pre>
	 * @see #update(String, String, Record)
	 */
	public boolean update(String tableName, Record record) {
		if(target.isLocal()) {
			return db().update(tableName, record);
		} else {
			return dbService().update(tableName, record);
		}
	}
	
	/**
	 * @see #execute(String, ICallback)
	 */
	public Object execute(ICallback callback) {
		if(target.isLocal()) {
			return db().execute(callback);
		} else {
			return dbService().execute(callback);
		}
	}
	
	
	
	public boolean tx(int transactionLevel, IAtom atom) {
		if(target.isLocal()) {
			return db().tx(transactionLevel, atom);
		} else {
			return dbService().tx(transactionLevel, atom);
		}
	}
	
	/**
	 * Execute transaction with default transaction level.
	 * @see #tx(int, IAtom)
	 */
	public boolean tx(IAtom atom) {
		if(target.isLocal()) {
			return db().tx(atom);
		} else {
			return dbService().tx(atom);
		}
	}
	
	/**
	 * Find Record by cache.
	 * @see #find(String, Object...)
	 * @param cacheName the cache name
	 * @param key the key used to get date from cache
	 * @return the list of Record
	 */
	public List<Record> findByCache(String cacheName, Object key, String sql, Object... paras) {
		if(target.isLocal()) {
			return db().findByCache(cacheName, key, sql, paras);
		} else {
			return dbService().findByCache(cacheName, key, sql, paras);
		}
	}
	
	/**
	 * @see #findByCache(String, Object, String, Object...)
	 */
	public List<Record> findByCache(String cacheName, Object key, String sql) {
		return findByCache(cacheName, key, sql, NULL_PARA_ARRAY);
	}
	
	/**
	 * Find first record by cache. I recommend add "limit 1" in your sql.
	 * @see #findFirst(String, Object...)
	 * @param cacheName the cache name
	 * @param key the key used to get date from cache
	 * @param sql an SQL statement that may contain one or more '?' IN parameter placeholders
	 * @param paras the parameters of sql
	 * @return the Record object
	 */
	public Record findFirstByCache(String cacheName, Object key, String sql, Object... paras) {
		if(target.isLocal()) {
			return db().findFirstByCache(cacheName, key, sql, paras);
		} else {
			return dbService().findFirstByCache(cacheName, key, sql, paras);
		}
	}
	
	/**
	 * @see #findFirstByCache(String, Object, String, Object...)
	 */
	public Record findFirstByCache(String cacheName, Object key, String sql) {
		return findFirstByCache(cacheName, key, sql, NULL_PARA_ARRAY);
	}
	
	/**
	 * Paginate by cache.
	 * @see #paginate(int, int, String, String, Object...)
	 * @return Page
	 */
	public Page<Record> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize, String select, String sqlExceptSelect, Object... paras) {
		return paginateByCache(cacheName, key, pageNumber, pageSize, null, select, sqlExceptSelect, paras);
	}
	
	/**
	 * @see #paginateByCache(String, Object, int, int, String, String, Object...)
	 */
	public Page<Record> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize, String select, String sqlExceptSelect) {
		return paginateByCache(cacheName, key, pageNumber, pageSize, null, select, sqlExceptSelect, NULL_PARA_ARRAY);
	}
	
	public Page<Record> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize, boolean isGroupBySql, String select, String sqlExceptSelect, Object... paras) {
		if(target.isLocal()) {
			return db().paginateByCache(cacheName, key, pageNumber, pageSize, isGroupBySql, select, sqlExceptSelect, paras);
		} else {
			return dbService().paginateByCache(cacheName, key, pageNumber, pageSize, isGroupBySql, select, sqlExceptSelect, paras);
		}
	}
	
    /**
     * Execute a batch of SQL INSERT, UPDATE, or DELETE queries.
     * <pre>
     * Example:
     * String sql = "insert into user(name, cash) values(?, ?)";
     * int[] result = Db.use().batch(sql, new Object[][]{{"James", 888}, {"zhanjin", 888}});
     * </pre>
     * @param sql The SQL to execute.
     * @param paras An array of query replacement parameters.  Each row in this array is one set of batch replacement values.
     * @return The number of rows updated per statement
     */
	public int[] batch(String sql, Object[][] paras, int batchSize) {
		if(target.isLocal()) {
			return db().batch(sql, paras, batchSize);
		} else {
			return dbService().batch(sql, paras, batchSize);
		}
	}
	
	/**
     * Execute a batch of SQL INSERT, UPDATE, or DELETE queries.
     * <pre>
     * Example:
     * String sql = "insert into user(name, cash) values(?, ?)";
     * int[] result = Db.use().batch(sql, "name, cash", modelList, 500);
     * </pre>
	 * @param sql The SQL to execute.
	 * @param columns the columns need be processed by sql.
	 * @param modelOrRecordList model or record object list.
	 * @param batchSize batch size.
	 * @return The number of rows updated per statement
	 */
	public int[] batch(String sql, String columns, List modelOrRecordList, int batchSize) {
		if(target.isLocal()) {
			return db().batch(sql, columns, modelOrRecordList, batchSize);
		} else {
			return dbService().batch(sql, columns, modelOrRecordList, batchSize);
		}
	}
	
	
    /**
     * Execute a batch of SQL INSERT, UPDATE, or DELETE queries.
     * <pre>
     * Example:
     * int[] result = Db.use().batch(sqlList, 500);
     * </pre>
	 * @param sqlList The SQL list to execute.
	 * @param batchSize batch size.
	 * @return The number of rows updated per statement
	 */
    public int[] batch(List<String> sqlList, int batchSize) {
    	if(target.isLocal()) {
			return db().batch(sqlList, batchSize);
		} else {
			return dbService().batch(sqlList, batchSize);
		}
    }
    
    /**
     * Batch save models using the "insert into ..." sql generated by the first model in modelList.
     * Ensure all the models can use the same sql as the first model.
     */
    public int[] batchSave(List<? extends Model> modelList, int batchSize) {
    	if(target.isLocal()) {
			return db().batchSave(modelList, batchSize);
		} else {
			return dbService().batchSave(modelList, batchSize);
		}
    }
    
    /**
     * Batch save records using the "insert into ..." sql generated by the first record in recordList.
     * Ensure all the record can use the same sql as the first record.
     * @param tableName the table name
     */
    public int[] batchSave(String tableName, List<Record> recordList, int batchSize) {
    	if(target.isLocal()) {
			return db().batchSave(tableName, recordList, batchSize);
		} else {
			return dbService().batchSave(tableName, recordList, batchSize);
		}
    }
    
    /**
     * Batch update models using the attrs names of the first model in modelList.
     * Ensure all the models can use the same sql as the first model.
     */
    public int[] batchUpdate(List<? extends Model> modelList, int batchSize) {
    	if(target.isLocal()) {
			return db().batchUpdate(modelList, batchSize);
		} else {
			return dbService().batchUpdate(modelList, batchSize);
		}
    }
    
    /**
     * Batch update records using the columns names of the first record in recordList.
     * Ensure all the records can use the same sql as the first record.
     * @param tableName the table name
     * @param primaryKey the primary key of the table, composite primary key is separated by comma character: ","
     */
    public int[] batchUpdate(String tableName, String primaryKey, List<Record> recordList, int batchSize) {
    	if(target.isLocal()) {
			return db().batchUpdate(tableName, primaryKey, recordList, batchSize);
		} else {
			return dbService().batchUpdate(tableName, primaryKey, recordList, batchSize);
		}
    }
    
    /**
     * Batch update records with default primary key, using the columns names of the first record in recordList.
     * Ensure all the records can use the same sql as the first record.
     * @param tableName the table name
     */
    public int[] batchUpdate(String tableName, List<Record> recordList, int batchSize) {
    	if(target.isLocal()) {
			return db().batchUpdate(tableName, recordList, batchSize);
		} else {
			return dbService().batchUpdate(tableName, recordList, batchSize);
		}
    }
    
    public String getSql(String key) {
    	if(target.isLocal()) {
			return db().getSql(key);
		} else {
			return dbService().getSql(key);
		}
    }
    
    public SqlPara getSqlPara(String key, Record record) {
    	return getSqlPara(key, record.getColumns());
    }
    
    public SqlPara getSqlPara(String key, Model model) {
    	return getSqlPara(key, model._getAttrsEntrySet());
    }
    
    public SqlPara getSqlPara(String key, Map data) {
    	if(target.isLocal()) {
			return db().getSqlPara(key, data);
		} else {
			return dbService().getSqlPara(key, data);
		}
    }
    
    public SqlPara getSqlPara(String key, Object... paras) {
    	if(target.isLocal()) {
			return db().getSqlPara(key, paras);
		} else {
			return dbService().getSqlPara(key, paras);
		}
    }
	
	public SqlPara getSqlParaByString(String content, Map data) {
		if(target.isLocal()) {
			return db().getSqlParaByString(content, data);
		} else {
			return dbService().getSqlParaByString(content, data);
		}
	}
	
	public SqlPara getSqlParaByString(String content, Object... paras) {
		if(target.isLocal()) {
			return db().getSqlParaByString(content, paras);
		} else {
			return dbService().getSqlParaByString(content, paras);
		}
	}
	
    public List<Record> find(SqlPara sqlPara) {
    	return find(sqlPara.getSql(), sqlPara.getPara());
    }
    
    public Record findFirst(SqlPara sqlPara) {
    	return findFirst(sqlPara.getSql(), sqlPara.getPara());
    }
    
    public int update(SqlPara sqlPara) {
    	return update(sqlPara.getSql(), sqlPara.getPara());
    }
    
    public Page<Record> paginate(int pageNumber, int pageSize, SqlPara sqlPara) {
    	String[] sqls = PageSqlKit.parsePageSql(sqlPara.getSql());
    	return paginate(pageNumber, pageSize, null, sqls[0], sqls[1], sqlPara.getPara());
    }
	
	public Page<Record> paginate(int pageNumber, int pageSize, boolean isGroupBySql, SqlPara sqlPara) {
		String[] sqls = PageSqlKit.parsePageSql(sqlPara.getSql());
		return paginate(pageNumber, pageSize, isGroupBySql, sqls[0], sqls[1], sqlPara.getPara());
	}
	
	// ---------
//	
//	public DbTemplate template(String key, Map data) {
//		return new DbTemplate(this, key, data);
//	}
//	
//	public DbTemplate template(String key, Object... paras) {
//		return new DbTemplate(this, key, paras);
//	}
//	
//	// ---------
//	
//	public DbTemplate templateByString(String content, Map data) {
//		return new DbTemplate(true, this, content, data);
//	}
//	
//	public DbTemplate templateByString(String content, Object... paras) {
//		return new DbTemplate(true, this, content, paras);
//	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}



	public NodeTarget getTarget() {
		return target;
	}



	public void setTarget(NodeTarget target) {
		this.target = target;
	}
}
