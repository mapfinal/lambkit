package com.lambkit.net.node;

import java.io.Serializable;
import java.util.Map;

import com.google.common.collect.Maps;
import com.jfinal.kit.StrKit;
import com.lambkit.common.LambkitConsts;

/**
 * 网络节点
 * 
 * @author yangyong
 *
 */
public class LambkitNode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private String id;
	/**
	 * 用户的key
	 */
	private String key;
	/**
	 * 树结构的层级
	 */
	private int level;
	/**
	 * 网络组
	 */
	private String netGroup = "internet";
	/**
	 * 主要地址
	 */
	private String ip;
	/**
	 * 其他地址
	 */
	private Map<String, String> localIps;
	/**
	 * 端口
	 */
	private int port = LambkitConsts.DEFAULT_PORT;
	/**
	 * 地址
	 */
	private String url;
	/**
	 * 父节点
	 */
	private String parentNodeUrl;
	/**
	 * 兄弟节点
	 */
	private String siblingNodeUrl;
	/**
	 * 父子连通情况
	 */
	private int linkParent = 0;// 0单向，1双向
	
	private String nodeType = "normal";
	
	public void addIp(String netGroup, String ip) {
		getLocalIps().put(netGroup, ip);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLinkParent() {
		return linkParent;
	}

	public void setLinkParent(int linkParent) {
		this.linkParent = linkParent;
	}

	public String getParentNodeUrl() {
		return parentNodeUrl;
	}

	public void setParentNodeUrl(String parentNodeUrl) {
		this.parentNodeUrl = parentNodeUrl;
	}

	public String getSiblingNodeUrl() {
		return siblingNodeUrl;
	}

	public void setSiblingNodeUrl(String siblingNodeUrl) {
		this.siblingNodeUrl = siblingNodeUrl;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getNetGroup() {
		return netGroup;
	}

	public void setNetGroup(String netGroup) {
		this.netGroup = netGroup;
	}

	public Map<String, String> getLocalIps() {
		if(localIps==null) {
			localIps = Maps.newHashMap();
		}
		return localIps;
	}

	public void setLocalIps(Map<String, String> localIps) {
		this.localIps = localIps;
	}

	public String getUrl() {
		if(StrKit.isBlank(url) && StrKit.notBlank(ip)) {
			url = "http://" + ip + ":" + port;
		}
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
}
