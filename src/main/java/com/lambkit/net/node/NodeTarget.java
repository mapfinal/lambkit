package com.lambkit.net.node;

import com.jfinal.kit.StrKit;
import com.lambkit.common.LambkitConsts;

public class NodeTarget {

	private String ip = "localhost";
	private int port = LambkitConsts.DEFAULT_PORT;

	public NodeTarget() {
	}

	public NodeTarget(String ip_port) {
		this();
		if(StrKit.notBlank(ip_port)) {
    		if(ip_port.contains(":")) {
    			String[] url = ip_port.split(":");
    			if(url.length > 0) {
    				this.ip = url[0];
    			}
    			if(url.length > 1) {
    				Integer p = Integer.valueOf(url[1]);
    				if(p!=null) {
    					this.port = p;
    				}
    			}
    		} else {
    			this.ip = ip_port;
    		}
    	}
	}

	public NodeTarget(String ip, int port) {
		this();
		this.ip = ip;
		this.port = port;
	}
	
	public boolean isLocal() {
		LambkitNode node = LambkitNodeManager.me().getNode();
		if(port == node.getPort()) {
			if(ip.equals(node.getIp()) || node.getLocalIps().containsValue(ip)) {
				return true;
			}
		}
		return false;
	}
	
	public String getHttp() {
		return "http://" + ip + ":" + port;
	}
	
	public String getIpPort() {
		return ip + ":" + port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
