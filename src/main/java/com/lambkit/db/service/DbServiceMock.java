package com.lambkit.db.service;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.ICallback;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;

public class DbServiceMock implements DbService {

	@Override
	public <T> List<T> query(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> query(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T queryFirst(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T queryFirst(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T queryColumn(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T queryColumn(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String queryStr(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String queryStr(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer queryInt(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer queryInt(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long queryLong(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long queryLong(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double queryDouble(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double queryDouble(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Float queryFloat(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Float queryFloat(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal queryBigDecimal(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal queryBigDecimal(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] queryBytes(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] queryBytes(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date queryDate(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date queryDate(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Time queryTime(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Time queryTime(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp queryTimestamp(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp queryTimestamp(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean queryBoolean(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean queryBoolean(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Short queryShort(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Short queryShort(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Byte queryByte(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Byte queryByte(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Number queryNumber(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Number queryNumber(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(String sql) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Record> find(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Record> find(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Record> findAll(String tableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Record findFirst(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Record findFirst(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Record findById(String tableName, Object idValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Record findById(String tableName, String primaryKey, Object idValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Record findByIds(String tableName, String primaryKey, Object... idValues) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteById(String tableName, Object idValue) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteById(String tableName, String primaryKey, Object idValue) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteByIds(String tableName, String primaryKey, Object... idValues) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(String tableName, String primaryKey, Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(String tableName, Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int delete(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(String sql) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, String select, String sqlExceptSelect, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, String select, String sqlExceptSelect) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, boolean isGroupBySql, String select,
			String sqlExceptSelect, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Record> paginateByFullSql(int pageNumber, int pageSize, String totalRowSql, String findSql,
			Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Record> paginateByFullSql(int pageNumber, int pageSize, boolean isGroupBySql, String totalRowSql,
			String findSql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean save(String tableName, String primaryKey, Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean save(String tableName, Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(String tableName, String primaryKey, Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(String tableName, Record record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object execute(ICallback callback) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean tx(int transactionLevel, IAtom atom) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tx(IAtom atom) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Record> findByCache(String cacheName, Object key, String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Record> findByCache(String cacheName, Object key, String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Record findFirstByCache(String cacheName, Object key, String sql, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Record findFirstByCache(String cacheName, Object key, String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Record> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize, String select,
			String sqlExceptSelect, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Record> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize, String select,
			String sqlExceptSelect) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Record> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize,
			boolean isGroupBySql, String select, String sqlExceptSelect, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] batch(String sql, Object[][] paras, int batchSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] batch(String sql, String columns, List modelOrRecordList, int batchSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] batch(List<String> sqlList, int batchSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] batchSave(List<? extends Model> modelList, int batchSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] batchSave(String tableName, List<Record> recordList, int batchSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] batchUpdate(List<? extends Model> modelList, int batchSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] batchUpdate(String tableName, String primaryKey, List<Record> recordList, int batchSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] batchUpdate(String tableName, List<Record> recordList, int batchSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSql(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SqlPara getSqlPara(String key, Record record) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SqlPara getSqlPara(String key, Model model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SqlPara getSqlPara(String key, Map data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SqlPara getSqlPara(String key, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SqlPara getSqlParaByString(String content, Map data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SqlPara getSqlParaByString(String content, Object... paras) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Record> find(SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Record findFirst(SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, boolean isGroupBySql, SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getConfigName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setConfigName(String configName) {
		// TODO Auto-generated method stub

	}

}
