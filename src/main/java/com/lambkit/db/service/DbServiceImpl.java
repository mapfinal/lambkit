package com.lambkit.db.service;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.ICallback;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;

public class DbServiceImpl implements DbService {
	
	private String configName;

	private DbPro db() {
		if(StrKit.notBlank(getConfigName())) {
			return Db.use(getConfigName());
		}
		return Db.use();
	}
	
	@Override
	public <T> List<T> query(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().query(sql, paras);
	}

	@Override
	public <T> List<T> query(String sql) {
		// TODO Auto-generated method stub
		return db().query(sql);
	}

	@Override
	public <T> T queryFirst(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryFirst(sql, paras);
	}

	@Override
	public <T> T queryFirst(String sql) {
		// TODO Auto-generated method stub
		return db().queryFirst(sql);
	}

	@Override
	public <T> T queryColumn(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryColumn(sql, paras);
	}

	@Override
	public <T> T queryColumn(String sql) {
		// TODO Auto-generated method stub
		return db().queryColumn(sql);
	}

	@Override
	public String queryStr(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryStr(sql, paras);
	}

	@Override
	public String queryStr(String sql) {
		// TODO Auto-generated method stub
		return db().queryStr(sql);
	}

	@Override
	public Integer queryInt(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryInt(sql, paras);
	}

	@Override
	public Integer queryInt(String sql) {
		// TODO Auto-generated method stub
		return db().queryInt(sql);
	}

	@Override
	public Long queryLong(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryLong(sql, paras);
	}

	@Override
	public Long queryLong(String sql) {
		// TODO Auto-generated method stub
		return db().queryLong(sql);
	}

	@Override
	public Double queryDouble(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryDouble(sql, paras);
	}

	@Override
	public Double queryDouble(String sql) {
		// TODO Auto-generated method stub
		return db().queryDouble(sql);
	}

	@Override
	public Float queryFloat(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryFloat(sql, paras);
	}

	@Override
	public Float queryFloat(String sql) {
		// TODO Auto-generated method stub
		return db().queryFloat(sql);
	}

	@Override
	public BigDecimal queryBigDecimal(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryBigDecimal(sql, paras);
	}

	@Override
	public BigDecimal queryBigDecimal(String sql) {
		// TODO Auto-generated method stub
		return db().queryBigDecimal(sql);
	}

	@Override
	public byte[] queryBytes(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryBytes(sql, paras);
	}

	@Override
	public byte[] queryBytes(String sql) {
		// TODO Auto-generated method stub
		return db().queryBytes(sql);
	}

	@Override
	public Date queryDate(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryDate(sql, paras);
	}

	@Override
	public Date queryDate(String sql) {
		// TODO Auto-generated method stub
		return db().queryDate(sql);
	}

	@Override
	public Time queryTime(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryTime(sql, paras);
	}

	@Override
	public Time queryTime(String sql) {
		// TODO Auto-generated method stub
		return db().queryTime(sql);
	}

	@Override
	public Timestamp queryTimestamp(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryTimestamp(sql, paras);
	}

	@Override
	public Timestamp queryTimestamp(String sql) {
		// TODO Auto-generated method stub
		return db().queryTimestamp(sql);
	}

	@Override
	public Boolean queryBoolean(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryBoolean(sql, paras);
	}

	@Override
	public Boolean queryBoolean(String sql) {
		// TODO Auto-generated method stub
		return db().queryBoolean(sql);
	}

	@Override
	public Short queryShort(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryShort(sql, paras);
	}

	@Override
	public Short queryShort(String sql) {
		// TODO Auto-generated method stub
		return db().queryShort(sql);
	}

	@Override
	public Byte queryByte(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryByte(sql, paras);
	}

	@Override
	public Byte queryByte(String sql) {
		// TODO Auto-generated method stub
		return db().queryByte(sql);
	}

	@Override
	public Number queryNumber(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().queryNumber(sql, paras);
	}

	@Override
	public Number queryNumber(String sql) {
		// TODO Auto-generated method stub
		return db().queryNumber(sql);
	}

	@Override
	public int update(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().update(sql, paras);
	}

	@Override
	public int update(String sql) {
		// TODO Auto-generated method stub
		return db().update(sql);
	}

	@Override
	public List<Record> find(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().find(sql, paras);
	}

	@Override
	public List<Record> find(String sql) {
		// TODO Auto-generated method stub
		return db().find(sql);
	}

	@Override
	public List<Record> findAll(String tableName) {
		// TODO Auto-generated method stub
		return db().findAll(tableName);
	}

	@Override
	public Record findFirst(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().findFirst(sql, paras);
	}

	@Override
	public Record findFirst(String sql) {
		// TODO Auto-generated method stub
		return db().findFirst(sql);
	}

	@Override
	public Record findById(String tableName, Object idValue) {
		// TODO Auto-generated method stub
		return db().findById(tableName, idValue);
	}

	@Override
	public Record findById(String tableName, String primaryKey, Object idValue) {
		// TODO Auto-generated method stub
		return db().findById(tableName, primaryKey, idValue);
	}

	@Override
	public Record findByIds(String tableName, String primaryKey, Object... idValues) {
		// TODO Auto-generated method stub
		return db().findByIds(tableName, primaryKey, idValues);
	}

	@Override
	public boolean deleteById(String tableName, Object idValue) {
		// TODO Auto-generated method stub
		return db().deleteById(tableName, idValue);
	}

	@Override
	public boolean deleteById(String tableName, String primaryKey, Object idValue) {
		// TODO Auto-generated method stub
		return db().deleteById(tableName, primaryKey, idValue);
	}

	@Override
	public boolean deleteByIds(String tableName, String primaryKey, Object... idValues) {
		// TODO Auto-generated method stub
		return db().deleteByIds(tableName, primaryKey, idValues);
	}

	@Override
	public boolean delete(String tableName, String primaryKey, Record record) {
		// TODO Auto-generated method stub
		return db().delete(tableName, primaryKey, record);
	}

	@Override
	public boolean delete(String tableName, Record record) {
		// TODO Auto-generated method stub
		return db().delete(tableName, record);
	}

	@Override
	public int delete(String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().delete(sql, paras);
	}

	@Override
	public int delete(String sql) {
		// TODO Auto-generated method stub
		return db().delete(sql);
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, String select, String sqlExceptSelect, Object... paras) {
		// TODO Auto-generated method stub
		return db().paginate(pageNumber, pageSize, select, sqlExceptSelect, paras);
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, String select, String sqlExceptSelect) {
		// TODO Auto-generated method stub
		return db().paginate(pageNumber, pageSize, select, sqlExceptSelect);
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, boolean isGroupBySql, String select,
			String sqlExceptSelect, Object... paras) {
		// TODO Auto-generated method stub
		return db().paginate(pageNumber, pageSize, isGroupBySql, select, sqlExceptSelect, paras);
	}

	@Override
	public Page<Record> paginateByFullSql(int pageNumber, int pageSize, String totalRowSql, String findSql,
			Object... paras) {
		// TODO Auto-generated method stub
		return db().paginateByFullSql(pageNumber, pageSize, totalRowSql, findSql, paras);
	}

	@Override
	public Page<Record> paginateByFullSql(int pageNumber, int pageSize, boolean isGroupBySql, String totalRowSql,
			String findSql, Object... paras) {
		// TODO Auto-generated method stub
		return db().paginateByFullSql(pageNumber, pageSize, isGroupBySql, totalRowSql, findSql, paras);
	}

	@Override
	public boolean save(String tableName, String primaryKey, Record record) {
		// TODO Auto-generated method stub
		return db().save(tableName, primaryKey, record);
	}

	@Override
	public boolean save(String tableName, Record record) {
		// TODO Auto-generated method stub
		return db().save(tableName, record);
	}

	@Override
	public boolean update(String tableName, String primaryKey, Record record) {
		// TODO Auto-generated method stub
		return db().update(tableName, primaryKey, record);
	}

	@Override
	public boolean update(String tableName, Record record) {
		// TODO Auto-generated method stub
		return db().update(tableName, record);
	}

	@Override
	public Object execute(ICallback callback) {
		// TODO Auto-generated method stub
		return db().execute(callback);
	}

	@Override
	public boolean tx(int transactionLevel, IAtom atom) {
		// TODO Auto-generated method stub
		return db().tx(transactionLevel, atom);
	}

	@Override
	public boolean tx(IAtom atom) {
		// TODO Auto-generated method stub
		return db().tx(atom);
	}

	@Override
	public List<Record> findByCache(String cacheName, Object key, String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().findByCache(cacheName, key, sql, paras);
	}

	@Override
	public List<Record> findByCache(String cacheName, Object key, String sql) {
		// TODO Auto-generated method stub
		return db().findByCache(cacheName, key, sql);
	}

	@Override
	public Record findFirstByCache(String cacheName, Object key, String sql, Object... paras) {
		// TODO Auto-generated method stub
		return db().findFirstByCache(cacheName, key, sql, paras);
	}

	@Override
	public Record findFirstByCache(String cacheName, Object key, String sql) {
		// TODO Auto-generated method stub
		return db().findFirstByCache(cacheName, key, sql);
	}

	@Override
	public Page<Record> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize, String select,
			String sqlExceptSelect, Object... paras) {
		// TODO Auto-generated method stub
		return db().paginateByCache(cacheName, key, pageNumber, pageSize, select, sqlExceptSelect, paras);
	}

	@Override
	public Page<Record> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize, String select,
			String sqlExceptSelect) {
		// TODO Auto-generated method stub
		return db().paginateByCache(cacheName, key, pageNumber, pageSize, select, sqlExceptSelect);
	}

	@Override
	public Page<Record> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize,
			boolean isGroupBySql, String select, String sqlExceptSelect, Object... paras) {
		// TODO Auto-generated method stub
		return db().paginateByCache(cacheName, key, pageNumber, pageSize, isGroupBySql, select, sqlExceptSelect, paras);
	}

	@Override
	public int[] batch(String sql, Object[][] paras, int batchSize) {
		// TODO Auto-generated method stub
		return db().batch(sql, paras, batchSize);
	}

	@Override
	public int[] batch(String sql, String columns, List modelOrRecordList, int batchSize) {
		// TODO Auto-generated method stub
		return db().batch(sql, columns, modelOrRecordList, batchSize);
	}

	@Override
	public int[] batch(List<String> sqlList, int batchSize) {
		// TODO Auto-generated method stub
		return db().batch(sqlList, batchSize);
	}

	@Override
	public int[] batchSave(List<? extends Model> modelList, int batchSize) {
		// TODO Auto-generated method stub
		return db().batchSave(modelList, batchSize);
	}

	@Override
	public int[] batchSave(String tableName, List<Record> recordList, int batchSize) {
		// TODO Auto-generated method stub
		return db().batchSave(tableName, recordList, batchSize);
	}

	@Override
	public int[] batchUpdate(List<? extends Model> modelList, int batchSize) {
		// TODO Auto-generated method stub
		return db().batchUpdate(modelList, batchSize);
	}

	@Override
	public int[] batchUpdate(String tableName, String primaryKey, List<Record> recordList, int batchSize) {
		// TODO Auto-generated method stub
		return db().batchUpdate(tableName, primaryKey, recordList, batchSize);
	}

	@Override
	public int[] batchUpdate(String tableName, List<Record> recordList, int batchSize) {
		// TODO Auto-generated method stub
		return db().batchUpdate(tableName, recordList, batchSize);
	}

	@Override
	public String getSql(String key) {
		// TODO Auto-generated method stub
		return db().getSql(key);
	}

	@Override
	public SqlPara getSqlPara(String key, Record record) {
		// TODO Auto-generated method stub
		return db().getSqlPara(key, record);
	}

	@Override
	public SqlPara getSqlPara(String key, Model model) {
		// TODO Auto-generated method stub
		return db().getSqlPara(key, model);
	}

	@Override
	public SqlPara getSqlPara(String key, Map data) {
		// TODO Auto-generated method stub
		return db().getSqlPara(key, data);
	}

	@Override
	public SqlPara getSqlPara(String key, Object... paras) {
		// TODO Auto-generated method stub
		return db().getSqlPara(key, paras);
	}

	@Override
	public SqlPara getSqlParaByString(String content, Map data) {
		// TODO Auto-generated method stub
		return db().getSqlParaByString(content, data);
	}

	@Override
	public SqlPara getSqlParaByString(String content, Object... paras) {
		// TODO Auto-generated method stub
		return db().getSqlParaByString(content, paras);
	}

	@Override
	public List<Record> find(SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return db().find(sqlPara);
	}

	@Override
	public Record findFirst(SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return db().findFirst(sqlPara);
	}

	@Override
	public int update(SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return db().update(sqlPara);
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return db().paginate(pageNumber, pageSize, sqlPara);
	}

	@Override
	public Page<Record> paginate(int pageNumber, int pageSize, boolean isGroupBySql, SqlPara sqlPara) {
		// TODO Auto-generated method stub
		return db().paginate(pageNumber, pageSize, isGroupBySql, sqlPara);
	}

	@Override
	public String getConfigName() {
		// TODO Auto-generated method stub
		return configName;
	}

	@Override
	public void setConfigName(String configName) {
		// TODO Auto-generated method stub
		this.configName = configName;
	}

}
