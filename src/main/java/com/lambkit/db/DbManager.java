/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db;

import com.google.common.collect.Maps;
import com.jfinal.config.Plugins;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbPro;
import com.lambkit.common.util.StringUtils;
import com.lambkit.core.aop.AopKit;
import com.lambkit.db.datasource.DataSourceConfig;
import com.lambkit.db.datasource.DataSourceConfigManager;
import com.lambkit.db.dialect.LambkitDialect;

import java.util.*;

/**
 * 数据库 管理
 */
public class DbManager {
    private static DbManager manager;

    private Map<String, LambkitDataSource> dataSources = null;

    public static DbManager me() {
        if (manager == null) {
            manager = AopKit.singleton(DbManager.class);
        }
        return manager;
    }
    
    public LambkitDataSource start(DataSourceConfig datasourceConfig) {
    	if(!datasourceConfig.isConfigOk() || StrKit.isBlank(datasourceConfig.getName())) return null;
    	addDataSource(datasourceConfig);
    	LambkitDataSource dbw = getDataSource(datasourceConfig.getName());
    	if(dbw!=null) dbw.start();
    	return dbw;
    }
    
    public LambkitDataSource stop(DataSourceConfig datasourceConfig) {
    	if(StrKit.isBlank(datasourceConfig.getName())) return null;
    	LambkitDataSource dbw = getDataSource(datasourceConfig.getName());
    	if(dbw!=null) dbw.stop();
    	return dbw;
    }
    
    /**
     * 加入数据源
     * @param datasourceConfig
     * @return
     */
    public LambkitDataSource addDataSource(DataSourceConfig datasourceConfig) {
    	if (datasourceConfig.isConfigOk()) {
    		LambkitDataSource db = new LambkitDataSource(datasourceConfig);
        	getDataSources().put(datasourceConfig.getName(), db);
        	return db;
        }
    	return null;
    }
    
    /**
     * 监听动态数据源存活状态，并移除长时间停用的数据源
     * @return
     */
    public int monitorDynamicDataSourceActive() {
    	return 0;
    }
    
    /**
     * 数据源加入Table
     * @param configName
     * @param table
     */
    public void addTable(String configName, LambkitTable table) {
    	LambkitDataSource db = getDataSources().get(configName);
    	if(db!=null) {
    		db.addTable(table);
    	}
    }
    
    public LambkitDataSource getDataSource(String configName) {
    	return getDataSources().get(configName);
    }
    
    public LambkitDataSource removeDataSource(String configName) {
    	return getDataSources().remove(configName);
    }

    public Map<String, LambkitDataSource> init(Plugins plugin) {
        // 所有的数据源
        Map<String, DataSourceConfig> datasourceConfigs = DataSourceConfigManager.me().getDatasourceConfigs();
        // 分库的数据源，一个数据源包含了多个数据源。
        Map<String, DataSourceConfig> shardingDatasourceConfigs = DataSourceConfigManager.me().getShardingDatasourceConfigs();
        if (shardingDatasourceConfigs != null && shardingDatasourceConfigs.size() > 0) {
            for (Map.Entry<String, DataSourceConfig> entry : shardingDatasourceConfigs.entrySet()) {
                String databaseConfig = entry.getValue().getShardingDatabase();
                if (StringUtils.isBlank(databaseConfig)) {
                    continue;
                }
                Set<String> databases = StringUtils.splitToSet(databaseConfig, ",");
                for (String database : databases) {
                    DataSourceConfig datasourceConfig = datasourceConfigs.remove(database);
                    if (datasourceConfig == null) {
                        throw new NullPointerException("has no datasource config named " + database + ",plase check your sharding database config");
                    }
                    entry.getValue().addChildDatasourceConfig(datasourceConfig);
                }
            }
        }
        //所有数据源，包含了分库的和未分库的
        Map<String, DataSourceConfig> allDatasourceConfigs = new HashMap<>();
        if (datasourceConfigs != null) {
            allDatasourceConfigs.putAll(datasourceConfigs);
        }
        if (shardingDatasourceConfigs != null) {
            allDatasourceConfigs.putAll(shardingDatasourceConfigs);
        }
        for (Map.Entry<String, DataSourceConfig> entry : allDatasourceConfigs.entrySet()) {
            DataSourceConfig datasourceConfig = entry.getValue();
            LambkitDataSource db = addDataSource(datasourceConfig);
            db.setDynamicDataSource(false);
            if (db!=null) {
            	plugin.add(db.getDataSourcePlugin());
            }
        }
        return getDataSources();
    }


	public Map<String, LambkitDataSource> getDataSources() {
		if(dataSources==null) {
			dataSources = Maps.newConcurrentMap();
		}
		return dataSources;
	}
	
	
//	public DbPro db(String configName) {
//		LambkitDataSource ds = getDataSource(configName);
//		return ds!=null ? ds.db() : null;
//	}
	 
	public DbPro db(String datasourceConfigName) {
		if(StrKit.isBlank(datasourceConfigName)) {
			return Db.use();
		} else {
			return Db.use(datasourceConfigName);
		}
	}
	
	public LambkitDialect getDialect(String datasourceConfigName) {
		DbPro dp = db(datasourceConfigName);
		if(dp!=null) {
			return (LambkitDialect) dp.getConfig().getDialect();
		} else {
			return null;
		}
	}
}
