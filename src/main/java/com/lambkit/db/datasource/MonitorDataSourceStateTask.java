package com.lambkit.db.datasource;

import java.util.Map;

import com.lambkit.db.DbManager;
import com.lambkit.db.LambkitDataSource;

/**
 * 监控数据源，并移除睡眠状态的数据源
 * @author yangyong
 *
 */
public class MonitorDataSourceStateTask implements Runnable {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Map<String, LambkitDataSource> dataSources = DbManager.me().getDataSources();
		for (String configName : dataSources.keySet()) {
			LambkitDataSource ds = dataSources.get(configName);
			if(ds.isDynamicDataSource()) {
				int status = ds.getDataSourcePlugin().getDataSourceActiveState();
				if(status==2) {
					ds.stop();
					DbManager.me().removeDataSource(configName);
				}
			}//if
		}//for
	}

}
