package com.lambkit.db.datasource;

import com.lambkit.db.DataSourcePlugin;
import com.zaxxer.hikari.HikariDataSource;

public class HikariDataSourcePlugin extends DataSourcePlugin {

	private HikariDataSource hikariDataSource;
	
	public HikariDataSourcePlugin(String configName, HikariDataSource hikariDataSource) {
		super(configName);
		this.setHikariDataSource(hikariDataSource);
	}

	@Override
	public boolean start() {
		// TODO Auto-generated method stub
		return startArp();
	}

	@Override
	public boolean stop() {
		// TODO Auto-generated method stub
		return stopArp();
	}

	@Override
	public int getDataSourceActiveState() {
		// TODO Auto-generated method stub
		return hikariDataSource == null || getArp()==null ? 0 : 1;
	}
	
	@Override
	public HikariDataSource getDataSource() {
		// TODO Auto-generated method stub
		return hikariDataSource;
	}

	public HikariDataSource getHikariDataSource() {
		return hikariDataSource;
	}

	public void setHikariDataSource(HikariDataSource hikariDataSource) {
		this.hikariDataSource = hikariDataSource;
	}
}
