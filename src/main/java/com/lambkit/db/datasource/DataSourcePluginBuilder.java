/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.datasource;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.lambkit.common.exception.LambkitException;
import com.lambkit.common.util.StringUtils;
import com.lambkit.core.aop.AopKit;
import com.lambkit.db.LambkitTable;
import com.lambkit.db.DataSourcePlugin;
import com.lambkit.db.LambkitDataSource;
import com.lambkit.db.TableManager;

import io.shardingsphere.api.config.ShardingRuleConfiguration;
import io.shardingsphere.api.config.TableRuleConfiguration;
import io.shardingsphere.api.config.strategy.ShardingStrategyConfiguration;
import io.shardingsphere.core.keygen.KeyGenerator;
import io.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory;

public class DataSourcePluginBuilder {

	private DataSourceConfig datasourceConfig;

	public DataSourcePluginBuilder(DataSourceConfig datasourceConfig) {
		this.datasourceConfig = datasourceConfig;
	}

	public DataSourcePlugin build(LambkitDataSource datasource) {
		if (datasourceConfig.isShardingEnable()) {
			return createShardingDataSource(datasource);
		} else {
			return createRecordPlugin(datasourceConfig);
		}
	}

	private DataSourcePlugin createShardingDataSource(LambkitDataSource datasource) {
		String configName = datasourceConfig.getName();
		DataSourceShardingPlugin shardingPlugin = new DataSourceShardingPlugin(configName);
		Map<String, DataSource> dataSourceMap = new HashMap<>();

		if (datasourceConfig.getChildDatasourceConfigs() != null) {
			for (DataSourceConfig childConfig : datasourceConfig.getChildDatasourceConfigs()) {
				DataSourcePlugin ds = createDataSource(childConfig);
				shardingPlugin.add(ds);
				dataSourceMap.put(childConfig.getName(), ds.getDataSource());
			}
		}
		/**
		 * 可能只是分表，不分库
		 */
		else {
			DataSourcePlugin ds = createDataSource(datasourceConfig);
			shardingPlugin.add(ds);
			dataSourceMap.put(datasourceConfig.getName(), ds.getDataSource());
		}

		ShardingRuleConfiguration shardingRuleConfiguration = new ShardingRuleConfiguration();

		List<LambkitTable> tableInfos = TableManager.me().getTablesInfos(datasourceConfig.getTable(),
				datasourceConfig.getExcludeTable());
		StringBuilder bindTableGroups = new StringBuilder();
		for (LambkitTable ti : tableInfos) {
			TableRuleConfiguration tableRuleConfiguration = getTableRuleConfiguration(ti);
			shardingRuleConfiguration.getTableRuleConfigs().add(tableRuleConfiguration);
			bindTableGroups.append(ti.getTableName()).append(",");
		}

		if (bindTableGroups.length() > 0) {
			bindTableGroups.deleteCharAt(bindTableGroups.length() - 1); // delete last char
			shardingRuleConfiguration.getBindingTableGroups().add(bindTableGroups.toString());
		}

		try {
			DataSource shardingDataSource = ShardingDataSourceFactory.createDataSource(dataSourceMap,
					shardingRuleConfiguration, new HashMap<String, Object>(), new Properties());
			shardingPlugin.setShardingDataSource(shardingDataSource);

			ActiveRecordPlugin activeRecordPlugin = StringUtils.isNotBlank(configName)
					? new ActiveRecordPlugin(configName, shardingDataSource)
					: new ActiveRecordPlugin(shardingDataSource);
			shardingPlugin.setArp(activeRecordPlugin);
			return shardingPlugin;
		} catch (SQLException e) {
			throw new LambkitException(e);
		}
	}

	private TableRuleConfiguration getTableRuleConfiguration(LambkitTable tableInfo) {
		TableRuleConfiguration tableRuleConfig = new TableRuleConfiguration();
		tableRuleConfig.setLogicTable(tableInfo.getTableName());

		if (StrKit.notBlank(tableInfo.getActualDataNodes())) {
			tableRuleConfig.setActualDataNodes(tableInfo.getActualDataNodes());
		}

		if (tableInfo.getKeyGeneratorClass() != KeyGenerator.class) {
			tableRuleConfig.setKeyGenerator(AopKit.instance(tableInfo.getKeyGeneratorClass()));
		}

		if (StrKit.notBlank(tableInfo.getKeyGeneratorColumnName())) {
			tableRuleConfig.setKeyGeneratorColumnName(tableInfo.getKeyGeneratorColumnName());
		}

		if (tableInfo.getDatabaseShardingStrategyConfig() != ShardingStrategyConfiguration.class) {
			tableRuleConfig
					.setDatabaseShardingStrategyConfig(AopKit.instance(tableInfo.getDatabaseShardingStrategyConfig()));
		}

		if (tableInfo.getTableShardingStrategyConfig() != ShardingStrategyConfiguration.class) {
			tableRuleConfig.setTableShardingStrategyConfig(AopKit.instance(tableInfo.getTableShardingStrategyConfig()));
		}

		return tableRuleConfig;
	}

	private DataSourcePlugin createDataSource(DataSourceConfig dataSourceConfig) {
		DataSourceFactory factory = AopKit.get(dataSourceConfig.getFactory());
		if (factory == null) {
			factory = new DruidDataSourceFactory();
		}
		DataSourcePlugin plugin = factory.createDataSource(dataSourceConfig);
		return plugin;
	}

	private DataSourcePlugin createRecordPlugin(DataSourceConfig dataSourceConfig) {
		DataSourceFactory factory = null;
		if (StrKit.notBlank(dataSourceConfig.getFactory())) {
			factory = AopKit.get(dataSourceConfig.getFactory());
		}
		if (factory == null) {
			factory = new DruidDataSourceFactory();
		}
		return factory.createDatasourcePlugin(dataSourceConfig);
	}
}
