package com.lambkit.db.datasource;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Config;
import com.jfinal.plugin.activerecord.Model;
import com.lambkit.common.LambkitManager;
import com.lambkit.common.bean.TableMappingBean;
import com.lambkit.db.DbManager;
import com.lambkit.db.LambkitTable;
import com.lambkit.db.TableManager;

public class ActiveRecordPluginWrapper {

	private ActiveRecordPlugin arp;
	
	public ActiveRecordPluginWrapper(ActiveRecordPlugin arp) {
		this.arp = arp;
	}
	
	public ActiveRecordPlugin getActiveRecordPlugin() {
		return this.arp;
	}

	public Config getConfig() {
		return this.arp.getConfig();
	}

	public ActiveRecordPluginWrapper addMapping(String tableName, String primaryKey, Class<? extends Model<?>> modelClass) {
		arp.addMapping(tableName, primaryKey, modelClass);
		String configName = arp.getConfig().getName();
		LambkitManager.me().addMapping(new TableMappingBean(configName, tableName, primaryKey, modelClass.getName()));
		LambkitTable tableWrapper = TableManager.me().addMapping(configName, tableName, primaryKey, modelClass);
		DbManager.me().addTable(configName, tableWrapper);
		return this;
	}

	public ActiveRecordPluginWrapper addMapping(String tableName, Class<? extends Model<?>> modelClass) {
		arp.addMapping(tableName, modelClass);
		String configName = arp.getConfig().getName();
		LambkitManager.me().addMapping(new TableMappingBean(configName, tableName, "", modelClass.getName()));
		LambkitTable tableWrapper = TableManager.me().addMapping(configName, tableName, "", modelClass);
		DbManager.me().addTable(configName, tableWrapper);
		return this;
	}

	public ActiveRecordPluginWrapper addSqlTemplate(String sqlTemplate) {
		arp.addSqlTemplate(sqlTemplate);
		return this;
	}

	public ActiveRecordPluginWrapper addSqlTemplate(com.jfinal.template.source.ISource sqlTemplate) {
		arp.addSqlTemplate(sqlTemplate);
		return this;
	}

	public ActiveRecordPluginWrapper setBaseSqlTemplatePath(String baseSqlTemplatePath) {
		arp.setBaseSqlTemplatePath(baseSqlTemplatePath);
		return this;
	}
}
