package com.lambkit.db.sql.template;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.common.LambkitResult;
import com.lambkit.common.ResultKit;
import com.lambkit.db.sql.IQuery;

public class QuerySqlTemplate implements IQuery {
	
	private String dbConfig = null;
	private SqlTemplateStore templateStore;

	public QuerySqlTemplate(SqlTemplateStore templateStore) {
		this.templateStore = templateStore;
	}

	public QuerySqlTemplate(SqlTemplateStore templateStore, String dbConfig) {
		this.templateStore = templateStore;
		this.dbConfig = dbConfig;
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "sqltemplate";
	}

	public LambkitResult sql(JSONObject jsonObj) {
		if (jsonObj == null) {
			return ResultKit.json(0, "json invalid", null);
		}
		String name = jsonObj.getString("name");
		if (StrKit.isBlank(name)) {
			return ResultKit.json(0, "name invalid", null);
		}
		String sqlTemplate = getSqlTemplate(name);
		return ResultKit.json(1, "sqltemplate", sqlTemplate);
	}

	/**
	 * 前端传入json，返回查询结果
	 * 
	 * @param json
	 * @param queryType 0-all,1-select,2-insert,3-update,4-delete
	 * @return
	 */
	public LambkitResult query(String json, int queryType) {
		if (StrKit.isBlank(json)) {
			return ResultKit.json(0, "json null", null);
		}
		JSONObject jsonObj = JSON.parseObject(json);
		return query(jsonObj, queryType);
	}

	public LambkitResult query(JSONObject jsonObj, int queryType) {

		if (jsonObj == null) {
			return ResultKit.json(0, "json invalid", null);
		}
		String name = jsonObj.getString("name");
		if (StrKit.isBlank(name)) {
			return ResultKit.json(0, "name invalid", null);
		}
		String type = jsonObj.getString("method");
		type = StrKit.isBlank(type) ? "query" : type.trim();
		// Map paras = jsonObj.getObject("param", Map.class);
		Object paras = jsonObj.get("param");
		Integer pageNumber = jsonObj.getInteger("pageNumber");
		Integer pageSize = jsonObj.getInteger("pageSize");
		if ("update".equalsIgnoreCase(type)) {
			if (queryType == 0 || queryType == 3) {
				int u = update(name, paras);
				return ResultKit.json(1, name + " updated", u);
			} else {
				return ResultKit.json(0, "unsupprot update", "不支持更新操作");
			}
		} else if ("delete".equalsIgnoreCase(type)) {
			if (queryType == 0 || queryType == 4) {
				int d = delete(name, paras);
				return ResultKit.json(1, name + " deleted", d);
			} else {
				return ResultKit.json(0, "unsupprot delete", "不支持删除操作");
			}
		} else if ("save".equalsIgnoreCase(type)) {
			if (queryType == 0 || queryType == 2) {
				int d = update(name, paras);
				return ResultKit.json(1, name + " saved", d);
			} else {
				return ResultKit.json(0, "unsupprot save", "不支持保存操作");
			}
		} else {
			if (pageNumber == null || pageNumber == 0) {
				return ResultKit.json(1, "1 record selected", findFirst(name, paras));
			} else {
				if (pageSize == null || pageSize == 0) {
					List<Record> data = find(name, paras);
					return ResultKit.json(1, String.valueOf(data.size()) + " records selected", data);
				} else {
					return ResultKit.json(1, "1 page selected", paginate(name, pageNumber, pageSize, paras));
				}
			}
		}
	}

	public LambkitResult query(String name, String json, int queryType, Integer pageNumber, Integer pageSize) {
		if (StrKit.isBlank(json)) {
			return ResultKit.json(0, "json null", null);
		}
		Object paras = JSON.parse(json);
		if (paras == null) {
			return ResultKit.json(0, "json invalid", null);
		}
		if (StrKit.isBlank(name)) {
			return ResultKit.json(0, "name invalid", null);
		}
		if (queryType == 3 || queryType == 2) {
			int u = update(name, paras);
			return ResultKit.json(1, name + " updated", u);
		} else if (queryType == 4) {
			int d = delete(name, paras);
			return ResultKit.json(1, name + " deleted", d);
		} else {
			if (pageNumber == null || pageNumber == 0) {
				return ResultKit.json(1, "1 record selected", findFirst(name, paras));
			} else {
				if (pageSize == null || pageSize == 0) {
					List<Record> data = find(name, paras);
					return ResultKit.json(1, String.valueOf(data.size()) + " records selected", data);
				} else {
					return ResultKit.json(1, "1 page selected", paginate(name, pageNumber, pageSize, paras));
				}
			}
		}
	}

	public Record findFirst(String key, Object paras) {
		if (templateStore == null)
			return null;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		if (paras instanceof JSONObject) {
			JSONObject jsonObject = (JSONObject) paras;
			Map sqlParas = jsonObject.toJavaObject(Map.class);
			return Db.templateByString(sqlTemplate, sqlParas).findFirst();
		} else if (paras instanceof JSONArray) {
			JSONArray jsonArray = (JSONArray) paras;
			return Db.templateByString(sqlTemplate, jsonArray.toArray()).findFirst();
		} else {
			return null;
		}
	}

	public List<Record> find(String key, Object paras) {
		if (templateStore == null)
			return null;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		if (paras instanceof JSONObject) {
			JSONObject jsonObject = (JSONObject) paras;
			Map sqlParas = jsonObject.toJavaObject(Map.class);
			return Db.templateByString(sqlTemplate, sqlParas).find();
		} else if (paras instanceof JSONArray) {
			JSONArray jsonArray = (JSONArray) paras;
			return Db.templateByString(sqlTemplate, jsonArray.toArray()).find();
		} else {
			return null;
		}
	}

	public Page<Record> paginate(String key, int pageNumber, int pageSize, Object paras) {
		if (templateStore == null)
			return null;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		if (paras instanceof JSONObject) {
			JSONObject jsonObject = (JSONObject) paras;
			Map sqlParas = jsonObject.toJavaObject(Map.class);
			return Db.templateByString(sqlTemplate, sqlParas).paginate(pageNumber, pageSize);
		} else if (paras instanceof JSONArray) {
			JSONArray jsonArray = (JSONArray) paras;
			return Db.templateByString(sqlTemplate, jsonArray.toArray()).paginate(pageNumber, pageSize);
		} else {
			return null;
		}
	}

	public int update(String key, Object paras) {
		if (templateStore == null)
			return 0;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		if (paras instanceof JSONObject) {
			JSONObject jsonObject = (JSONObject) paras;
			Map sqlParas = jsonObject.toJavaObject(Map.class);
			return Db.templateByString(sqlTemplate, sqlParas).update();
		} else if (paras instanceof JSONArray) {
			JSONArray jsonArray = (JSONArray) paras;
			return Db.templateByString(sqlTemplate, jsonArray.toArray()).update();
		} else {
			return 0;
		}
	}

	public int delete(String key, Object paras) {
		if (templateStore == null)
			return 0;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		if (paras instanceof JSONObject) {
			JSONObject jsonObject = (JSONObject) paras;
			Map sqlParas = jsonObject.toJavaObject(Map.class);
			return Db.templateByString(sqlTemplate, sqlParas).delete();
		} else if (paras instanceof JSONArray) {
			JSONArray jsonArray = (JSONArray) paras;
			return Db.templateByString(sqlTemplate, jsonArray.toArray()).delete();
		} else {
			return 0;
		}
	}

	public Record findFirst(String key, Map paras) {
		if (templateStore == null)
			return null;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).findFirst();
	}

	public List<Record> find(String key, Map paras) {
		if (templateStore == null)
			return null;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).find();
	}

	public Page<Record> paginate(String key, int pageNumber, int pageSize, Map paras) {
		if (templateStore == null)
			return null;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).paginate(pageNumber, pageSize);
	}

	public int update(String key, Map paras) {
		if (templateStore == null)
			return 0;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).update();
	}

	public int delete(String key, Map paras) {
		if (templateStore == null)
			return 0;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).delete();
	}

	public Record findFirst(String key, Object... paras) {
		if (templateStore == null)
			return null;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).findFirst();
	}

	public List<Record> find(String key, Object... paras) {
		if (templateStore == null)
			return null;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).find();
	}

	public Page<Record> paginate(String key, int pageNumber, int pageSize, Object... paras) {
		if (templateStore == null)
			return null;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).paginate(pageNumber, pageSize);
	}

	public int update(String key, Object... paras) {
		if (templateStore == null)
			return 0;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).update();
	}

	public int delete(String key, Object... paras) {
		if (templateStore == null)
			return 0;
		String sqlTemplate = templateStore.getTemplate(key, dbConfig);
		return Db.templateByString(sqlTemplate, paras).delete();
	}

	public String getSqlTemplate(String key) {
		if (templateStore == null)
			return null;
		return templateStore.getTemplate(key, dbConfig);
	}

	public SqlTemplateStore getTemplateStore() {
		return templateStore;
	}

	public void setTemplateStore(SqlTemplateStore templateStore) {
		this.templateStore = templateStore;
	}

	public DbPro db() {
		if (StrKit.isBlank(dbConfig)) {
			return Db.use();
		} else {
			return Db.use(dbConfig);
		}
	}

	public String getDbConfig() {
		return dbConfig;
	}

	public void setDbConfg(String dbCofnig) {
		this.dbConfig = dbCofnig;
	}

	@Override
	public SqlPara getSqlPara() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getPage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getCount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPage(Integer page) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCount(Integer count) {
		// TODO Auto-generated method stub

	}

}
