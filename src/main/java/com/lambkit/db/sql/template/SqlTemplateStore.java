package com.lambkit.db.sql.template;

/**
 * SQL Template 模板存储
 * @author yangyong
 *
 */
public interface SqlTemplateStore {

	String getTemplate(String key, String config);
	
	void setTemplate(String key, String config, String sqlTemplate);
}
