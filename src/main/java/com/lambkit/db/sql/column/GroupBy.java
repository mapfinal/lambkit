package com.lambkit.db.sql.column;

import com.alibaba.fastjson.JSONObject;
import com.lambkit.db.meta.TableMeta;
import com.lambkit.db.mgr.MgrTable;

public class GroupBy {

	private ColumnsGroup having;
	private String name;
	
	public GroupBy() {
		// TODO Auto-generated constructor stub
	}
	
	public GroupBy(JSONObject jsonObject, MgrTable tbc) {
		// TODO Auto-generated constructor stub
		if(jsonObject.containsKey("by")) {
			setName(jsonObject.getString("by"));
		}
		if(jsonObject.containsKey("having")) {
			having = new ColumnsGroup();
			having.filter(jsonObject.getJSONObject("having"), tbc.getMeta());
		}
	}
	
	public GroupBy(JSONObject jsonObject, TableMeta meta) {
		// TODO Auto-generated constructor stub
		if(jsonObject.containsKey("by")) {
			setName(jsonObject.getString("by"));
		}
		if(jsonObject.containsKey("having")) {
			having = new ColumnsGroup();
			having.filter(jsonObject.getJSONObject("having"), meta);
		}
	}

	public ColumnsGroup getHaving() {
		return having;
	}
	
	public String getName() {
		return name;
	}
	
	public void setHaving(ColumnsGroup having) {
		this.having = having;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
