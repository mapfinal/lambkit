/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.sql.column;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.google.common.collect.Lists;
import com.jfinal.core.converter.TypeConverter;
import com.jfinal.kit.StrKit;
import com.lambkit.common.util.ArrayUtils;
import com.lambkit.common.util.StringUtils;
import com.lambkit.db.meta.JavaType;
import com.lambkit.db.sql.ConditionMode;

/**
 * Column 的工具类，用于方便组装sql
 */
public class Columns implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3093668879029947796L;
	
	private List<Column> cols = new ArrayList<>();
	private String inJunc = "and";//0-and, 1-or
	private String outJunc = "and";//0-and, 1-or, 2-not
	private List<String> message = null;
	private List<String> refs = null;
	private JavaType javaTypeMap = null;
	
	public Columns or() {
		setInJunc("or");
		return this;
	}
	
	public Columns and() {
		setInJunc("and");
		return this;
	}
	
	public Columns withOr() {
		setOutJunc("or");
		return this;
	}
	
	public Columns withAnd() {
		setOutJunc("and");
		return this;
	}
	
	public Columns withNot() {
		setOutJunc("not");
		return this;
	}
	
	public void addRef(String ref) {
 		if(refs==null) {
 			refs = Lists.newArrayList();
 		}
 		refs.add(ref);
 	}

	public List<String> getRefs() {
		return refs;
	}

	public void setRefs(List<String> refList) {
		this.refs = refList;
	}
	
	public void addMessage(String msg) {
		if(message==null) {
			message = Lists.newArrayList();
		}
		message.add(msg);
	}
	
	public List<String> getMessage() {
		return message;
	}
	
	public void setMessage(List<String> message) {
		this.message = message;
	}

    public static Columns create() {
        return new Columns();
    }
    
    public static Columns create(Column column) {
        Columns that = new Columns();
        that.cols.add(column);
        return that;

    }

    public static Columns create(String name, Object value) {
        return create().eq(name, value);
    }
    
    public static Columns by() {
        return new Columns();
    }
    
    public static Columns by(Column column) {
        Columns that = new Columns();
        that.cols.add(column);
        return that;
    }

    public static Columns by(String name, Object value) {
        return create().eq(name, value);
    }
    
    public static Columns byOr() {
    	return create().or();
    }
    
    public static Columns byOr(Column column) {
        Columns that = new Columns();
        that.or().add(column);
        return that;

    }

    public static Columns byOr(String name, Object value) {
        return create().or().eq(name, value);
    }

    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public Columns eq(String name, Object value) {
        cols.add(Column.create(name, value));
        return this;
    }
    
    public Columns add(String name, Object value)
    {
      return eq(name, value);
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public Columns ne(String name, Object value) {
        cols.add(Column.create(name, value, ConditionMode.NOT_EQUAL));
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public Columns like(String name, Object value) {
        cols.add(Column.create(name, value, ConditionMode.FUZZY));
        return this;
    }
    
    public Columns notLike(String name, Object value) {
        cols.add(Column.create(name, value, ConditionMode.NOT_FUZZY));
        return this;
    }
    
    public Columns likeAppendPercent(String name, Object value) {
    	if ((value == null) || (StringUtils.isBlank(value.toString()))) {
    		return this;
    	}
    	cols.add(Column.create(name, "%" + value + "%", ConditionMode.FUZZY));
    	return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public Columns gt(String name, Object value) {
        cols.add(Column.create(name, value, ConditionMode.GREATER_THEN));
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public Columns ge(String name, Object value) {
        cols.add(Column.create(name, value, ConditionMode.GREATER_EQUAL));
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public Columns lt(String name, Object value) {
        cols.add(Column.create(name, value, ConditionMode.LESS_THEN));
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public Columns le(String name, Object value) {
        cols.add(Column.create(name, value, ConditionMode.LESS_EQUAL));
        return this;
    }
    
    public Columns isnull(String name) {
        cols.add(Column.create(name, ConditionMode.ISNULL));
        return this;
    } 

    public Columns notNull(String name) {
        cols.add(Column.create(name, ConditionMode.NOT_NULL));
        return this;
    } 
    
    public Columns empty(String name) {
        cols.add(Column.create(name, ConditionMode.EMPTY));
        return this;
    } 
    
    public Columns notEmpty(String name) {
        cols.add(Column.create(name, ConditionMode.NOT_EMPTY));
        return this;
    } 
    
    public Columns in(String name, Object arrays) {
    	//System.out.println("list in: " + arrays);
        cols.add(Column.create(name, arrays, ConditionMode.IN));
        return this;
    } 
    
    public Columns notIn(String name, Object arrays) {
        cols.add(Column.create(name, arrays, ConditionMode.NOT_IN));
        return this;
    } 
    
    public Columns between(String name, Object start, Object end)
    {
      add(Column.create(name, start, end, ConditionMode.BETWEEN));
      return this;
    }
    
    public Columns notBetween(String name, Object start, Object end)
    {
      add(Column.create(name, start, end, ConditionMode.NOT_BETWEEN));
      return this;
    }
    
    public Columns add(Column column) {
    	cols.add(column);
    	return this;
    }
    
    public Columns addAll(List<Column> columns) {
    	cols.addAll(columns);
    	return this;
    }

    public List<Column> getList() {
        return cols;
    }

    public LinkedList<Object> getParams() {
    	LinkedList<Object> params = new LinkedList<Object>();
        if (ArrayUtils.isNotEmpty(cols)) {
            for (Column column : cols) {
                column.addValueToParam(params);
            }
        }
        return params;
    }

	public String getInJunc() {
		return inJunc;
	}

	public void setInJunc(String inJunc) {
		this.inJunc = inJunc;
	}

	public String getOutJunc() {
		return outJunc;
	}

	public void setOutJunc(String outJunc) {
		this.outJunc = outJunc;
	}

	public Columns column(String field, String value, String type, boolean javaType) {
		if(StrKit.notBlank(value)) {
			String info = value.trim();
			//System.out.println("Field: " + field + ", info: " + info);
			if(info.startsWith("{") && info.endsWith("}")) {
				info = info.substring(1, info.length()-1);
				if(info.equalsIgnoreCase("empty")) {
					empty(field);
				} else if(info.equalsIgnoreCase("not_empty")) {
					notEmpty(field);
				} else if(info.equalsIgnoreCase("isnull")) {
					isnull(field);
				} else if(info.equalsIgnoreCase("not_null")) {
					notNull(field);
				} 
			} else if(info.startsWith("@")) {
				//包含
				info = info.substring(1).trim();
				if(info.startsWith("-") || info.startsWith(",")) info = info.substring(1);
				if(info.endsWith("-") || info.endsWith(",")) info = info.substring(0, info.length()-1);
				if(info.contains("%")) {
					like(field, info);
				} else if(info.startsWith("=")) { 
					eq(field, getValue(type, info.substring(1), javaType));
				} else if(info.contains("-")) {
					String[] infos = info.split("-");
					between(field, getValue(type, infos[0], javaType), getValue(type, infos[1], javaType));
				} else if(info.contains(",") && !info.endsWith(",") && !info.endsWith(",%")) {
					String[] infos = info.split(",");
					if(infos.length > 0) {
						in(field, transArrayValue(type, infos, javaType));
					}
				} else {
					like(field, info);
				}
			} else if(info.startsWith("#")) {
				//不包含
				info = info.substring(1).trim();
				if(info.startsWith("-") || info.startsWith(",")) info = info.substring(1);
				if(info.endsWith("-") || info.endsWith(",")) info = info.substring(0, info.length()-1);
				if(info.contains("%")) {
					like(field, info);
				} else if(info.startsWith("=")) { 
					ne(field, getValue(type, info.substring(1), javaType));
				} else if(info.contains("-")) {
					String[] infos = info.split("-");
					notBetween(field, getValue(type, infos[0], javaType), getValue(type, infos[1], javaType));
				} else if(info.contains(",") && !info.endsWith(",") && !info.endsWith(",%")) {
					String[] infos = info.split(",");
					if(infos.length > 0) {
						notIn(field, transArrayValue(type, infos, javaType));
					}
				} else {
					notLike(field, info);
				}
			} else if(info.startsWith("!=")) { 
				//info = info.substring(2).trim();
				//if(info.startsWith("$") && info.endsWith("$")) info = info.substring(1, info.length()-1); 
				ne(field, getValue(type, info.substring(2), javaType));
			} else if(info.startsWith("<")) { 
				lt(field, getValue(type, info.substring(1), javaType));
			} else if(info.startsWith("<=")) { 
				le(field, getValue(type, info.substring(2), javaType));
			} else if(info.startsWith(">")) { 
				gt(field, getValue(type, info.substring(1), javaType));
			} else if(info.startsWith(">=")) { 
				ge(field, getValue(type, info.substring(2), javaType));
			} else if(info.contains("%")) {
				like(field, info);
			} else {
				eq(field, getValue(type, info, javaType));
			}
		}
		return this;
	}
	
	public Columns filter(String field, String value, String type, boolean javaType) {
		//System.out.println("filter0: " + value);
		if(StrKit.notBlank(value)) {
			String info = value.trim();
			if(info.startsWith("$")) {
				add(Column.createRefColumn(field, value));
				addRef(field);
			} else if(info.startsWith("@")) {
				return column(field, value, type, javaType);
			} else if(info.startsWith("#")) {
				return column(field, value, type, javaType);
			} else if(info.startsWith("!")) {
				info = info.substring(1).trim();
				//not
				if(info.startsWith("{") && info.endsWith("}")) {
					info = info.substring(1, info.length()-1);
					//null or empty
					if(info.equalsIgnoreCase("empty")) {
						notEmpty(field);
					} else if(info.equalsIgnoreCase("null")) {
						notNull(field);
					} 
				} else if(info.startsWith("[") && info.endsWith("]")) {
					//list
					info = info.substring(1, info.length()-1);
					String[] infos = info.split(",");
					if(infos.length > 0) {
						notIn(field, transArrayValue(type, infos, javaType));
					}
				} else if(info.startsWith("(") && info.endsWith(")")) {
					//多个过滤条件
					info = info.substring(1, info.length()-1);
					ColumnsGroup colg = new ColumnsGroup();
					filterGroup(colg, field, info, type, 0, false);
					colg.columnsWithAnd(this);
					return colg;
				} else {
					if(info.contains("%")) {
						String stType = getStType(type, javaType);
						if(stType.equals("string") || stType.equals("date")
								|| String.class.getName().equals(stType)
								|| Date.class.getName().equals(stType)
								|| Timestamp.class.getName().equals(stType)) {
							notLike(field, info);
						} else {
							addMessage(field + " (" + type + ") cannot use like " + info);
						}
					} else if(info.startsWith("=")) { 
						ne(field, getValue(type, info.substring(1), javaType));
					} else if(info.contains("-")) {
						String[] infos = info.split("-");
						notBetween(field, getValue(type, infos[0], javaType), getValue(type, infos[1], javaType));
//					} else if(info.contains(",") && !info.endsWith(",") && !info.endsWith(",%")) {
//						String[] infos = info.split(",");
//						if(infos.length > 0) {
//							notIn(field, transArrayValue(type, infos));
//						}
					} else {
						ne(field, info);
					}
				}
			} else {
				if(info.startsWith("{") && info.endsWith("}")) {
					//null or empty
					info = info.substring(1, info.length()-1);
					if(info.equalsIgnoreCase("empty")) {
						empty(field);
					} else if(info.equalsIgnoreCase("null")) {
						isnull(field);
					} 
				} else if(info.startsWith("[") && info.endsWith("]")) {
					//list
					info = info.substring(1, info.length()-1);
					String[] infos = info.split(",");
					if(infos.length > 0) {
						//System.out.println("list filter: " + field + "=" + info);
						in(field, transArrayValue(type, infos, javaType));
					}
				} else if(info.startsWith("(") && info.endsWith(")")) {
					//多个过滤条件
					info = info.substring(1, info.length()-1);
					ColumnsGroup colg = new ColumnsGroup();
					filterGroup(colg, field, info, type, 0, false);
					colg.columnsWithAnd(this);
					return colg;
				} else {
					if(info.contains("%")) {
						String stType = getStType(type, javaType);
						if(stType.equals("string") || stType.equals("date")
								|| String.class.getName().equals(stType)
								|| Date.class.getName().equals(stType)
								|| Timestamp.class.getName().equals(stType)) {
							like(field, info);
						} else {
							addMessage(field + " (" + type + ") cannot use like " + info);
						}
					} else if(info.startsWith("=")) { 
						eq(field, getValue(type, info.substring(1), javaType));
					} else if(info.contains("-")) {
						String[] infos = info.split("-");
						between(field, getValue(type, infos[0], javaType), getValue(type, infos[1], javaType));
//					} else if(info.contains(",") && !info.endsWith(",") && !info.endsWith(",%")) {
//						String[] infos = info.split(",");
//						if(infos.length > 0) {
//							in(field, transArrayValue(type, infos));
//						}
					} else if(info.startsWith("<")) { 
						lt(field, getValue(type, info.substring(1), javaType));
					} else if(info.startsWith("<=")) { 
						le(field, getValue(type, info.substring(2), javaType));
					} else if(info.startsWith(">")) { 
						gt(field, getValue(type, info.substring(1), javaType));
					} else if(info.startsWith(">=")) { 
						ge(field, getValue(type, info.substring(2), javaType));
					} else {
						eq(field, getValue(type, info, javaType));
					}
				}
			}
		}
		return this;
	}
	
	/**
	 * 解析 ((item) & (item)) | (item)
	 * @param columnsGroup
	 * @param field
	 * @param value
	 * @param type
	 * @param junc
	 */
	protected void filterGroup(ColumnsGroup columnsGroup, String field, String value, String type, int junc, boolean javaType) {
		//System.out.println("filter: " + value);
		if(StrKit.isBlank(value)) {
			return;
		}
		if(value.startsWith(")")) {
			value = value.substring(1).trim();
			filterGroup(columnsGroup, field, value, type, junc, javaType);
			return;
		}
		//((item) & (item)) | (item)
		//(item) & (item)) | (item)
		if(value.startsWith("(")) {
			value = value.substring(1).trim();
			if(value.startsWith("(")) {
				//(item) & (item)) | (item)
				ColumnsGroup colg = new ColumnsGroup();
				filterGroup(colg, field, value, type, junc, javaType);
				if(junc==0) {
					columnsGroup.columnsWithAnd(colg);
				} else {
					columnsGroup.columnsWithOr(colg);
				}
			} else {
				filterGroup(columnsGroup, field, value, type, junc, javaType);
			}
		} else if(value.startsWith("&")) {
			//& (item)) | (item)
			value = value.substring(1).trim();
			filterGroup(columnsGroup, field, value, type, 0, javaType);
		} else if(value.startsWith("|")){
			//| (item)
			value = value.substring(1).trim();
			filterGroup(columnsGroup, field, value, type, 1, javaType);
		} else {
			//item) & (item)) | (item)
			int t = value.indexOf(")");
			if(t>0) {
				String tval = value.substring(0, t);
				Columns col = new Columns();
				col.filter(field, tval, type, javaType);
				if(junc==0) {
					columnsGroup.columnsWithAnd(col);
				} else {
					columnsGroup.columnsWithOr(col);
				}
				value = value.substring(t+1).trim();
				//System.out.println("filter: " + value);
				filterGroup(columnsGroup, field, value, type, junc, javaType);
			}
		}
	}
	
	protected String getStType(String type, boolean javaType) {
		if(javaType) {
			return type;
		}
		type = type.toLowerCase();
		if(type.startsWith("bigint") || type.startsWith("bigserial") || type.startsWith("int8") || type.equals("int unsigned"))return "long";
 		else if(type.startsWith("int") || type.startsWith("serial") || type.startsWith("tinyint"))return "int";
 		else if(type.startsWith("float"))return "double";
 		else if(type.startsWith("double"))return "double";
 		else if(type.startsWith("num"))return "double";//numeric,number
 		else if(type.startsWith("date")) return "date";
 		else if(type.startsWith("datetime")) return "date";
 		else if(type.startsWith("timestamp")) return "date";
 		else return "string";
	}
	
	protected Object getValue(String type, String value, boolean javaType) {
		if(javaType) {
			if(javaTypeMap==null) {
				javaTypeMap = new JavaType();
			}
			Class<?> colType = javaTypeMap.getType(type);
			if(colType!=null) {
				try {
					return StrKit.notBlank(value) ? TypeConverter.me().convert(colType, value) : null;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			return value;
		}
 		type = type.toLowerCase();
 		if(type.startsWith("bigint") || type.startsWith("bigserial") || type.startsWith("int8") || type.equals("int unsigned"))return Long.parseLong(value);
 		else if(type.startsWith("int") || type.startsWith("serial") || type.startsWith("tinyint"))return Integer.parseInt(value);
 		else if(type.startsWith("float"))return Float.parseFloat(value);
 		else if(type.startsWith("double"))return Double.parseDouble(value);
 		else if(type.startsWith("num"))return Double.parseDouble(value);//numeric,number
 		else if(type.startsWith("date")) return Date.valueOf(value);
 		else if(type.startsWith("datetime")) return Timestamp.valueOf(value);
 		else if(type.startsWith("timestamp")) return Timestamp.valueOf(value);
 		else return value;
 	}
 	
	protected Object transArrayValue(String type, String[] value, boolean javaType) {
		if(javaType) {
			if(value.length > 0) {
				List<Object> result = Lists.newArrayList();
				for(int i=0; i<value.length; i++) {
					String val = value[i];
					Object oval = getValue(type, val, javaType);
					result.add(oval);
				}
				return result;
			}
			return java.util.Arrays.asList(value);
		}
 		//判断字段类型
 		type = type.toLowerCase();
 		if(type.startsWith("bigint") || type.startsWith("bigserial") || type.startsWith("int8") || type.equals("int unsigned"))return StringToLong(value);
 		else if(type.startsWith("int") || type.startsWith("tinyint")) return StringToInt(value);
 		else if(type.startsWith("float")) return StringToFloat(value);
 		else if(type.startsWith("double")) return StringToDouble(value);
 		else if(type.startsWith("num")) return StringToDouble(value);//numeric,number
 		else if(type.startsWith("date")) return StringToDate(value);
 		else if(type.startsWith("datetime")) return StringToTimestamp(value);
 		return java.util.Arrays.asList(value);
 	}
 	
 	private List<Integer> StringToInt(String[] arrs){
 	    List<Integer> ints = new ArrayList<Integer>();
 	    for(int i=0;i<arrs.length;i++){
 	    	ints.add(Integer.parseInt(arrs[i]));
 	    }
 	    return ints;
 	}
 	
 	private List<Long> StringToLong(String[] arrs){
 	    List<Long> ints = new ArrayList<Long>();
 	    for(int i=0;i<arrs.length;i++){
 	    	ints.add(Long.parseLong(arrs[i]));
 	    }
 	    return ints;
 	}
 	
 	private List<Float> StringToFloat(String[] arrs){
 		List<Float> ints = new ArrayList<Float>();
 	    for(int i=0;i<arrs.length;i++){
 	    	ints.add(Float.parseFloat(arrs[i]));
 	    }
 	    return ints;
 	}
 	
 	private List<Double> StringToDouble(String[] arrs){
 	   List<Double> ints = new ArrayList<Double>();
	    for(int i=0;i<arrs.length;i++){
	    	ints.add(Double.parseDouble(arrs[i]));
	    }
	    return ints;
 	}

 	private List<Date> StringToDate(String[] arrs){
 		List<Date> ints = new ArrayList<Date>();
 	    for(int i=0;i<arrs.length;i++){
 	    	ints.add(Date.valueOf(arrs[i]));
 	    }
 	    return ints;
 	}
 	
 	private List<Timestamp> StringToTimestamp(String[] arrs){
 		List<Timestamp> ints = new ArrayList<Timestamp>();
 	    for(int i=0;i<arrs.length;i++){
 	    	ints.add(Timestamp.valueOf(arrs[i]));
 	    }
 	    return ints;
 	}
// 	
// 	private int[] StringToInt(String[] arrs){
// 	    int[] ints = new int[arrs.length];
// 	    for(int i=0;i<arrs.length;i++){
// 	        ints[i] = Integer.parseInt(arrs[i]);
// 	    }
// 	    return ints;
// 	}
// 	
// 	private float[] StringToFloat(String[] arrs){
// 		float[] ints = new float[arrs.length];
// 	    for(int i=0;i<arrs.length;i++){
// 	        ints[i] = Float.parseFloat(arrs[i]);
// 	    }
// 	    return ints;
// 	}
// 	
// 	private double[] StringToDouble(String[] arrs){
// 		double[] ints = new double[arrs.length];
// 	    for(int i=0;i<arrs.length;i++){
// 	        ints[i] = Double.parseDouble(arrs[i]);
// 	    }
// 	    return ints;
// 	}
//
// 	private Date[] StringToDate(String[] arrs){
// 		Date[] ints = new Date[arrs.length];
// 	    for(int i=0;i<arrs.length;i++){
// 	        ints[i] = Date.valueOf(arrs[i]);
// 	    }
// 	    return ints;
// 	}
// 	
// 	private Timestamp[] StringToTimestamp(String[] arrs){
// 		Timestamp[] ints = new Timestamp[arrs.length];
// 	    for(int i=0;i<arrs.length;i++){
// 	        ints[i] = Timestamp.valueOf(arrs[i]);
// 	    }
// 	    return ints;
// 	}
}
