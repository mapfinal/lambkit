/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.sql.column;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jfinal.kit.StrKit;
import com.lambkit.db.meta.MetaKit;
import com.lambkit.db.meta.TableMeta;
import com.lambkit.db.mgr.MgrConstants;
import com.lambkit.db.mgr.MgrTable;
import com.lambkit.db.mgr.MgrdbManager;
import com.lambkit.db.mgr.MgrdbService;
import com.lambkit.db.sql.SqlJoinMode;

public class JoinOn {
	private SqlJoinMode type = SqlJoinMode.INNER_JOIN;
	private String mainTableName;
	private String joinTableName;
	private String mainAlias;
	private String joinAlias;
	private List<JoinOnField> joinOnFields;
	private ColumnsGroup columnsGroup;
	private String loadColumns = null;

	public static JoinOn createJoin(JSONObject joinObj, String database) {
		if (joinObj == null) {
			return null;
		}
		JoinOn join = new JoinOn();
		String type = joinObj.getString("type");
		type = StrKit.notBlank(type) ? type.trim().toLowerCase() : type;
		if("left".equals(type)) {
			join.setType(SqlJoinMode.LEFT_JOIN);
		} else if("right".equals(type)) {
			join.setType(SqlJoinMode.RIGHT_JOIN);
		} else if("inner".equals(type)) {
			join.setType(SqlJoinMode.INNER_JOIN);
		} else {
			join.setType(SqlJoinMode.INNER_JOIN);
		}
		String table = joinObj.getString("table");
		String alias = joinObj.getString("alias");
		String mainTable = joinObj.getString("mainTable");
		String mainAlias = joinObj.getString("mainAlias");
		
		join.setMainTableName(mainTable);
		join.setJoinTableName(table);
		if(StrKit.notBlank(mainAlias)) join.setMainAlias(mainAlias);
		if(StrKit.notBlank(alias)) join.setJoinAlias(alias);
		
		String columnskey = null;
		if(joinObj.containsKey("columns")) {
			columnskey = joinObj.getString("columns");
		}
		if(StrKit.notBlank(columnskey)) {
			join.setLoadColumns(columnskey);
		}
		
		JSONArray array = joinObj.getJSONArray("on");
		for(int i=0; i<array.size(); i++) {
			JSONObject onObj = array.getJSONObject(i);
			String mainField = onObj.getString("mainField");
			String joinField = onObj.getString("joinField");
			JoinOnField field = new JoinOnField(mainField, joinField);
			join.getJoinOnFields().add(field);
		}
		
		TableMeta joinMeta = MetaKit.createTable(database, join.getJoinTableName(), "");
		if(joinMeta!=null) {
			join.getColumnsGroup().filter(joinObj.getJSONObject("filter"), joinMeta);
		} else {
			join.getColumnsGroup().filter(joinObj.getJSONObject("filter"), null);
		}
		
		MgrdbService service = StrKit.isBlank(database) ? MgrdbManager.me().getService() : MgrdbManager.me().getService(database);
		MgrTable joinTbc = service !=null ? service.createTable(join.getJoinTableName(), MgrConstants.NONE, null) : null;
		if(joinTbc!=null) {
			if(StrKit.notBlank(columnskey)) {
				columnskey = columnskey.trim();
				if(columnskey.endsWith("*")) {
					columnskey = joinTbc.getLoadColumns(join.getJoinAlias());
				}
			}
			if(StrKit.notBlank(columnskey)) {
				join.setLoadColumns(columnskey);
			}
		}
		return join;
	}
	
	public JoinOn() {
		columnsGroup = new ColumnsGroup();
		joinOnFields = Lists.newArrayList();
	}

	public JoinOn(String mainTableName, String mainTableField, String joinTableName, String joinTableField,
			SqlJoinMode type, Columns cols) {
		this.type = type;
		this.mainTableName = mainTableName;
		this.mainAlias = mainTableName;
		this.joinTableName = joinTableName;
		this.joinAlias = joinTableName;
		if(this.joinOnFields==null) {
			this.joinOnFields = Lists.newArrayList();
		}
		this.joinOnFields.add(new JoinOnField(mainTableField, joinTableField));
		if(columnsGroup==null) {
			columnsGroup = new ColumnsGroup();
		}
		columnsGroup.addColumns(cols);
	}

	public JoinOn(String mainTableName, String mainTableField, String joinTableName, String joinTableField,
			Columns cols) {
		this.mainTableName = mainTableName;
		this.mainAlias = mainTableName;
		this.joinTableName = joinTableName;
		this.joinAlias = joinTableName;
		if(this.joinOnFields==null) {
			this.joinOnFields = Lists.newArrayList();
		}
		this.joinOnFields.add(new JoinOnField(mainTableField, joinTableField));
		if(columnsGroup==null) {
			columnsGroup = new ColumnsGroup();
		}
		columnsGroup.addColumns(cols);
	}

	public JoinOn(String mainTableName, String mainTableField, String joinTableName, String joinTableField,
			SqlJoinMode type) {
		this.type = type;
		this.mainTableName = mainTableName;
		this.mainAlias = mainTableName;
		this.joinTableName = joinTableName;
		this.joinAlias = joinTableName;
		if(this.joinOnFields==null) {
			this.joinOnFields = Lists.newArrayList();
		}
		this.joinOnFields.add(new JoinOnField(mainTableField, joinTableField));
	}

	public JoinOn(String mainTableName, String mainTableField, String joinTableName, String joinTableField) {
		this.mainTableName = mainTableName;
		this.mainAlias = mainTableName;
		this.joinTableName = joinTableName;
		this.joinAlias = joinTableName;
		if(this.joinOnFields==null) {
			this.joinOnFields = Lists.newArrayList();
		}
		this.joinOnFields.add(new JoinOnField(mainTableField, joinTableField));
	}

	/**
	 * 归集所有消息
	 * 
	 * @return
	 */
	public List<String> getMessageList() {
		List<String> msg = Lists.newArrayList();
		if (columnsGroup != null) {
			return columnsGroup.getMessageList();
		}
		return msg;
	}
	
	/**
	 * 归集所有引用
	 * 
	 * @return
	 */
	public List<String> getRefList() {
		List<String> msg = Lists.newArrayList();
		if (columnsGroup != null) {
			return columnsGroup.getRefList();
		}
		return msg;
	}

	public JoinOn mode(SqlJoinMode type) {
		this.type = type;
		return this;
	}

	public String getJoinTableName() {
		return joinTableName;
	}

	public JoinOn setJoinTableName(String joinTableName) {
		this.joinTableName = joinTableName;
		if(StrKit.isBlank(joinAlias)) {
			joinAlias = joinTableName;
		}
		return this;
	}

	public SqlJoinMode getType() {
		return type;
	}

	public JoinOn setType(SqlJoinMode type) {
		this.type = type;
		return this;
	}

	public String getMainTableName() {
		return mainTableName;
	}

	public void setMainTableName(String mainTableName) {
		this.mainTableName = mainTableName;
		if(StrKit.isBlank(mainAlias)) {
			mainAlias = mainTableName;
		}
	}

	public List<JoinOnField> getJoinOnFields() {
		return joinOnFields;
	}

	public void setJoinOnFields(List<JoinOnField> joinOnFields) {
		this.joinOnFields = joinOnFields;
	}

	public ColumnsGroup getColumnsGroup() {
		return columnsGroup;
	}

	public void setColumnsGroup(ColumnsGroup columnsGroup) {
		this.columnsGroup = columnsGroup;
	}

	public String getMainAlias() {
		return mainAlias;
	}

	public void setMainAlias(String mainAlias) {
		this.mainAlias = mainAlias;
	}

	public String getJoinAlias() {
		return joinAlias;
	}

	public void setJoinAlias(String joinAlias) {
		this.joinAlias = joinAlias;
	}

	public String getLoadColumns() {
		return loadColumns;
	}

	public void setLoadColumns(String loadColumns) {
		this.loadColumns = loadColumns;
	}

}
