package com.lambkit.db.sql.column;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.common.util.StringUtils;
import com.lambkit.db.dialect.LambkitDialect;
import com.lambkit.db.meta.ColumnMeta;
import com.lambkit.db.meta.MetaKit;
import com.lambkit.db.meta.TableMeta;
import com.lambkit.db.mgr.MgrConstants;
import com.lambkit.db.mgr.MgrTable;
import com.lambkit.db.mgr.MgrdbManager;
import com.lambkit.db.mgr.MgrdbService;
import com.lambkit.db.sql.IQuery;

public class QueryExample implements IQuery {

	// 数据库方言
	private LambkitDialect dialect;
	// 查询配置
	private Example example;

	public QueryExample(String tableName, LambkitDialect dialect) {
		setDialect(dialect);
		setExample(Example.create(tableName));
	}
	
	public QueryExample(Example example, LambkitDialect dialect) {
		setDialect(dialect);
		setExample(example);
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "example";
	}

	public static QueryExample by(JSONObject json) {
		String tableName = json.getString("table");
		if (StrKit.notBlank(tableName)) {
//				if(tableName.contains(":")) {
//					String[] tbs = tableName.split(":");
//					configName = tbs[0];
//					tableName = tbs[1];
//				}
			String database = null;
			if (json.containsKey("database")) {
				database = json.getString("databse");
			}
			MgrdbService service = StrKit.isBlank(database) ? MgrdbManager.me().getService()
					: MgrdbManager.me().getService(database);
			MgrTable tbc = service != null ? service.createTable(tableName, MgrConstants.NONE, null) : null;
			if (tbc != null) {
				QueryExample builder = new QueryExample(tbc.getName(), tbc.getDialect());
				builder.getExample().setJSON(json, tbc);
				return builder;
			} else {
				TableMeta meta = MetaKit.createTable(database, tableName, "");
				if (meta != null) {
					QueryExample builder = new QueryExample(tableName, (LambkitDialect) meta.getDialect());
					builder.getExample().setJSON(json, meta);
					return builder;
				}
			}
		}
		return null;
	}

	public static QueryExample by(MgrTable tbc, JSONObject json) {
		if (tbc != null) {
			QueryExample builder = new QueryExample(tbc.getName(), tbc.getDialect());
			builder.getExample().setJSON(json, tbc);
			return builder;
		}
		return null;
	}

	public static QueryExample by(MgrTable tbc, Controller controller) {
//			QueryExample builder = new QueryExample(tbc.getName(), tbc.getDialect());
//			List<? extends IField> flds = tbc.getFieldList();
//			for(int i=0; i<flds.size(); i++) {
//				IField fld = flds.get(i);
//				if(fld.getIsselect().toUpperCase().equals("Y"))
//				{
//					builder.column(fld.getName(), getParaTrans(controller, fld.getName()), fld.getDatatype(), false);
//				}
//			}
//			return builder;
		if (tbc == null) {
			return null;
		}
		return by(tbc.getMeta(), controller);
	}

	public static QueryExample by(TableMeta tbc, Controller controller) {
		if (tbc == null) {
			return null;
		}
		QueryExample builder = new QueryExample(tbc.getName(), (LambkitDialect) tbc.getDialect());
		List<ColumnMeta> flds = tbc.getColumnMetas();
		for (int i = 0; i < flds.size(); i++) {
			ColumnMeta fld = flds.get(i);
			builder.column(fld.getName(), getParaTrans(controller, fld.getName()), fld.getJavaType(), true);
		}
		return builder;
	}

	private static String getParaTrans(Controller controller, String name) {
		String param = controller.getPara(name);
		if (param != null && param.trim().length() > 0) {
			return StringUtils.transactSQLInjection(param);
		}
		return null;
	}

	public Record findFirst(DbPro db) {
		SqlPara sqlPara = getDialect().forFindByExample(example, null);
		return db.findFirst(sqlPara);
	}

	public List<Record> find(DbPro db) {
		SqlPara sqlPara = getDialect().forFindByExample(example, null);
		return db.find(sqlPara);
	}

	public List<Record> find(DbPro db, Integer count) {
		SqlPara sqlPara = getDialect().forFindByExample(example, count);
		// System.out.println("SQL: " + sqlPara.getSql());
		// System.out.println("para size: " + sqlPara.getPara().length);
		return db.find(sqlPara);
	}

	public Page<Record> paginate(DbPro db, int pageNumber, int pageSize) {
		SqlPara sqlPara = getDialect().forPaginateByExample(example);
		return db.paginate(pageNumber, pageSize, sqlPara);
	}

	public SqlPara findSqlPara() {
		return getDialect().forFindByExample(example, null);
	}

	public SqlPara findSqlPara(Integer count) {
		return getDialect().forFindByExample(example, count);
	}

	public SqlPara paginateSqlPara() {
		return getDialect().forPaginateByExample(example);
	}

	public QueryExample orderBy(String orderby) {
		String oby = example.getOrderBy();
		if (StrKit.isBlank(oby)) {
			oby = orderby;
		} else {
			oby += ", " + orderby;
		}
		example.setOrderBy(oby);
		return this;
	}

	public QueryExample groupBy(String groupby) {
		GroupBy gby = example.getGroupBy();
		if (gby == null) {
			gby = new GroupBy();
		}
		if (StrKit.isBlank(gby.getName())) {
			gby.setName(groupby);
		} else {
			String name = gby.getName();
			name += ", " + gby;
			gby.setName(name);
		}
		example.setGroupBy(gby);
		return this;
	}

	public QueryExample select(String selectItem) {
		String item = example.getLoadColumns();
		if (StrKit.isBlank(item)) {
			item = selectItem;
		} else {
			item += ", " + selectItem;
		}
		example.setLoadColumns(item);
		return this;
	}

	/**
	 * 加入select语句内容
	 * 
	 * @param tbc
	 * @param alias
	 * @return
	 */
	public QueryExample select(MgrTable tbc, String alias) {
		String select = example.getLoadColumns();
		if ("*".equals(select)) {
			select = MgrdbManager.me().getService().getSelectNamesOfView(tbc, alias);
		} else {
			select += ", " + MgrdbManager.me().getService().getSelectNamesOfView(tbc, alias);
		}
		example.setLoadColumns(select);
		return this;
	}

	public Example example() {
		return example;
	}

//		public Columns and() {
//			return example.and();
//		}
	//
//		public Columns or() {
//			return example.or();
//		}

	public Columns columns() {
		return example.columns();
	}

	/**
	 * 加入where条件
	 * 
	 * @param field
	 * @param value
	 * @param type
	 * @return
	 */
	public QueryExample column(String field, String value, String type, boolean javaType) {
		example.columns().filter(field, value, type, javaType);
		return this;
	}

	public Example getExample() {
		return example;
	}

	public void setExample(Example example) {
		this.example = example;
	}

	public LambkitDialect getDialect() {
		return dialect;
	}

	public void setDialect(LambkitDialect dialect) {
		this.dialect = dialect;
	}

	@Override
	public SqlPara getSqlPara() {
		// TODO Auto-generated method stub
		return findSqlPara();
	}

	@Override
	public Integer getPage() {
		// TODO Auto-generated method stub
		return example.getPage();
	}

	@Override
	public Integer getCount() {
		// TODO Auto-generated method stub
		return example.getCount();
	}

	@Override
	public void setPage(Integer page) {
		// TODO Auto-generated method stub
		example.setPage(page);
	}

	@Override
	public void setCount(Integer count) {
		// TODO Auto-generated method stub
		example.setCount(count);
	}
}
