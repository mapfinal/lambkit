/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */package com.lambkit.db.sql.column;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.common.LambkitResult;
import com.lambkit.common.util.ArrayUtils;
import com.lambkit.db.meta.TableMeta;
import com.lambkit.db.mgr.MgrTable;
import com.lambkit.db.sql.SqlJoinMode;

public class Example implements IExample{
	private String tableName;
	private String alias;
	//select
	private String loadColumns = "*";
	//where/filter
	private List<ColumnsGroup> oredColumns;
	//排序
	private String orderBy;
	//暂未实现，等待完善
	private List<JoinOn> joinOns;
	//group查询
	private GroupBy groupBy;
	//查询页码
	private int page = 0;
	//查询个数限制
	private int count = 15;
	
	//数据库配置名称
	private String configName;
	
	
	public Example() {
		oredColumns = Lists.newArrayList();
	}
	
	public static Example create(String table) {
		Example example = new Example();
		example.setTableName(table);
		return example;
	}
	
	public static Example create(String table, Columns columns) {
		Example example = new Example();
		example.setTableName(table).addColumns(columns);
		return example;
	}
	
	public void setJSON(JSONObject json, MgrTable tbc) {
		if(json==null) {
			return;
		}
		
		ColumnsGroup group = new ColumnsGroup();
		if(tbc!=null) {
			group.filter(json.getJSONObject("filter"), tbc.getMeta());
		} else {
			group.filter(json.getJSONObject("filter"), null);
		}
		add(group);
		
		if(json.containsKey("group")) {
			groupBy = new GroupBy(json.getJSONObject("group"), tbc);
		}
		
		generalJsonSetting(json);
		
		if(tbc!=null) {
			String columnskey = getLoadColumns().trim();
			if(StrKit.isBlank(columnskey) ||  columnskey.endsWith("*")) {
				columnskey = tbc.getLoadColumns(getAlias());
			}
			if(StrKit.notBlank(columnskey)) {
				setLoadColumns(columnskey);
			}
		}
		
	}
	
	public void setJSON(JSONObject json, TableMeta meta) {
		if(json==null) {
			return;
		}
		
		ColumnsGroup group = new ColumnsGroup();
		group.filter(json.getJSONObject("filter"), meta);
		add(group);
		
		
		if(json.containsKey("group")) {
			groupBy = new GroupBy(json.getJSONObject("group"), meta);
		}
		
		generalJsonSetting(json);
	}
	
	private void generalJsonSetting(JSONObject json) {
		String database = null;
		if (json.containsKey("database")) {
			database = json.getString("databse");
		}
		
		if (json.containsKey("alias")) {
			setAlias(json.getString("alias"));
		}
		
		if(json.containsKey("columns")) {
			String columnskey = json.getString("columns");
			setLoadColumns(columnskey);
		}
		
		if(json.containsKey("page")) {
			setPage(json.getInteger("page"));
		}
		if(json.containsKey("count")) {
			setCount(json.getInteger("count"));
		}
		if(json.containsKey("order")) {
			setOrderBy(json.getString("order"));
		}
		if(json.containsKey("join")) {
			JSONArray array = json.getJSONArray("join");
			for(int i=0; i<array.size(); i++) {
				JSONObject joinObj = array.getJSONObject(i);
				JoinOn join = JoinOn.createJoin(joinObj, database);
				if(joinOns==null) joinOns = Lists.newArrayList();
		    	joinOns.add(join);
			}
		}
	}
	
	/**
	 * 第一个ColumnsGroup
	 * @return
	 */
	public ColumnsGroup columns() {
		if(oredColumns.size() > 0) {
			return oredColumns.get(0);
		}
        return createColumns();
    }
	
	/**
	 * 加入or查询ColumnsGroup
	 * @param criteria
	 * @return
	 */
	public Example or(ColumnsGroup criteria) {
        criteria.withOr();
        oredColumns.add(criteria);
        return this;
    }

	/**
	 * 加入and查询ColumnsGroup
	 * @param criteria
	 * @return
	 */
    public Example and(ColumnsGroup criteria) {
        oredColumns.add(criteria);
        return this;
    }
    
    /**
     * 加入查询ColumnsGroup
     * @return
     */
    public ColumnsGroup createColumns() {
    	ColumnsGroup criteria = createColumnsInternal();
        oredColumns.add(criteria);
        return criteria;
    }

    protected ColumnsGroup createColumnsInternal() {
    	ColumnsGroup criteria = new ColumnsGroup();
        return criteria;
    }
    
    public Example join(String mainField, String joinTableName, String joinField, SqlJoinMode type, Columns cols) {
    	if(joinOns==null) joinOns = Lists.newArrayList();
    	JoinOn joinOn = new JoinOn(tableName, mainField, joinTableName, joinField, type, cols);
    	joinOns.add(joinOn);
    	return this;
	}
    
    public Example join(String mainField, String joinTableName, String joinField, SqlJoinMode type) {
    	if(joinOns==null) joinOns = Lists.newArrayList();
    	JoinOn joinOn = new JoinOn(tableName, mainField, joinTableName, joinField, type);
    	joinOns.add(joinOn);
    	return this;
	}
    
    public Example join(String mainField, String joinTableName, String joinField, Columns cols) {
    	if(joinOns==null) joinOns = Lists.newArrayList();
    	JoinOn joinOn = new JoinOn(tableName, mainField, joinTableName, joinField, cols);
    	joinOns.add(joinOn);
    	return this;
	}
    
    public Example join(String mainField, String joinTableName, String joinField) {
    	if(joinOns==null) joinOns = Lists.newArrayList();
    	JoinOn joinOn = new JoinOn(tableName, mainField, joinTableName, joinField);
    	joinOns.add(joinOn);
    	return this;
	}
    
    public Example join(String mainTableName, String mainField, String joinTableName, String joinField, SqlJoinMode type, Columns cols) {
    	if(joinOns==null) joinOns = Lists.newArrayList();
    	JoinOn joinOn = new JoinOn(mainTableName, mainField, joinTableName, joinField, type, cols);
    	joinOns.add(joinOn);
    	return this;
	}
    
    public Example join(String mainTableName, String mainField, String joinTableName, String joinField, SqlJoinMode type) {
    	if(joinOns==null) joinOns = Lists.newArrayList();
    	JoinOn joinOn = new JoinOn(mainTableName, mainField, joinTableName, joinField, type);
    	joinOns.add(joinOn);
    	return this;
	}
    
    public Example join(String mainTableName, String mainField, String joinTableName, String joinField) {
    	if(joinOns==null) joinOns = Lists.newArrayList();
    	JoinOn joinOn = new JoinOn(mainTableName, mainField, joinTableName, joinField);
    	joinOns.add(joinOn);
    	return this;
	}
	
	public JoinOn createJoinOn(String mainField, String joinTableName, String joinField, SqlJoinMode type) {
		JoinOn joinOn = new JoinOn(tableName, mainField, joinTableName, joinField, type);
    	joinOns.add(joinOn);
		return joinOn;
	}
	
	public JoinOn createJoinOn(String mainField, String joinTableName, String joinField) {
		JoinOn joinOn = new JoinOn(tableName, mainField, joinTableName, joinField);
    	joinOns.add(joinOn);
		return joinOn;
	}

    public void add(ColumnsGroup columns) {
    	this.oredColumns.add(columns);
    }
    
    public void clear() {
        oredColumns.clear();
        orderBy = null;
    }
    
	public List<ColumnsGroup> getColumnsList() {
		return oredColumns;
	}

	public String getLoadColumns() {
		return loadColumns;
	}
	
	/**
	 * 获取select，归集joinOn的所有查询
	 * @return
	 */
	public String getSelectSql() {
		String lcs = loadColumns;
		if(joinOns!=null && joinOns.size() > 0) {
			for (JoinOn joinOn : joinOns) {
				String jlc = joinOn.getLoadColumns();
				if(StrKit.notBlank(jlc)) {
					if(StrKit.notBlank(lcs)) {
						lcs += ",";
					}
					lcs += jlc;
				}
			}
		}
		return lcs;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public String getTableName() {
		return tableName;
	}
	
	public List<JoinOn> getJoinOnList() {
		return joinOns;
	}
	
	public Example addColumns(ColumnsGroup columns) {
		// TODO Auto-generated method stub
		oredColumns.add(columns);
		return this;
	}
	
	public Example setColumns(List<ColumnsGroup> oredColumns) {
		this.oredColumns = oredColumns;
		return this;
	}
	
	public Example setLoadColumns(String loadColumns) {
		this.loadColumns = loadColumns;
		return this;
	}

	public Example setSelectSql(String sql) {
		// TODO Auto-generated method stub
		this.loadColumns = sql;
		return this;
	}

	public Example setOrderBy(String orderBy) {
		// TODO Auto-generated method stub
		this.orderBy = orderBy;
		return this;
	}

	public Example setTableName(String table) {
		this.tableName = table;
		if(StrKit.isBlank(alias)) {
			alias = tableName;
		}
		return this;
	}
	
	/**
	 * 
	 */
	public void setRefValue(String refName, LambkitResult result) {
		
	}
	
	public void setRefValue(String refName, Object result) {
		
	}

	/**
	 * 添加para的value到sqlPara中
	 * @param sqlPara
	 */
	public void addValueToParam(SqlPara sqlPara) {
        if (ArrayUtils.isNotEmpty(getColumnsList())) {
            for (ColumnsGroup columnsGroup : getColumnsList()) {
            	addValueToParam(sqlPara, columnsGroup);
//            	if (ArrayUtils.isNotEmpty(columnsGroup.getList())) {
//                    for (Column column : columnsGroup.getList()) {
//                        column.addValueToParam(sqlPara);
//                    }
//                }
//            	for (Columns columns : columnsGroup.getOredColumns()) {
//            		if (ArrayUtils.isNotEmpty(columns.getList())) {
//                        for (Column column : columns.getList()) {
//                            column.addValueToParam(sqlPara);
//                        }
//                    }
//				}
            }
        }
        if (ArrayUtils.isNotEmpty(getJoinOnList())) {
        	for (JoinOn jon : getJoinOnList()) {
        		addValueToParam(sqlPara, jon.getColumnsGroup());
        	}
        }
	}
	
	private void addValueToParam(SqlPara sqlPara, ColumnsGroup columnsGroup) {
		if(columnsGroup==null) {
			return;
		}
		//System.out.println("columnsGroup: " + columnsGroup.getOredColumns().size());
		//System.out.println("columns: " + columnsGroup.getList().size());
        if (ArrayUtils.isNotEmpty(columnsGroup.getList())) {
            for (Column column : columnsGroup.getList()) {
                column.addValueToParam(sqlPara);
            }
        }
    	for (Columns columns : columnsGroup.getOredColumns()) {
    		if(columns instanceof ColumnsGroup) {
    			ColumnsGroup group = (ColumnsGroup) columns;
    			addValueToParam(sqlPara, group);
    		} else if (ArrayUtils.isNotEmpty(columns.getList())) {
    			//System.out.println("column: " + columns.getList().size());
                for (Column column : columns.getList()) {
                    column.addValueToParam(sqlPara);
                }
            }
		}
	}

	public GroupBy getGroupBy() {
		return groupBy;
	}

	public Example setGroupBy(GroupBy groupBy) {
		this.groupBy = groupBy;
		return this;
	}

	@Override
	public Example addColumns(Columns columns) {
		// TODO Auto-generated method stub
		columns().columns(columns);
		return this;
	}
	
	public Example add(Columns columns) {
		// TODO Auto-generated method stub
		columns().columns(columns);
		return this;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	/**
	 * 归集所有消息
	 * @return
	 */
	public List<String> getMessageList() {
		List<String> msg = Lists.newArrayList();
        if (ArrayUtils.isNotEmpty(getColumnsList())) {
            for (ColumnsGroup columnsGroup : getColumnsList()) {
            	msg.addAll(columnsGroup.getMessageList());
            }
        }
        if (ArrayUtils.isNotEmpty(getJoinOnList())) {
        	for (JoinOn jon : getJoinOnList()) {
        		msg.addAll(jon.getMessageList());
        	}
        }
        return msg;
	}
	
	/**
	 * 归集所有引用
	 * @return
	 */
	public List<String> getRefList() {
		List<String> msg = Lists.newArrayList();
        if (ArrayUtils.isNotEmpty(getColumnsList())) {
            for (ColumnsGroup columnsGroup : getColumnsList()) {
            	msg.addAll(columnsGroup.getRefList());
            }
        }
        if (ArrayUtils.isNotEmpty(getJoinOnList())) {
        	for (JoinOn jon : getJoinOnList()) {
        		msg.addAll(jon.getRefList());
        	}
        }
        return msg;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}
	
}
