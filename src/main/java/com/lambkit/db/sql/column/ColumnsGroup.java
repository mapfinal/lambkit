package com.lambkit.db.sql.column;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jfinal.kit.StrKit;
import com.lambkit.db.meta.ColumnMeta;
import com.lambkit.db.meta.TableMeta;

public class ColumnsGroup extends Columns {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4312562714499507154L;
	
	private List<Columns> oredColumns;

	public ColumnsGroup() {
		oredColumns = Lists.newArrayList();
	}
	
	
	public static ColumnsGroup create() {
        return new ColumnsGroup();
    }
    
    public static ColumnsGroup create(Column column) {
    	ColumnsGroup that = new ColumnsGroup();
        that.oredColumns.add(Columns.create(column));
        return that;

    }

    public static Columns create(String name, Object value) {
        return create().eq(name, value);
    }
    
    public static ColumnsGroup by() {
        return new ColumnsGroup();
    }
    
    public static ColumnsGroup by(Column column) {
    	ColumnsGroup that = new ColumnsGroup();
        that.oredColumns.add(Columns.create(column));
        return that;
    }

    public static ColumnsGroup by(String name, Object value) {
    	ColumnsGroup that = new ColumnsGroup();
        that.oredColumns.add(Columns.by(name, value));
        return that;
    }
    
	public ColumnsGroup columnsWithOr(Columns columns) {
		columns.withOr();
        oredColumns.add(columns);
        return this;
    }
	
	public ColumnsGroup columnsWithAnd(Columns columns) {
		columns.withAnd();
        oredColumns.add(columns);
        return this;
    }
	
	public ColumnsGroup addColumns(Columns columns) {
        oredColumns.add(columns);
        return this;
    }
	
	public ColumnsGroup columns(Columns columns) {
        oredColumns.add(columns);
        return this;
    }

	public List<Columns> getOredColumns() {
		return oredColumns;
	}

	public void setOredColumns(List<Columns> oredColumns) {
		this.oredColumns = oredColumns;
	}
	
	public Columns columns(int i) {
		return oredColumns.get(i);
    }
	
	public ColumnsGroup filter(String json, TableMeta tableMeta) {
		JSONObject jsonObj = JSON.parseObject(json);
		filter(jsonObj, tableMeta);
		return this;
	}
	
	public ColumnsGroup filter(JSONObject jsonObj, TableMeta tableMeta) {
		if(jsonObj!=null) {
			for(String field : jsonObj.keySet()) {
				if(StrKit.isBlank(field)) {
					continue;
				}
				if("$and".equalsIgnoreCase(field)) {
					ColumnsGroup group = new ColumnsGroup();
					group.filter(jsonObj.getJSONObject(field), tableMeta);
					columnsWithAnd(group);
				} else if("$or".equalsIgnoreCase(field)) {
					ColumnsGroup group = new ColumnsGroup();
					group.filter(jsonObj.getJSONObject(field), tableMeta);
					columnsWithOr(group);
				} else {
					if(field.contains("@")) {
						String[] flds = field.split("@");
						if(flds.length > 0) {
							String type = StrKit.notBlank(flds[1]) ? flds[1].toLowerCase() : null;
							filter(flds[0], jsonObj.getString(field), type, true);
						} else {
							ColumnMeta columnMeta = tableMeta!=null ? tableMeta.getColumn(flds[0]) : null;
							filter(flds[0], jsonObj.getString(field), columnMeta);
						}
					} else {
						ColumnMeta columnMeta = tableMeta!=null ? tableMeta.getColumn(field) : null;
						filter(field, jsonObj.getString(field), columnMeta);
					}
				}
			}
		}
		return this;
	}
		
	public ColumnsGroup filter(String field, String value, ColumnMeta columnMeta) {
		if(StrKit.notBlank(field)) {
			String javatype = columnMeta!=null ? columnMeta.getJavaType() : "java.lang.String";
			filter(field, value, javatype, true);
		}
		return this;
	}

	public ColumnsGroup filter(String field, String value, String type, boolean javaType) {
		if(StrKit.notBlank(value)) {
			String info = value.trim();
			if(info.startsWith("!")) {
				info = info.substring(1).trim();
			}
			if(info.startsWith("(") && info.endsWith(")")) {
				//多个过滤条件
				info = info.substring(1, info.length()-1);
				//System.out.println("filter: " + info);
				filterGroup(this, field, info, type, 0, javaType);
			} else {
				Columns columns = new Columns();
				columns.filter(field, value, type, javaType);
				this.columnsWithAnd(columns);
			}
		}
		return this;
	}
	
	/**
	 * 归集所有消息
	 * @return
	 */
	public List<String> getMessageList() {
		List<String> msg = Lists.newArrayList();
		if(getMessage()!=null) {
			msg.addAll(getMessage());
		}
		for (Columns columns : getOredColumns()) {
			if(columns instanceof ColumnsGroup) {
    			ColumnsGroup group = (ColumnsGroup) columns;
    			msg.addAll(group.getMessageList());
    		} else {
    			if(columns.getMessage()!=null) {
    				msg.addAll(columns.getMessage());
    			}
            }
		}
        return msg;
	}
	
	/**
	 * 归集所有引用
	 * @return
	 */
	public List<String> getRefList() {
		List<String> msg = Lists.newArrayList();
		if(getRefs()!=null) {
			msg.addAll(getRefs());
		}
		for (Columns columns : getOredColumns()) {
			if(columns instanceof ColumnsGroup) {
    			ColumnsGroup group = (ColumnsGroup) columns;
    			msg.addAll(group.getRefList());
    		} else {
    			if(columns.getRefs()!=null) {
    				msg.addAll(columns.getRefs());
    			}
            }
		}
        return msg;
	}
}
