/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.sql.criterion;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.db.sql.IQuery;
import com.lambkit.db.sql.criterion.junction.Junction;

public class QueryCriterion implements IQuery {

	private Integer page;
	private Integer count;
	private String select;
	private String sql;
	private List<Object> paraList;
	
	public QueryCriterion() {
	}
	
	public QueryCriterion(String sql) {
		// TODO Auto-generated constructor stub
		setSql(sql);
	}
	
	public QueryCriterion(String sql, List<Object> paraList) {
		// TODO Auto-generated constructor stub
		setSql(sql);
		addParaList(paraList);
	}
	
	public QueryCriterion(String sql, Object[] paras) {
		// TODO Auto-generated constructor stub
		setSql(sql);
		addParas(paras);
	}
	
	public QueryCriterion(String sql, Object value) {
		// TODO Auto-generated constructor stub
		setSql(sql);
		addPara(value);
	}
	
	/********************************************************/
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "criterion";
	}
	
	public String getSql() {
		return sql;
	}
	
	public void setSql(String sql) {
		this.sql = sql;
	}
	
	public void addPara(Object para) {
		if (paraList == null) {
			paraList = new ArrayList<Object>();
		}
		paraList.add(para);
	}
	
	public void addParas(Object[] value) {
		for(int i=0; i<value.length; i++) {
			addPara(value[i]);
		}
	}
	
	public void addParaList(List<Object> value) {
		paraList.addAll(value);
	}
	
	public Object[] getPara() {
		if (paraList == null || paraList.size() == 0) {
			return new Object[0];
		} else {
			return paraList.toArray(new Object[paraList.size()]);
		}
	}
	
	public void clear() {
		sql = null;
		if (paraList != null) {
			paraList.clear();
		}
	}
	
	public List<Object> getParaList() {
		// TODO Auto-generated method stub
		return this.paraList;
	}
	
	/********************************************************/
	
	public QueryCriterion and(Criteria criteria, Criterion criterion) {
		if(criterion==null) return this;
		StringBuilder sb = new StringBuilder();
		if(StrKit.notBlank(getSql())) sb.append(getSql());
		QueryCriterion csql = criterion.getQuery(criteria);
		if(StrKit.notBlank(csql.getSql())) {
			if(criterion instanceof Junction) {
				if(StrKit.notBlank(getSql())) sb.append(" and (");
				sb.append(csql.getSql());
				sb.append(") ");
			} else {
				if(StrKit.notBlank(getSql())) sb.append(" and ");
				sb.append(csql.getSql());
			}
			setSql(sb.toString());
			addParaList(csql.getParaList());
		}
		return this;
	}

	public QueryCriterion or(Criteria criteria, Criterion criterion) {
		if(criterion==null) return this;
		StringBuilder sb = new StringBuilder();
		QueryCriterion csql = criterion.getQuery(criteria);
		if(StrKit.notBlank(csql.getSql())) {
			if(criterion instanceof Junction) {
				if(StrKit.notBlank(getSql())) {
					sb.append(" (");
					sb.append(getSql());
					sb.append(")");
				}
				sb.append(" or (");
				sb.append(csql.getSql());
				sb.append(") ");
			} else {
				if(StrKit.notBlank(getSql())) {
					sb.append(getSql());
					sb.append(" or ");
				}
				sb.append(csql.getSql());
			}
			setSql(sb.toString());
			addParaList(csql.getParaList());
		}
		return this;
	}
	
	/*
	public CriterionSql and(CriterionSql criterion) {
		if(StrKit.notBlank(sql)) setSql(sql + " and " + criterion.getSql());
		else setSql(criterion.getSql());
		put(criterion.getParamList());
		return this;
	}
	
	public CriterionSql and(List<CriterionSql> criterionList) {
		for (CriterionSql criterion : criterionList) {
			and(criterion);
		}
		return this;
	}
	
	public CriterionSql or(CriterionSql criterion) {
		setSql("(" + sql + ") or " + criterion.getSql());
		put(criterion.getParamList());
		return this;
	}
	
	public CriterionSql or(List<CriterionSql> criterionList) {
		CriterionSql orCri = new CriterionSql();
		for (CriterionSql criterion : criterionList) {
			orCri.and(criterion);
		}
		setSql("(" + sql + ") or (" + orCri.getSql() + ")");
		put(orCri.getParamList());
		return this;
	}
	
	public void put(List<Object> paraList) {
		if(paramList!=null) {
			paramList.addAll(paraList);
		}
	}
	*/
	
	public QueryCriterion setSql(Criteria criteria) {
		if(StrKit.notBlank(getSql())) {
			if(StrKit.notBlank(criteria.getAlias())) {
				setSql(getSql().replace("{alias}", criteria.getAlias()));
			} else {
				setSql(getSql().replace("{alias}.", ""));
			}
		}
		return this;
	}
	
	public QueryCriterion selectAll(Criteria criteria) {
		int joinSize = criteria.joinSize();
		if(joinSize==0) {
			setSelect("select * ");
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append("select").append(criteria.getAlias()).append(".*");
			for (Criteria crt : criteria.getCriteriaList()) {
				String _alias = crt.getAlias();
				_alias = StrKit.isBlank(_alias) ? crt.getTableName() : _alias;
				sb.append(", ").append(_alias).append(".*");
			}
			sb.append(" ");
			setSelect(sb.toString());
		}
		return this;
	}
	/********************************************************/

	@Override
	public Integer getPage() {
		return page;
	}

	@Override
	public void setPage(Integer page) {
		this.page = page;
	}
	
	@Override
	public Integer getCount() {
		// TODO Auto-generated method stub
		return count;
	}
	
	@Override
	public void setCount(Integer count) {
		// TODO Auto-generated method stub
		this.count = count;
	}
	
	@Override
	public SqlPara getSqlPara() {
		// TODO Auto-generated method stub
		SqlPara sqlPara = new SqlPara();
		for(int i=0; i<paraList.size(); i++) {
			sqlPara.addPara(paraList.get(i));
		}
		sqlPara.setSql(select + " " + sql);
		return sqlPara;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}
	
}
