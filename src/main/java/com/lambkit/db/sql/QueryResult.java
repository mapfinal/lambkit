package com.lambkit.db.sql;

public class QueryResult {

	/**
	 * SQL查询方法，find,findFirst,paginate,update,uelete,count
	 */
	private String sqlMethod;

	/**
	 * find,findFirst,paginate的结果
	 */
	private Object queryResult;

	/**
	 * update,delete,count的结果
	 */
	private int countResult;

	public QueryResult(String sqlMethod) {
		this.sqlMethod = sqlMethod;
	}

	public QueryResult(String sqlMethod, Object queryResult) {
		this.sqlMethod = sqlMethod;
		this.queryResult = queryResult;
	}

	public QueryResult(String sqlMethod, int countResult) {
		this.sqlMethod = sqlMethod;
		this.countResult = countResult;
	}

	public String getSqlMethod() {
		return this.sqlMethod;
	}

	public void setSqlMethod(String sqlMethod) {
		this.sqlMethod = sqlMethod;
	}

	public Object getQueryResult() {
		return queryResult;
	}

	public void setQueryResult(Object queryResult) {
		this.queryResult = queryResult;
	}

	public int getCountResult() {
		return countResult;
	}

	public void setCountResult(int countResult) {
		this.countResult = countResult;
	}

}
