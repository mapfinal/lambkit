/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.sql;

import com.jfinal.plugin.activerecord.SqlPara;

public interface IQuery {

	/**
	 * 用于数据库操作
	 * @return
	 */
	SqlPara getSqlPara();
	
	/**
	 * 页码
	 * @return
	 */
	Integer getPage();
	
	/**
	 * 查询数量
	 * @return
	 */
	Integer getCount();

	void setPage(Integer page);

	void setCount(Integer count);
	
	/**
	 * 类型, example, condition, criterion, sqltemplate
	 * @return
	 */
	String getType();
	
}
