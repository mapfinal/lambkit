package com.lambkit.db.sql;

import java.util.Map;

import com.lambkit.db.sql.column.QueryExample;

public class QueryBuilder {

	// 查询组合
	private Map<String, IQuery> queryMap;
	// 其他查询结果
	private Map<String, QueryResult> result;

	public void addExample(String name, QueryExample example) {

	}

	public void addResult(String name, QueryResult value) {

	}

	public Map<String, QueryResult> getResult() {
		return result;
	}

	public void setResult(Map<String, QueryResult> result) {
		this.result = result;
	}

	public Map<String, IQuery> getQueryMap() {
		return queryMap;
	}

	public void setQueryMap(Map<String, IQuery> queryMap) {
		this.queryMap = queryMap;
	}

}
