/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db;

import java.util.List;

import com.google.common.collect.Lists;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.Model;
import com.lambkit.Lambkit;
import com.lambkit.common.LambkitManager;
import com.lambkit.common.bean.ActiveRecordBean;
import com.lambkit.common.exception.LambkitIllegalConfigException;
import com.lambkit.common.util.ArrayUtils;
import com.lambkit.common.util.StringUtils;
import com.lambkit.core.cache.CacheManager;
import com.lambkit.db.datasource.DataSourceConfig;
import com.lambkit.db.datasource.DataSourceConfigManager;
import com.lambkit.db.datasource.DataSourcePluginBuilder;
import com.lambkit.db.dialect.LambkitAnsiSqlDialect;
import com.lambkit.db.dialect.LambkitMysqlDialect;
import com.lambkit.db.dialect.LambkitOracleDialect;
import com.lambkit.db.dialect.LambkitPostgreSqlDialect;
import com.lambkit.db.dialect.LambkitSqlServerDialect;
import com.lambkit.db.dialect.LambkitSqlite3Dialect;

public class LambkitDataSource {

	/**
	 * 数据源名称
	 */
	private String configName; // DataSourceConfig Name from DataSourceConfigManager
	/**
	 * 数据源运行插件
	 */
	private DataSourcePlugin dataSourcePlugin;
	/**
	 * 数据表
	 */
	private List<LambkitTable> tables;
	/**
	 * 是否是动态数据源
	 */
	private boolean dynamicDataSource = false;
	
	public LambkitDataSource(DataSourceConfig datasourceConfig) {
		if (datasourceConfig.isConfigOk()) {
			this.configName = datasourceConfig.getName();
			dataSourcePlugin = createDataSourcePlugin(datasourceConfig);
			dataSourcePlugin.getActiveRecordPlugin().setShowSql(Lambkit.isDevMode());
			dataSourcePlugin.getActiveRecordPlugin().setCache(CacheManager.me().getCache());
            configSqlTemplate(datasourceConfig, dataSourcePlugin.getActiveRecordPlugin());
            configDialect(dataSourcePlugin.getActiveRecordPlugin(), datasourceConfig);
        }
	}
	
	public LambkitDataSource(DataSourceConfig datasourceConfig, boolean dynamicDataSource) {
		this(datasourceConfig);
		this.dynamicDataSource = dynamicDataSource;
	}
	
	public DataSourceConfig getDatasourceConfig() {
		return DataSourceConfigManager.me().getDatasourceConfig(configName);
	}
	
	public DbPro db() {
		return Db.use(configName);
	}
	
	/**
	 * 启动插件
	 */
	public boolean start() {
		if(dataSourcePlugin!=null) {
			return dataSourcePlugin.start();
		}
		return true;
	}
	
	/**
	 * 停止插件
	 */
	public boolean stop() {
		if(dataSourcePlugin!=null) {
			return dataSourcePlugin.stop();
		}
		return true;
	}
	
	/**
	 * 状态
	 * @return
	 */
	public int getDataSourceActiveState() {
		return dataSourcePlugin!=null ? dataSourcePlugin.getDataSourceActiveState() : 0;
	}
	
	public void addTable(LambkitTable table) {
		if(tables==null) {
			tables = Lists.newArrayList();
		}
		tables.add(table);
	}
	
	/**
     * 配置 本地 sql
     *
     * @param datasourceConfig
     * @param activeRecordPlugin
     */
    private void configSqlTemplate(DataSourceConfig datasourceConfig, ActiveRecordPlugin activeRecordPlugin) {
        String sqlTemplatePath = datasourceConfig.getSqlTemplatePath();
        if (StringUtils.isNotBlank(sqlTemplatePath)) {
            if (sqlTemplatePath.startsWith("/")) {
                activeRecordPlugin.setBaseSqlTemplatePath(datasourceConfig.getSqlTemplatePath());
            } else {
                activeRecordPlugin.setBaseSqlTemplatePath(PathKit.getRootClassPath() + "/" + datasourceConfig.getSqlTemplatePath());
            }
        } else {
            activeRecordPlugin.setBaseSqlTemplatePath(PathKit.getRootClassPath());
        }
        String sqlTemplateString = datasourceConfig.getSqlTemplate();
        if (sqlTemplateString != null) {
            String[] sqlTemplateFiles = sqlTemplateString.split(",");
            for (String sql : sqlTemplateFiles) {
                activeRecordPlugin.addSqlTemplate(sql);
            }
        }
    }

    /**
     * 配置 数据源的 方言
     *
     * @param activeRecordPlugin
     * @param datasourceConfig
     */
    private void configDialect(ActiveRecordPlugin activeRecordPlugin, DataSourceConfig datasourceConfig) {
        switch (datasourceConfig.getType()) {
            case DataSourceConfig.TYPE_MYSQL:
                activeRecordPlugin.setDialect(new LambkitMysqlDialect());
                break;
            case DataSourceConfig.TYPE_ORACLE:
                activeRecordPlugin.setDialect(new LambkitOracleDialect());
                break;
            case DataSourceConfig.TYPE_SQLSERVER:
                activeRecordPlugin.setDialect(new LambkitSqlServerDialect());
                break;
            case DataSourceConfig.TYPE_SQLITE:
                activeRecordPlugin.setDialect(new LambkitSqlite3Dialect());
                break;
            case DataSourceConfig.TYPE_ANSISQL:
                activeRecordPlugin.setDialect(new LambkitAnsiSqlDialect());
                break;
            case DataSourceConfig.TYPE_POSTGRESQL:
                activeRecordPlugin.setDialect(new LambkitPostgreSqlDialect());
                break;
            default:
                throw new LambkitIllegalConfigException("only support datasource type : mysql、orcale、sqlserver、sqlite、ansisql and postgresql, please check your lambkit.properties. ");
        }
    }

    /**
     * 创建 ActiveRecordPlugin 插件，用于数据库读写
     *
     * @param config
     * @return
     */
    private DataSourcePlugin createDataSourcePlugin(DataSourceConfig config) {
    	DataSourcePlugin dataSourcePlugin = new DataSourcePluginBuilder(config).build(this);
        if(dataSourcePlugin!=null) {
        	LambkitManager.me().addActiveRecord(new ActiveRecordBean(config.getName(), config.getDbname(), config));
        }
        /**
         * 不需要添加映射的直接返回
         */
        if (!config.isNeedAddMapping()) {
            return dataSourcePlugin;
        }
        System.out.println("需要添加映射");

        String configTableString = config.getTable();
        String excludeTableString = config.getExcludeTable();
        
        List<LambkitTable> TableMappings = TableManager.me().getTablesInfos(configTableString, excludeTableString);
        if (ArrayUtils.isNullOrEmpty(TableMappings)) {
            return dataSourcePlugin;
        }
        for (LambkitTable ti : TableMappings) {
            if (StringUtils.isNotBlank(ti.getPrimaryKey())) {
            	dataSourcePlugin.addMapping(ti.getTableName(), ti.getPrimaryKey(), (Class<? extends Model<?>>) ti.getModelClass());
            } else {
            	dataSourcePlugin.addMapping(ti.getTableName(), (Class<? extends Model<?>>) ti.getModelClass());
            }
        }
        return dataSourcePlugin;
    }
	
	public String getConfigName() {
		return configName;
	}
	public void setConfigName(String configName) {
		this.configName = configName;
	}
	public List<LambkitTable> getTables() {
		return tables;
	}
	public void setTables(List<LambkitTable> tables) {
		this.tables = tables;
	}

	public boolean isDynamicDataSource() {
		return dynamicDataSource;
	}

	public void setDynamicDataSource(boolean dynamicDataSource) {
		this.dynamicDataSource = dynamicDataSource;
	}

	public DataSourcePlugin getDataSourcePlugin() {
		return dataSourcePlugin;
	}

	public void setDataSourcePlugin(DataSourcePlugin dataSourcePlugin) {
		this.dataSourcePlugin = dataSourcePlugin;
	}
}
