package com.lambkit.db.remote;

import com.lambkit.distributed.node.NodeManager;
import com.lambkit.distributed.node.info.Node;

public class RemoteDataSource {

	private String nodeId;
	private Node node;
	
	public RemoteDataSource(String nodeId) {
		this.setNodeId(nodeId);
		setNode(NodeManager.me().getNode(nodeId));
	}
	
	
	

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
}
