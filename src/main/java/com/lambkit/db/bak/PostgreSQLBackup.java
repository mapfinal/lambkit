/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.bak;

import com.jfinal.core.JFinal;

public class PostgreSQLBackup extends DbBackup {

	private String ostype = "windows";
	private String dbpath;
	
	public PostgreSQLBackup(String ostype, String dbpath) {
		config();
		this.ostype = ostype;
		this.dbpath = dbpath;
	}
	
	@Override
	public void backup() {
		// TODO Auto-generated method stub
		// 得到配置文件
		try {
			Runtime rt = Runtime.getRuntime();
			String backPath = JFinal.me().getServletContext().getRealPath("/")
					+ "/static/dbback/" + System.currentTimeMillis() + ".sql";
			
			StringBuffer cmdbuf = new StringBuffer();
	        cmdbuf.append(dbpath);
	        cmdbuf.append("pg_dump -U ");// 用户名
	        cmdbuf.append(getUsername());
	        cmdbuf.append(" -f ");
	        cmdbuf.append(backPath);
	        cmdbuf.append(" -E utf8 ");// 编码
	        cmdbuf.append(" -Ft ");
	        cmdbuf.append(getDbname());
	        String pg = cmdbuf.toString();
//			System.out.println("dump : " + pg);
			if("windows".equals(ostype)) {
				Process proc = rt.exec("cmd.exe /c " + pg);// 设置导出编码为utf8。这里必须是utf8
				int tag = proc.waitFor();// 等待进程终止
				System.out.println("load: "+tag);
			} else if("linux".equals(ostype)) {
				Process proc = rt.exec(new String[]{"/bin/sh", "-c", pg});// 设置导出编码为utf8。这里必须是utf8
				int tag = proc.waitFor();// 等待进程终止
				System.out.println("load: "+tag);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void load(String selectName) {
		// TODO Auto-generated method stub
		// System.out.println(sqlPath);
		if (selectName.substring(selectName.lastIndexOf(".") + 1).equals("sql")) {
			String backPath = JFinal.me().getServletContext().getRealPath("/")
					+ "/static/dbback/" + selectName;
			// 得到配置文件
			try {
				Runtime rt = Runtime.getRuntime();
				StringBuffer cmdTemp = new StringBuffer();// 命令模版
		        cmdTemp.append(dbpath);
		        cmdTemp.append("pg_restore -U ");
		        cmdTemp.append(getUsername());
		        cmdTemp.append(" -c -d ");
		        cmdTemp.append(" " + getDbname() + " ");
		        cmdTemp.append(backPath);
		        String pg = cmdTemp.toString();
				System.out.println("restore : " + pg);
				if("windows".equals(ostype)) {
					Process proc = rt.exec("cmd.exe /c " + pg);
					int tag = proc.waitFor();// 等待进程终止
					System.out.println("load: "+tag);
				} else if("linux".equals(ostype)) {
					Process proc = rt.exec(new String[]{"/bin/sh", "-c", pg});
					int tag = proc.waitFor();// 等待进程终止
					System.out.println("load: "+tag);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String getOstype() {
		return ostype;
	}

	public void setOstype(String ostype) {
		this.ostype = ostype;
	}

}
