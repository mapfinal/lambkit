package com.lambkit.db;

import javax.sql.DataSource;

import com.jfinal.kit.LogKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.IPlugin;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Config;
import com.jfinal.plugin.activerecord.Model;
import com.lambkit.Lambkit;
import com.lambkit.common.LambkitManager;
import com.lambkit.common.bean.TableMappingBean;
import com.lambkit.db.datasource.ActiveRecordPluginWrapper;

public abstract class DataSourcePlugin implements IPlugin {

	private static Log log = Log.getLog(DataSourcePlugin.class);

	private String configName;
	private ActiveRecordPlugin arp;

	public DataSourcePlugin(String configName) {
		this.setConfigName(configName);
	}

	/**
	 * 数据源状态，0未启动，1是运行，2是睡眠
	 * @return
	 */
	public abstract int getDataSourceActiveState();

	/**
	 * 通用数据源接口获取
	 * @return
	 */
	public abstract DataSource getDataSource();

	/**
	 * 启动arp
	 * @return
	 */
	protected boolean startArp() {
		if (arp.getDevMode() == null) {
			arp.setDevMode(Lambkit.isDevMode());
		}
		if (arp.start() == false) {
			String message = "Plugin start error: " + arp.getClass().getName();
			log.error(message);
			throw new RuntimeException(message);
		}
		return true;
	}

	protected boolean stopArp() {
		boolean success = false;
		try {
			success = arp.stop();
		} catch (Exception e) {
			success = false;
			LogKit.error(e.getMessage(), e);
		}
		if (!success) {
			System.err.println("Plugin stop error: " + arp.getClass().getName());
		}
		return true;
	}

	public ActiveRecordPlugin getActiveRecordPlugin() {
		return this.arp;
	}

	public Config getConfig() {
		return this.arp.getConfig();
	}

	public DataSourcePlugin addMapping(String tableName, String primaryKey, Class<? extends Model<?>> modelClass) {
		arp.addMapping(tableName, primaryKey, modelClass);
		String configName = arp.getConfig().getName();
		LambkitManager.me().addMapping(new TableMappingBean(configName, tableName, primaryKey, modelClass.getName()));
		LambkitTable tableWrapper = TableManager.me().addMapping(configName, tableName, primaryKey, modelClass);
		DbManager.me().addTable(configName, tableWrapper);
		return this;
	}

	public DataSourcePlugin addMapping(String tableName, Class<? extends Model<?>> modelClass) {
		arp.addMapping(tableName, modelClass);
		String configName = arp.getConfig().getName();
		LambkitManager.me().addMapping(new TableMappingBean(configName, tableName, "", modelClass.getName()));
		LambkitTable tableWrapper = TableManager.me().addMapping(configName, tableName, "", modelClass);
		DbManager.me().addTable(configName, tableWrapper);
		return this;
	}

	public DataSourcePlugin addSqlTemplate(String sqlTemplate) {
		arp.addSqlTemplate(sqlTemplate);
		return this;
	}

	public DataSourcePlugin addSqlTemplate(com.jfinal.template.source.ISource sqlTemplate) {
		arp.addSqlTemplate(sqlTemplate);
		return this;
	}

	public DataSourcePlugin setBaseSqlTemplatePath(String baseSqlTemplatePath) {
		arp.setBaseSqlTemplatePath(baseSqlTemplatePath);
		return this;
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public ActiveRecordPlugin getArp() {
		return arp;
	}

	public void setArp(ActiveRecordPlugin arp) {
		this.arp = arp;
	}

	public ActiveRecordPluginWrapper getArpWrapper() {
		// TODO Auto-generated method stub
		return new ActiveRecordPluginWrapper(arp);
	}
}
