package com.lambkit.web.commond;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jfinal.kit.StrKit;
import com.jfinal.render.JsonRender;
import com.lambkit.Lambkit;
import com.lambkit.common.ResultKit;
import com.lambkit.common.util.EncryptUtils;
import com.lambkit.common.util.StringUtils;
import com.lambkit.plugin.auth.AuthManager;

public class CommonAction {

	public void execute(HttpServletRequest request, HttpServletResponse response) {
		String cmd = request.getParameter("cmd");
		if(StrKit.isBlank(cmd)) {
			render(ResultKit.json(0, "cmd invalid", null), request, response);
			return;
		}
		cmd = cmd.trim();
		HttpSession session = request.getSession();
		
		if(cmd.startsWith("lambkit")) {
			cmd = cmd.substring(7);
			cmd = cmd.trim();
			if(cmd.startsWith("-p")) {
				cmd = cmd.substring(2);
				cmd = cmd.trim();
				String encryptKey = Lambkit.getLambkitConfig().getEncryptKey();
				String password = Lambkit.getLambkitConfig().getCommondSecret();
				String pswd = EncryptUtils.MD5(cmd + encryptKey);
				if(pswd.equals(password)) {
					session.setAttribute("lambkit_cms_user", StringUtils.getUuid32());
					render(ResultKit.json(1, "lambkit connected", null), request, response);
					return;
				} else {
					render(ResultKit.json(0, "password invalid", null), request, response);
					return;
				}
			} else {
				render(ResultKit.json(1, "only -p commond", null), request, response);
				return;
			}
		} else if(cmd.equals("exit")) {
			session.invalidate();//removeAttribute("lambkit_cms_user");
			render(ResultKit.json(0, "cmd invalid", "command is necessary"), request, response);
		} else {
			Object user = session.getAttribute("lambkit_cms_user");
			if(user==null) {
				render(ResultKit.json(0, "user invalid", null), request, response);
				return;
			} else {
				if("auth -c clear".equals(cmd)) {
					AuthManager.me().clearCache();
					render(ResultKit.json(1, "command execution completed", null), request, response);
					return;
				}
				render(ResultKit.json(0, "cmd unsupported", null), request, response);
				return;
			}
		}
		
	}
	
	private void render(Object object, HttpServletRequest request, HttpServletResponse response) {
		JsonRender render = new JsonRender(object);
		render.setContext(request, response);
		render.render();
	}
	
}
