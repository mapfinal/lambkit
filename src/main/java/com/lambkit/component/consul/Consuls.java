package com.lambkit.component.consul;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.lambkit.Lambkit;
import com.lambkit.common.LambkitManager;
import com.lambkit.common.bean.ActionBean;
import com.lambkit.common.bean.ActionMapping;
import com.lambkit.core.api.route.ApiAction;
import com.lambkit.core.api.route.ApiRoute;
import com.lambkit.core.api.route.ApiStore;
import com.lambkit.core.gateway.RouteServiceInstance;
import com.lambkit.distributed.node.NodeManager;
import com.lambkit.distributed.node.info.Node;

public class Consuls {

	public static final String SERVICE_NAME = "lambkit-service";
	
	static ConsulClient consulClient;

	public static ConsulClient use() {
		consulClient = consulClient==null ? new ConsulClient() : consulClient;
		return consulClient;
	}
	
	public static ConsulClient use(String hostAndPort) {
		return new ConsulClient(hostAndPort);
	}
	
	public static String getInstanceName() {
		ConsulConfig config = Lambkit.config(ConsulConfig.class);
		StringBuffer strb = new StringBuffer();
		strb.append(config.getName());
		strb.append("-");
		strb.append(config.getGroup());
		return strb.toString();
	}
	
	public static String getInstanceName(String group) {
		ConsulConfig config = Lambkit.config(ConsulConfig.class);
		StringBuffer strb = new StringBuffer();
		strb.append(config.getName());
		strb.append("-");
		strb.append(group);
		return strb.toString();
	}
	
	public static String getServiceName(RouteServiceInstance rsi) {
		StringBuffer strb = new StringBuffer();
		strb.append("http-");
		strb.append(rsi.getType());
		strb.append("-");
		strb.append(rsi.getName().replaceAll("/", "."));
		String serviceName = strb.toString();
		return serviceName;
	}

	public static String getServiceName(String protocol, String type, String name) {
		StringBuffer strb = new StringBuffer();
		strb.append(protocol);
		strb.append("-");
		strb.append(type);
		strb.append("-");
		strb.append(name.replaceAll("/", "."));
		String serviceName = strb.toString();
		return serviceName;
	}
	
	public static void start() {
		Node node = NodeManager.me().getNode();
		String ip = node.getIp();
		int port = LambkitManager.me().getPort();
		
		StringBuffer strb = new StringBuffer("http://");
		strb.append(ip).append(":").append(port);
		String address = strb.toString();
		
//		System.out.println(address);
		//加入Api到Consul，用于HttpRpc
		ApiStore apiStore = ApiRoute.me().getApiStore();
		ConcurrentHashMap<String, ApiAction> apiMap = apiStore.getApiMap();
		for (String key : apiMap.keySet()) {
			ConsulService service = new ConsulService(SERVICE_NAME, ip, port);
			service.setUrl(address + ApiRoute.me().getTargetName() + "?method=" + key);
			service.setId(ip + ":" + port + "-" + key);
			service.setNode(node.getHost());
			service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_PROTOCOL + "http");
			service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_TYPE + "apiroute");
			service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_URL + address + key);
			use().registry(service);
		}
		
		//加入Controller到Consul，用户Gateway
		ActionMapping actionMapping = LambkitManager.me().getInfo().getActionMapping();
		Map<String, ActionBean> mapping = actionMapping.getMapping();
		for(String key : mapping.keySet()) {
//		List<String> keys=Lists.newArrayList();
//		keys.add("/sdss/gis");
//		keys.add("/sdss/gis/wms");
//		keys.add("/sdss/gis/wfs");
//		keys.add("/sdss/gis/tile");
//		for (String key : keys) {
			ConsulService service = new ConsulService(SERVICE_NAME, ip, port);
			service.setUrl(address + key);
			service.setId(ip + ":" + port + "-" + key);
			service.setNode(node.getHost());
			service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_PROTOCOL + "http");
			service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_TYPE + "action");
			service.addTag(ConsulConstants.CONSUL_TAG_LAMBKIT_URL + address + key);
			use().registry(service);
		}
	}
	
	public static void stop() {
		
	}
	
}
