package com.lambkit.component.httpclient;

import org.apache.http.HttpHost;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.lambkit.Lambkit;

/**
 * CloseableHttpClient的使用和优化<br/>
 * HttpClient优化思路<br/>
 * 1、池化 <br/>
 * 2、长连接 <br/>
 * 3、httpclient和httpget复用 <br/>
 * 4、合理的配置参数（最大并发请求数，各种超时时间，重试次数）<br/>
 * 5、异步 <br/>
 * 6、多读源码
 * @author yangyong
 */
public class HttpClientFactory {

	private static CloseableHttpClient httpClient;

	public static CloseableHttpClient getHttpClient() {
		
		PoolingHttpClientConfig config = Lambkit.config(PoolingHttpClientConfig.class);

		if (!config.getIsEnableConnectionManager()) {

			return HttpClients.custom().addInterceptorLast(new HttpCrossIntercepter())
					.setRetryHandler(new LambkitHttpRequestRetryHandler()).build();
		}

		if (httpClient == null) {

			PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
			// Increase max total connection to 200
			cm.setMaxTotal(config.getMaxTotal());
			// Increase default max connection per route to 20
			cm.setDefaultMaxPerRoute(config.getDefaultMaxPerRoute());
			// Increase max connections for localhost:80 to 50
			HttpHost localhost = new HttpHost("locahost", config.getLocalhostRoutePort());
			cm.setMaxPerRoute(new HttpRoute(localhost), config.getMaxPerRoute());

			// 定时清除已关闭的连接
			new IdleConnectionMonitorThread(cm).start();

			httpClient = HttpClients.custom().addInterceptorLast(new HttpCrossIntercepter())
					.setRetryHandler(new LambkitHttpRequestRetryHandler()).setConnectionManager(cm).build();
		}

		return httpClient;
	}

}
