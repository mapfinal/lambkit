package com.lambkit.component.httpclient;

public class PoolingHttpClientConfig {

	private String targetUrl = "";
	// Increase max total connection to 200
	private Integer maxTotal = 200;
	// Increase default max connection per route to 20
	private Integer defaultMaxPerRoute = 20;
	// port
	private Integer localhostRoutePort = 80;
	// Increase max connections for localhost:80 to 50
	private Integer maxPerRoute = 50;
	// cookie
	private String local_cookie_path = "/";
	/**
	 * SSL连接 配置
	 */
	private String amHost;
	private String password;
	private String keyStorePath;
	private Long updateTime = 600L;
	private Long updateFastTime = 1L;

	private Boolean isEnable = true;

	private Boolean isEnableConnectionManager = false;

	public Integer getMaxTotal() {
		return maxTotal;
	}

	public void setMaxTotal(Integer maxTotal) {
		this.maxTotal = maxTotal;
	}

	public Integer getDefaultMaxPerRoute() {
		return defaultMaxPerRoute;
	}

	public void setDefaultMaxPerRoute(Integer defaultMaxPerRoute) {
		this.defaultMaxPerRoute = defaultMaxPerRoute;
	}

	public Integer getLocalhostRoutePort() {
		return localhostRoutePort;
	}

	public void setLocalhostRoutePort(Integer localhostRoutePort) {
		this.localhostRoutePort = localhostRoutePort;
	}

	public Integer getMaxPerRoute() {
		return maxPerRoute;
	}

	public void setMaxPerRoute(Integer maxPerRoute) {
		this.maxPerRoute = maxPerRoute;
	}

	public String getLocal_cookie_path() {
		return local_cookie_path;
	}

	public void setLocal_cookie_path(String local_cookie_path) {
		this.local_cookie_path = local_cookie_path;
	}

	public String getAmHost() {
		return amHost;
	}

	public void setAmHost(String amHost) {
		this.amHost = amHost;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getKeyStorePath() {
		return keyStorePath;
	}

	public void setKeyStorePath(String keyStorePath) {
		this.keyStorePath = keyStorePath;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public Long getUpdateFastTime() {
		return updateFastTime;
	}

	public void setUpdateFastTime(Long updateFastTime) {
		this.updateFastTime = updateFastTime;
	}

	public Boolean getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Boolean isEnable) {
		this.isEnable = isEnable;
	}

	public Boolean getIsEnableConnectionManager() {
		return isEnableConnectionManager;
	}

	public void setIsEnableConnectionManager(Boolean isEnableConnectionManager) {
		this.isEnableConnectionManager = isEnableConnectionManager;
	}

	public String getTargetUrl() {
		return targetUrl;
	}

	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}

}
