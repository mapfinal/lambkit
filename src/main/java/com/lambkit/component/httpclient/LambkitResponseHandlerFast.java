package com.lambkit.component.httpclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ContentType;

import com.alibaba.fastjson.JSON;

/**
 * 将http获取到的结果转换成对象，完成RPC的远程过程调用
 * 
 * @author yangyong
 *
 * @param <T>
 */
public class LambkitResponseHandlerFast<T> implements ResponseHandler<T> {

	private Class<T> clazz;

	public LambkitResponseHandlerFast(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Override
	public T handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
		// TODO Auto-generated method stub
		HttpEntity entity = response.getEntity();
		StatusLine statusLine = response.getStatusLine();
		BufferedReader reader = null;
		String readJson = "";
		try {
			if (statusLine.getStatusCode() >= 300) {
				throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
			}
			if (entity == null) {
				throw new ClientProtocolException("Response contains no content");
			}
			ContentType contentType = ContentType.getOrDefault(entity);
			Charset charset = contentType.getCharset();
			InputStreamReader inputStreamReader = new InputStreamReader(entity.getContent(), charset);
			reader = new BufferedReader(inputStreamReader);
			String tempString = null;
			while ((tempString = reader.readLine()) != null) {
				readJson += tempString;
			}
		} catch (HttpResponseException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return JSON.parseObject(readJson, clazz);
	}

}
