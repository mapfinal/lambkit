package com.lambkit.component.httpclient.proxy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.lambkit.common.util.RequestUtils;

/**
 * Controller中的方法通过反向代理实现
 * 
 * @author yangyong
 *
 */
public class LambkitControllerProxyInterceptor implements Interceptor {

	private static final Log LOG = Log.getLog(LambkitControllerProxyInterceptor.class);
	
	@Override
	public void intercept(Invocation inv) {
		// TODO Auto-generated method stub
		Proxy ma = inv.getMethod().getAnnotation(Proxy.class);

		if (ma != null) {

			String uri = ma.uri();
			Controller c = inv.getController();
			HttpServletRequest request = c.getRequest();
			HttpServletResponse response = c.getResponse();
			if (uri.equals("")) {
				LOG.info("请求真实IP: " + RequestUtils.getIpAddress(request));
				new DefaultHttpProxy(ma.target()).execute(request, response);
			} else {
				LOG.info("IP: " + RequestUtils.getIpAddress(request) + "  代理:" + uri);
				new DefaultHttpProxy(ma.target()).execute(request, response, uri);
			}
			c.renderNull();

			return;

		} else {

			inv.invoke();

		}
	}
}
