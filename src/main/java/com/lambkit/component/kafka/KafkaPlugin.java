/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.component.kafka;

import com.jfinal.log.Log;
import com.jfinal.plugin.IPlugin;

/**
 * kafka插件
 * 
 * @author yangyong
 */
public class KafkaPlugin implements IPlugin {
	private final Log logger = Log.getLog(this.getClass());

	private KafkaConfig config;
	
	public KafkaPlugin(KafkaConfig config) {
		this.config = config;
	}
	
	public KafkaPlugin(String name, String servers, String keySerializer, String valueSerializer) {
		config.setName(name);
		config.setServers(servers);
		config.setKeySerializer(keySerializer);
		config.setValueSerializer(valueSerializer);
	}

	@Override
	public boolean start() {
		Kafka.addProducer(config);
		return true;
	}

	@Override
	public boolean stop() {
		logger.info("销毁生产者:"+config.getName()+",及其消费者开始");
		Kafka.close(config.getName());
		logger.info("销毁生产者:"+config.getName()+",及其消费者结束");
		return true;
	}

}
