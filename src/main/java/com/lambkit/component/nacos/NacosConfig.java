package com.lambkit.component.nacos;

import com.jfinal.kit.StrKit;
import com.lambkit.core.config.annotation.PropertieConfig;

@PropertieConfig(prefix = "lambkit.component.nacos")
public class NacosConfig {

	private boolean enable = false;
	private String address ;//nacos的IP
	private String name = "lambkit"; //命名空间
	private String group = "api";//组名称
	private String ip ;//ip
	private int port ;//端口
	private String clusterName ;//集群名称
	
	public boolean isUsable() {
		return enable && StrKit.notBlank(address) && StrKit.notBlank("groupName");
	}
	
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getClusterName() {
		return clusterName;
	}
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
