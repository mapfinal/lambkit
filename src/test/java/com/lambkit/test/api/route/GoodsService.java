package com.lambkit.test.api.route;

import com.lambkit.core.api.ApiRequest;
import com.lambkit.core.api.route.ApiMapping;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.test.api.route
 */
public interface GoodsService {

	// 无缝集成
	@ApiMapping(value = "lambkit.api.goods.add", auth = 2)
	public Goods addGoods(Goods goods, Integer id, ApiRequest apiRequest);

	@ApiMapping("lambkit.api.goods.get")
	public Goods getGodds(Integer id);
}
