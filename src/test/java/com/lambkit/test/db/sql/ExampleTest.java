package com.lambkit.test.db.sql;

import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.db.dialect.LambkitMysqlDialect;
import com.lambkit.db.sql.column.ExampleBuilder;

public class ExampleTest {

	public ExampleTest() {
		// TODO Auto-generated constructor stub
	}
	
	public void sql01() {
		ExampleBuilder example = new ExampleBuilder("upms_user",new LambkitMysqlDialect());
		example.columns().eq("username", "admin");
		SqlPara sqlPara = example.findSqlPara();
		System.out.println(sqlPara.toString());
	}
	
	public void sql02() {
		ExampleBuilder example = new ExampleBuilder("upms_user",new LambkitMysqlDialect());
		example.columns().eq("sex", "1").eq("locked", 0);
		SqlPara sqlPara = example.findSqlPara();
		System.out.println(sqlPara.toString());
	}
	
	public void sql03() {
		ExampleBuilder example = new ExampleBuilder("upms_user",new LambkitMysqlDialect());
		example.columns().eq("sex", "1").or().eq("locked", 0);
		SqlPara sqlPara = example.findSqlPara();
		System.out.println(sqlPara.toString());
		
		
	}
	
	public void sql04() {
		ExampleBuilder example = new ExampleBuilder("upms_user",new LambkitMysqlDialect());
		example.columns().eq("sex", "1").or().eq("locked", 0);
		SqlPara sqlPara = example.findSqlPara();
		System.out.println(sqlPara.toString());
		
		
	}
}
