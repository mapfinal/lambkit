package com.lambkit.test.db.sql;

import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.db.dialect.LambkitPostgreSqlDialect;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

public class ColumnsExampleTest {

	public static void main(String[] args) {
		Columns columns1 = Columns.by("name", "tom");
		Columns columns2 = Columns.by("name", "cat");
		Example example = Example.create("db_names", columns1);
		example.add(columns2.withOr());
		
		LambkitPostgreSqlDialect dialect = new LambkitPostgreSqlDialect();
		SqlPara sqlPara = dialect.forFindByExample(example, null);
		System.out.println("SQL: " + sqlPara.getSql());
		for (Object obj : sqlPara.getPara()) {
			System.out.print(obj + ",");
		}
		System.out.println();
	}
	
}
