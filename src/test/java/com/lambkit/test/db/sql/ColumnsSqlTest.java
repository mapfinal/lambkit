package com.lambkit.test.db.sql;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.db.dialect.LambkitPostgreSqlDialect;
import com.lambkit.db.sql.column.ColumnsGroup;
import com.lambkit.db.sql.column.ExampleBuilder;

public class ColumnsSqlTest {

	public static void main(String[] args) {
		Kv filter = Kv.by("name@String", "(((it%) & (%em)) | (=item))");
		filter.set("status", ">1");
		filter.set("userid", "=11");
		filter.set("pid", "!{null}");
		filter.set("tag", "%,action,%");
		filter.set("val", "3-7");
		
		Kv or = Kv.by("city", "衢州市");
		or.set("pac", "[330881,330802]");
		filter.set("$or", or);
		
		ColumnsGroup group = new ColumnsGroup();
		group.filter(JSONObject.toJSONString(filter), null);
		
		LambkitPostgreSqlDialect dialect = new LambkitPostgreSqlDialect();
		ExampleBuilder example = new ExampleBuilder("example", dialect);
		example.getExample().add(group);
		SqlPara sqlPara = example.findSqlPara();
		System.out.println("SQL: " + sqlPara.getSql());
		for (Object obj : sqlPara.getPara()) {
			System.out.print(obj + ",");
		}
		System.out.println();
		
	}
	
	public void testGroup() {
		String field = "name";
		String value = "(((it%) & (%em)) | (=item))";
		String type = "varchar";
		
		ColumnsGroup group = new ColumnsGroup();
		group.filter(field, value, type, false);
		
		LambkitPostgreSqlDialect dialect = new LambkitPostgreSqlDialect();
		ExampleBuilder example = new ExampleBuilder("example", dialect);
		example.getExample().add(group);
		SqlPara sqlPara = example.findSqlPara();
		System.out.println("SQL: " + sqlPara.getSql());
		for (Object obj : sqlPara.getPara()) {
			System.out.print(obj + ",");
		}
		System.out.println();
	}
	
	
}
