package com.lambkit.test.db.sql;

public class FieldItem {
	
		private String field = "name";
		private String value = "(((it%) & (%em)) | (=item))";
		private String type = "varchar";
		
		public FieldItem(String field, String value, String type) {
			// TODO Auto-generated constructor stub
			setField(field);
			setValue(value);
			setType(type);
		}
		
		public String getField() {
			return field;
		}
		
		public String getValue() {
			return value;
		}
		
		public String getType() {
			return type;
		}
		
		public void setValue(String value) {
			this.value = value;
		}
		
		public void setField(String field) {
			this.field = field;
		}
		
		public void setType(String type) {
			this.type = type;
		}
	}