package com.lambkit.test.db;

import com.jfinal.plugin.activerecord.PageSqlKit;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.db.dialect.LambkitPostgreSqlDialect;
import com.lambkit.db.sql.column.Example;

public class DialectTest {

	public static void main(String[] args) {
		Example example = new Example();
		example.setTableName("sys_resource");
		example.setOrderBy("create_time DESC");
		
		LambkitPostgreSqlDialect dialect = new LambkitPostgreSqlDialect();
		SqlPara sqlPara = dialect.forPaginateByExample(example);
		String[] sqls = PageSqlKit.parsePageSql(sqlPara.getSql());
		
		String totalRowSql = "select count(*) " + dialect.replaceOrderBy(sqls[1]);
		System.out.println(totalRowSql);
		
		StringBuilder findSql = new StringBuilder();
		findSql.append(sqls[0]).append(' ').append(sqls[1]);
		System.out.println(findSql);
		
		
	}
}
