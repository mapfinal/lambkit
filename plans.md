## 开发计划

作为后续开发的指导

### LambkitAdmin

- 1、作为测试用例
- 2、作为开发案例
- 3、集成cms、upms、lms等管理系统
- 4、开发前后台应用界面

### JSON应用协议

**JSON-SQL for SqlTemplate**

前后端基于json定义的数据交换协议。
- 前端传入json格式的查询条件
- 后端转化为查询SQL
- 查询结果转成json格式返回

**JSON-Service for ApiRoute**

前后端基于json定义的方法调用协议
- 客户端传入json格式的调用方法名称，调用参数
- 服务器接收后，执行，返回json格式的结果


